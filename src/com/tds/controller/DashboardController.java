
package com.tds.controller;

import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.servlet.ServletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tds.dao.AdministrationDAO;
import com.tds.dao.SharedRideDAO;
import com.tds.dao.UtilityDAO;
import com.tds.process.QueueProcess;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.DriverLocationBO;
import com.tds.tdsBO.OpenRequestBO;
import com.tds.tdsBO.QueueBean;
import com.common.util.SystemUtils;

@Controller
public class DashboardController {
	@Autowired
	ServletConfig serveletConfig;


	@RequestMapping(value="/SpringDashboard", params = { "params", "status","rtJobStatus","routeJob","zoneList","drStatus","shDate","shDDate","searchDriver","zoneSearchDriver" })
	public  @ResponseBody String getAllValuesForDashboard(ModelMap model,HttpSession session,HttpServletRequest request, HttpServletResponse response, @RequestParam("params") String paramValue, @RequestParam("rtJobStatus") String rtJobStatus, @RequestParam("routeJob") String routeJobs,@RequestParam("status") String statusValue, @RequestParam("zoneList") String zoneList,@RequestParam("drStatus") String drStatus, @RequestParam("shDate") String shDate,@RequestParam("shDDate") String shDDate,@RequestParam("searchDriver") String findDriver,@RequestParam("zoneSearchDriver") String zoneSearchDriver ) throws UnsupportedEncodingException{
		
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		/*
		 * @author Baskar
		 * @param paramArr,status,rtJobStatus,ZoneList-- For jobs
		 * @param drStatus -- For Driver Details
		 * @param SearchDriver -- For Driver Details
		 * @param zoneSearchDriver -- For ZoneWaiting
		 * @param shDate -- For Shared Ride 
		 * @param shDDate -- For Shared Ride Details
		 * @return jobDetails,DriverDetails,Zone Waiting,SharedRide,Review Shared Ride
		 */
		
		JSONArray totalArray = new JSONArray();
		JSONObject totalObject = new JSONObject();
		JSONArray array = new JSONArray();
		JSONArray driverList = new JSONArray();
		JSONArray zoneJsonArr = new JSONArray();
		JSONArray shArray = new JSONArray();
		JSONArray shDArray = new JSONArray();

		AdminRegistrationBO adminBO  = (AdminRegistrationBO)request.getSession().getAttribute("user");  
		if(adminBO!=null){

			JSONObject refCh = new JSONObject();

			//JobDetails JSon Start
			ArrayList<OpenRequestBO> Address = null;
			int status = 0;
			String paramValues = "";
			String[] paramArr = new String[5];
			paramValues = "11111";
			if(paramValue!=null){
				paramValues = paramValue;
			}
			if (statusValue!= null) {
				status = Integer.parseInt(statusValue);
			}
			for (int i = 0;i < paramValues.length(); i++){
				paramArr[i]=paramValues.charAt(i)+"";
			}
			try {
				refCh.put("check", paramValues);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			//JobDetails JSON Start
			if(paramArr[0].equalsIgnoreCase("1")){
				int statusRtJobs = 0;
				if (rtJobStatus != null) {
					statusRtJobs = Integer.parseInt(rtJobStatus);
				}
				int route=0;
				if(routeJobs!=null){
					route=Integer.parseInt(routeJobs);
				}
				String compCode = adminBO.getAssociateCode();
				if(adminBO.getAssociateCode().equals(adminBO.getMasterAssociateCode()) && adminBO.getFleetDispatch()==1){
						compCode = adminBO.getMasterAssociateCode();	
				}
//				if(adminBO.getAssociateCode().equals(adminBO.getMasterAssociateCode())){
//					compCode = adminBO.getMasterAssociateCode();
//				}
				if (zoneList != null &&zoneList != "") {
					Address = AdministrationDAO.getOpnReqDash(compCode, 0, adminBO.getTimeZone(), status, zoneList,statusRtJobs,route);
				} else {
					Address = AdministrationDAO.getOpnReqDash(compCode, 0, adminBO.getTimeZone(), status, "",statusRtJobs,route);
				}

				for (int i = 0; i < Address.size(); i++) {
					JSONObject jobs = new JSONObject();
					try {
						jobs.put("D", Address.get(i).getDriverid() == null ? "" : Address.get(i).getDriverid());
						jobs.put("V", Address.get(i).getVehicleNo() == null ? "" : Address.get(i).getVehicleNo());
						jobs.put("N", Address.get(i).getName() == null ? "" : Address.get(i).getName());
						jobs.put("T", Address.get(i).getTripid() == null ? "" : Address.get(i).getTripid());
						jobs.put("Q", Address.get(i).getQueueno() == null ? "" : Address.get(i).getQueueno());
						jobs.put("A", Address.get(i).getSadd1() == null ? "" : Address.get(i).getSadd1());
						jobs.put("DA", Address.get(i).getEadd1() == null ? "" : Address.get(i).getEadd1());
						jobs.put("PT", Address.get(i).getPendingTime() == null ? "" : Address.get(i).getPendingTime());
						jobs.put("P", Address.get(i).getPhone() == null ? "" : Address.get(i).getPhone());
						jobs.put("S", Address.get(i).getTripStatus() == null ? "" : Address.get(i).getTripStatus());
						jobs.put("ST", Address.get(i).getRequestTime() == null ? "" : Address.get(i).getRequestTime());
						jobs.put("SD", Address.get(i).getSdate() == null ? "" : Address.get(i).getSdate());
						jobs.put("LA", Address.get(i).getSlat() == null ? "" : Address.get(i).getSlat());
						jobs.put("LO", Address.get(i).getSlong() == null ? "" : Address.get(i).getSlong());
						jobs.put("DD", Address.get(i).getDontDispatch());
						jobs.put("SR", Address.get(i).getTypeOfRide() == null ? "" : Address.get(i).getTypeOfRide());
						jobs.put("AC", Address.get(i).getPaytype() == null ? "" : Address.get(i).getPaytype());
						jobs.put("VC", Address.get(i).getAcct() == null ? "" : Address.get(i).getAcct());
						jobs.put("AMT", Address.get(i).getAmt() == null ? "0.00" :Address.get(i).getAmt());
						jobs.put("PC", Address.get(i).getPremiumCustomer() == 0 ? "" : Address.get(i).getPremiumCustomer());
						if(Address.get(i).getRouteDesc()!=null && !Address.get(i).getRouteDesc().equals("")){
							jobs.put("RT", Address.get(i).getRouteNumber() == null ? "" : Address.get(i).getRouteNumber()+"("+Address.get(i).getRouteDesc()+")");
						} else {
							jobs.put("RT", Address.get(i).getRouteNumber() == null ? "" : Address.get(i).getRouteNumber());
						}
						jobs.put("EDQ", Address.get(i).getEndQueueno() == null ? "" : Address.get(i).getEndQueueno());
						jobs.put("LMN", Address.get(i).getSlandmark() == null ? "" : Address.get(i).getSlandmark());
						jobs.put("ELM", Address.get(i).getElandmark() == null ? "" : Address.get(i).getElandmark());
						jobs.put("TS", Address.get(i).getTripSource());
						jobs.put("AIR", Address.get(i).getAirNo()==null?"":(Address.get(i).getAirName()+"-"+Address.get(i).getAirNo()));
						jobs.put("SCTY", Address.get(i).getScity() == null ? "" : Address.get(i).getScity());
						jobs.put("ECTY", Address.get(i).getEcity() == null ? "" : Address.get(i).getEcity());
						jobs.put("CMT", Address.get(i).getComments() == null ? "" : Address.get(i).getComments());
						jobs.put("SPL", Address.get(i).getSpecialIns() == null ? "" : Address.get(i).getSpecialIns());
						jobs.put("FL", Address.get(i).getCustomField() == null ? "" : Address.get(i).getCustomField());
						jobs.put("CC", (Address.get(i).getAssociateCode() == null || Address.get(i).getAssociateCode().equals(""))? compCode : Address.get(i).getAssociateCode());
						jobs.put("MC", (Address.get(i).getMasterAddressKey() == null || Address.get(i).getMasterAddressKey().equals("")) ? compCode : Address.get(i).getMasterAddressKey());

						String driverFlagValue = SystemUtils.getCompanyFlagDescription(serveletConfig, adminBO.getMasterAssociateCode(), Address.get(i).getDrProfile(), ";", 0);

						jobs.put("DP", driverFlagValue);
						array.put(jobs);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			}
			//JobDetails Json End
			
			//Driver Details Json
			if(paramArr[1].equalsIgnoreCase("1")){
				ArrayList<DriverLocationBO> drivers = new ArrayList<DriverLocationBO>();
				long lastTime = -1;
				String compCode = adminBO.getAssociateCode();
				if(adminBO.getAssociateCode().equals(adminBO.getMasterAssociateCode())){
					compCode = adminBO.getMasterAssociateCode();
				}
				if (drStatus!=null){
					if (drStatus.equals("4")) {
						drivers = UtilityDAO.getDriverLocationListByStatus(compCode, 4,adminBO.getTimeZone(),findDriver);
					} else if (drStatus.equals("3")) {
						drivers = UtilityDAO.getDriverLocationListByStatus(compCode, 3,adminBO.getTimeZone(),findDriver);
					} else if (drStatus.equals("5")) {
						drivers = UtilityDAO.getDriverLocationListByStatus(compCode, 5,adminBO.getTimeZone(),findDriver);
					} else {
						drivers = UtilityDAO.getDriverLocationListByStatus(compCode, 1,adminBO.getTimeZone(),findDriver);
					}
				}

				for (int i = 0; i < drivers.size(); i++) {
					JSONObject driver = new JSONObject();
					try {
						driver.put("varDr", drivers.get(i).getDriverid());
						driver.put("varLt", drivers.get(i).getLatitude());
						driver.put("varLg", drivers.get(i).getLongitude());
						driver.put("varSms", drivers.get(i).getSmsid());
						driver.put("varPh", drivers.get(i).getPhone());
						//System.out.println("id:"+drivers.get(i).getDriverid()+"flag:"+drivers.get(i).getProfile());
						String driverFlagValue = SystemUtils.getCompanyFlagDescription(serveletConfig, adminBO.getAssociateCode(), drivers.get(i).getProfile()!=null?drivers.get(i).getProfile():"", ";", 0);

						driver.put("varDp", driverFlagValue);
						driver.put("varSwh", drivers.get(i).getStatus());
						driver.put("varAvS", drivers.get(i).getProvider());
						driver.put("varVNo", drivers.get(i).getVehicleNo());
						driver.put("varDrNa", drivers.get(i).getDrName());
						driver.put("varAg", drivers.get(i).getAge());
						driver.put("varAppV", drivers.get(i).getAppVersion()!=null?drivers.get(i).getAppVersion():"");
						driver.put("lastUpDate", drivers.get(i).getLastUpdatedMilliSeconds());

						int loginTime  = drivers.get(i).getLoginTime()/60;
						int logintMinutes = drivers.get(i).getLoginTime()%60;
						driver.put("varLT", "Total LoggedIn Time : "+loginTime+":"+logintMinutes);
						driver.put("st", drivers.get(i).getDriverActivateStatus());
						
						driver.put("DD", drivers.get(i).getDistance());
						driverList.put(driver);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			}
			//Driver Details Json End		

			//	For Zone Waiting
			if(paramArr[2].equalsIgnoreCase("1")){

				ArrayList<QueueBean> al_list = new ArrayList<QueueBean>();
				QueueBean queue = new QueueBean();
				if(adminBO.getDispatchBasedOnVehicleOrDriver()==1){
					queue.setDriverId(zoneSearchDriver);
				}else{
					queue.setVehicleNo(zoneSearchDriver);
				}
				al_list = AdministrationDAO.getDriverinQDash(adminBO.getAssociateCode(), 0, queue,adminBO.getMasterAssociateCode());

				for (int i = 0; i < al_list.size(); i++) {
					JSONObject zoneObj = new JSONObject();
					try {
						zoneObj.put("Dr", al_list.get(i).getQU_DRIVERID() == null ? "" : al_list.get(i).getQU_DRIVERID());
						zoneObj.put("QN", al_list.get(i).getQU_NAME() == null ? "" : al_list.get(i).getQU_NAME());
						zoneObj.put("LT", al_list.get(i).getQU_LOGINTIME() == null ? "" : al_list.get(i).getQU_LOGINTIME());
						zoneObj.put("DRP", al_list.get(i).getDriverProfile() == null ? "" : al_list.get(i).getDriverProfile());
						zoneObj.put("F", al_list.get(i).getFlg());
						zoneObj.put("QD", al_list.get(i).getQueueDescription() == null ? "" : al_list.get(i).getQueueDescription());
						zoneObj.put("Veh", al_list.get(i).getVehicleNo() == null ? "" : al_list.get(i).getVehicleNo());
						zoneJsonArr.put(zoneObj);
					} catch (JSONException e) {
						e.printStackTrace();
					}

				}
			}
			//	Zone Waiting End
			// SharedRide first Screen Json
			if(paramArr[3].equalsIgnoreCase("1")){

				String date = shDate;
				ArrayList<OpenRequestBO> shAddress = AdministrationDAO.getOpnReqDashForShared(adminBO.getAssociateCode(), 0,adminBO.getTimeZone(), 0, date);
				for (int i = 0; i < shAddress.size(); i++) {
					if(shAddress.get(i).getTypeOfRide()!=null&&shAddress.get(i).getTypeOfRide().equalsIgnoreCase("1")){
						JSONObject shAddres = new JSONObject();
						try {
							shAddres.put("D", shAddress.get(i).getDriverid() == null ? "" : shAddress.get(i).getDriverid());
							shAddres.put("V", shAddress.get(i).getVehicleNo() == null ? "" : shAddress.get(i).getVehicleNo());
							shAddres.put("N", shAddress.get(i).getName() == null ? "" : shAddress.get(i).getName());
							shAddres.put("T", shAddress.get(i).getTripid() == null ? "" : shAddress.get(i).getTripid());
							shAddres.put("Q", shAddress.get(i).getQueueno() == null ? "" : shAddress.get(i).getQueueno());
							shAddres.put("A", shAddress.get(i).getSadd1() == null ? "" : shAddress.get(i).getSadd1());
							shAddres.put("EA", shAddress.get(i).getEadd1() == null ? "" : shAddress.get(i).getEadd1());

							shAddres.put("PT", shAddress.get(i).getPendingTime() == null ? "" : shAddress.get(i).getPendingTime());
							shAddres.put("P", shAddress.get(i).getPhone() == null ? "" : shAddress.get(i).getPhone());
							shAddres.put("S", shAddress.get(i).getTripStatus() == null ? "" : shAddress.get(i).getTripStatus());
							shAddres.put("ST", shAddress.get(i).getRequestTime() == null ? "" : shAddress.get(i).getRequestTime());
							shAddres.put("SD", shAddress.get(i).getSdate() == null ? "" : shAddress.get(i).getSdate());
							shAddres.put("LA", shAddress.get(i).getSlat() == null ? "" : shAddress.get(i).getSlat());
							shAddres.put("LO", shAddress.get(i).getSlong() == null ? "" : shAddress.get(i).getSlong());
							shAddres.put("DD", shAddress.get(i).getDontDispatch());
							shAddres.put("SR", shAddress.get(i).getTypeOfRide() == null ? "" : shAddress.get(i).getTypeOfRide());
							shAddres.put("AC", shAddress.get(i).getPaytype() == null ? "" : shAddress.get(i).getPaytype());
							shAddres.put("PC", shAddress.get(i).getPremiumCustomer() == 0 ? "" : shAddress.get(i).getPremiumCustomer());
							shAddres.put("RT", shAddress.get(i).getRouteNumber() == null ? "" : shAddress.get(i).getRouteNumber());

							shAddres.put("EDQ", shAddress.get(i).getEndQueueno() == null ? "" : shAddress.get(i).getEndQueueno());
							shAddres.put("LMN", shAddress.get(i).getSlandmark() == null ? "" : shAddress.get(i).getSlandmark());
							shAddres.put("SCTY", shAddress.get(i).getScity() == null ? "" : shAddress.get(i).getScity());
							shAddres.put("ECTY", shAddress.get(i).getEcity() == null ? "" : shAddress.get(i).getEcity());

							String driverFlagValue = SystemUtils.getCompanyFlagDescription(serveletConfig, adminBO.getAssociateCode(), shAddress.get(i).getDrProfile(), ";", 0);

							shAddres.put("DP", driverFlagValue);
							shAddres.put("REF", shAddress.get(i).getRefNumber());
							shAddres.put("REF1", shAddress.get(i).getRefNumber1());
							shAddres.put("REF2", shAddress.get(i).getRefNumber2());
							shAddres.put("REF3", shAddress.get(i).getRefNumber3());
							shArray.put(shAddres);

						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}}

			}
			// End Of SharedRide	

			//Shared Summary 
			if(paramArr[4].equalsIgnoreCase("1")){

				String shdate = shDDate!=null?shDDate:"";
				ArrayList<OpenRequestBO> shlist = SharedRideDAO.getSharedRideGroups(adminBO.getAssociateCode(), adminBO.getTimeZone(),shdate);
				for (int i = 0; i < shlist.size(); i++) {
					JSONObject shDAddress = new JSONObject();
					try {
						shDAddress.put("RT",shlist.get(i).getRouteNumber()!=null?shlist.get(i).getRouteNumber():"");
						shDAddress.put("CB",shlist.get(i).getCreatedBy()!=null?shlist.get(i).getCreatedBy():"");
						shDAddress.put("T",shlist.get(i).getCreatedTime()!=null?shlist.get(i).getCreatedTime():"");
						shDAddress.put("D",shlist.get(i).getCreatedDate()!=null?shlist.get(i).getCreatedDate():"");
						shDAddress.put("TT",shlist.get(i).getTotal()!=null?shlist.get(i).getTotal():"");
						shDAddress.put("DRI",shlist.get(i).getDriverid()!=null?shlist.get(i).getDriverid():"");
						shDAddress.put("STA",shlist.get(i).getShRideStatus()!=null?shlist.get(i).getShRideStatus():"");
						shDAddress.put("V",shlist.get(i).getVehicleNo()!=null?shlist.get(i).getVehicleNo():"");
						shDAddress.put("DES",shlist.get(i).getRouteDesc()!=null?shlist.get(i).getRouteDesc():"");
						shDArray.put(shDAddress);
					}catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			}
			// End Of Shared Summary
			//forLastRuntime
			long lastTimeNew = -1;
			if (serveletConfig.getServletContext().getAttribute(adminBO.getAssociateCode() + "Timer") != null) {
				QueueProcess queueProcess = (QueueProcess) this.serveletConfig.getServletContext().getAttribute(adminBO.getAssociateCode() + "Process");
				if (queueProcess.getLastRunTime() > 0) {
					lastTimeNew = (System.currentTimeMillis() - queueProcess.getLastRunTime()) / 1000;
				} else {

				}
			}
			JSONObject lastRun = new JSONObject();
			try {
				lastRun.put("lastRunTime", lastTimeNew);
				// driverFirst.put("lastUpDate","");
				//driverList.put(lastRun);
			} catch (JSONException e) {
				e.printStackTrace();
			}

			// End of Last run time

			//	Common for All jsonArrays
			try {
				totalObject.put("jobsList", array);
				totalObject.put("driverList", driverList);
				totalObject.put("zonesList",zoneJsonArr);
				totalObject.put("shList",shArray);
				totalObject.put("shDList", shDArray);
				totalObject.put("lastRun", lastRun);
				totalObject.put("refArr", refCh);
				totalArray.put(totalObject);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			//	response.setContentType("application/json");
			//	response.getWriter().write(totalArray.toString());
		}
		return totalArray.toString();


	}
}
