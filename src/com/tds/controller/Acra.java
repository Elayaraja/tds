package com.tds.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.asteriskjava.manager.ManagerConnection;

import com.tds.dao.AcraDAO;

/**
 * Servlet implementation class TestServlet
 */
public class Acra extends HttpServlet {
	boolean holdTheServlet= true;
	private static final long serialVersionUID = 1L;

    private ManagerConnection managerConnection;

    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Acra() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		doProcess(request, response);
	}
	
	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		doProcess(request, response);
	}
	public void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException 
	{
		String driver = request.getParameter("driver");
		int j= 0;
		j=0;
		String logCat = request.getParameter("LOGCAT")==null?"":request.getParameter("LOGCAT");
		String eventsLog = request.getParameter("EVENTSLOG");
		String androidVersion = request.getParameter("ANDROID_VERSION");
		String appVersion = request.getParameter("APP_VERSION_NAME");
		String stackTrace = request.getParameter("STACK_TRACE");
		String reportID = request.getParameter("REPORT_ID");
		String customFields = request.getParameter("CUSTOM_DATA");
		//String sessionID = request.getParameter("SessionID");
		//System.out.println("Acra"+customFields);
		AcraDAO.insertError("", "", androidVersion, appVersion, logCat, stackTrace, reportID, customFields);
	}	
}
