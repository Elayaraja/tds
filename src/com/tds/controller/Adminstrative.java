package com.tds.controller;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sourceforge.sizeof.SizeOf;

import com.common.util.SendingSMS;
import com.tds.tdsBO.ApplicationPoolBO;
/**
 * Servlet implementation class Adminstrative
 */
public class Adminstrative extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Adminstrative() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProcess(request, response);		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		doProcess(request, response);		
	}

	protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Enumeration variable = getServletConfig().getServletContext().getAttributeNames();
		while(variable.hasMoreElements()){
		//System.out.println( variable.toString() + SizeOf.sizeOf(variable.nextElement()));
		}
		SizeOf.skipStaticField(false); //java.sizeOf will not compute static fields
		SizeOf.skipFinalField(false); //java.sizeOf will not compute final fields
		SizeOf.skipFlyweightObject(false); //java.sizeOf will not compute well-known flyweight objects
		//System.out.println(SizeOf.deepSizeOf(getServletConfig().getServletContext().getAttribute("100Zones"))); //this will print the object size in bytes
		ApplicationPoolBO poolBO = (ApplicationPoolBO)getServletContext().getAttribute("poolBO");

		SendingSMS sms;
		sms = new SendingSMS(poolBO.getSMTP_HOST_NAME(),poolBO.getSMTP_AUTH_USER(),poolBO.getSMTP_AUTH_PWD(),poolBO.getEmailFromAddress(),poolBO.getSMTP_PORT(),poolBO.getProtocol());
		try{
		String [] addresses = {"ram@888getacab.com", "ram_krishnamoorthy@yahoo.com"};
		//sms.sendMail("ram@888getacab.com", "From Ram");
		sms.sendMail(addresses, "Hare Ram");
		} catch (Exception e){
			System.out.println(e.toString());
			
		}
	}

}
