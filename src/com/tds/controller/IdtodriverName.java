package com.tds.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Category;
import org.apache.woden.wsdl20.Description;

import com.tds.dao.UtilityDAO;
import com.tds.tdsBO.AdminRegistrationBO;

/**
 * @author vimal
 * @see Description
 * This Class is used to service for mobile device.
 *	
 */
public class IdtodriverName extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
private final Category cat = TDSController.cat;
	
	
	@Override
	public void init(ServletConfig config) throws ServletException {
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("Class Name "+ IdtodriverName.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		super.init(config);
	}
	
	
	
	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	
	@Override
	public void doGet(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException,IOException {
		doProcess(p_request, p_response);
	}
	
	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public void doPost(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException,IOException {
		doProcess(p_request, p_response);
	}
	
	/**
	 * @author vimal
	 * @param p_request
	 * @param p_response
	 * @throws ServletException
	 * @throws IOException
	 * @see Description
	 * This method is used to forward to particular action.
	 */
	public void doProcess(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException,IOException {
		
		//System.out.println("EVENT:     :"+p_request.getParameter("event"));
		//System.out.println("DRIVER ID:::"+p_request.getParameter("Driverid"));
		
		if(p_request.getParameter("event")!=null && (p_request.getParameter("event").equalsIgnoreCase("zone"))) {
			ArrayList al_list = new ArrayList();
			//System.out.println("ZONE LIST AJAX DAO"); 
			String Did=p_request.getParameter("Driverid");
			String assocode = ((AdminRegistrationBO)(p_request.getSession()).getAttribute("user")).getAssociateCode();
			al_list = UtilityDAO.getzonelist(Did, assocode);
			
			for(int i=0;i<al_list.size();i++) {
				//System.out.println("DATA::"+al_list.get(i));
			}
			PrintWriter out = p_response.getWriter();
			StringBuffer buffer = new StringBuffer();
			buffer.append("<select name='zone' id='zone'>");
			for(int i=0;i<al_list.size();i=i+2) {
				buffer.append("<option value="+al_list.get(i)+">"+al_list.get(i+1)+"</option>");
			
			}
			buffer.append("</select>");
			//System.out.println("buffer string"+buffer.toString());
			out.print(buffer.toString()); 
			
			
		} else {
			//System.out.println(" DRVER LIST  ELSE  AJAX DAO");
			
			String Did=p_request.getParameter("Driverid");
			String assocode = ((AdminRegistrationBO)(p_request.getSession()).getAttribute("user")).getAssociateCode();
			PrintWriter out = p_response.getWriter();
			//System.out.println("sssssssssssssssssssssss"+UtilityDAO.IdtodriverName(Did));
			out.print(UtilityDAO.IdtodriverName(Did,assocode)); 
		}
	
}
}
