package com.tds.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.axis.types.URI.MalformedURIException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.lowagie.text.pdf.codec.Base64;
import com.tds.action.JasperReportViewer;
import com.tds.cmp.bean.CompanyFlagBean;
import com.tds.dao.AdministrationDAO;
import com.tds.tdsBO.AdminRegistrationBO;
import com.common.util.TDSConstants;

/**
 * Servlet implementation class ReportController
 */
public class ReportController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ReportController() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProcess(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProcess(request,response);
	}
	protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("Coming In Report Control");
		String eventParam = "";
		
		AdminRegistrationBO adminBo = (AdminRegistrationBO)request.getSession().getAttribute("user");
		if(request.getParameter(TDSConstants.eventParam) != null) {
			eventParam = request.getParameter(TDSConstants.eventParam);
		}
		if(eventParam.equalsIgnoreCase("reportEvent") && request.getParameter("type")!=null && request.getParameter("type").equals("basedMail")) {
			ReportWithoutSession(request,response);
		}
		else if((AdminRegistrationBO)request.getSession().getAttribute("user")!=null && adminBo.getRoles().contains("4000"))
		{
			if(eventParam.equalsIgnoreCase("reportEvent")) {
					Report(request,response);
				}else if(eventParam.equalsIgnoreCase("getSplFlags")){
					getSplFlags(request,response);
				}
		} else if(eventParam.equalsIgnoreCase("reportEvent") && request.getParameter("type")!=null&&request.getParameter("type").equals("invoicePdf")){
			reportForInvoice(request,response);
		}
		else {
			RequestDispatcher requestDispatcher = request.getRequestDispatcher(TDSConstants.securityURL);
			requestDispatcher.forward(request, response);
		}
	}
	private void reportForInvoice(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// TODO Auto-generated method stub
		if(request.getParameter("invoiceNo")!=null){
			String assoccode = request.getParameter("assoccode");
			String masterAssoccode = request.getParameter("master");
			String timeOffSet = request.getParameter("timeOffSet");
			String output = request.getParameter("output");
			String invoiceNo =request.getParameter("invoiceNo");
			String reportName = "/Report/"+request.getParameter("ReportName");
			
			try {
				HashMap hm = new HashMap();
				hm.put("reportUnit", reportName);
				hm.put("invoiceNo", invoiceNo);
				hm.put("assocode", assoccode);
				hm.put("companyCode",masterAssoccode);
				hm.put("timeOffset", timeOffSet);
				hm.put("output", output);
				
				OutputStream out = JasperReportViewer.doProcess(response, hm);
				
			} catch (MalformedURIException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		}
	}

	public void ReportWithoutSession(HttpServletRequest request, HttpServletResponse response) {
		String name = request.getParameter("ReportName");
		String tripId = request.getParameter("tripId");
		String output = request.getParameter("output");
		String assoccode = request.getParameter("assoccode");
		String timeZone = request.getParameter("timeOffset");
		try {
				HashMap hm = new HashMap();
				hm.put("reportUnit", "/Report/"+name);
				hm.put("tripId", tripId);
				hm.put("assocode", assoccode);
				hm.put("timeOffset", timeZone);
				hm.put("output", output);
				OutputStream out = JasperReportViewer.doProcess(response, hm);
				byte[] bytes = ((ByteArrayOutputStream) out).toByteArray();
				String string_Response = Base64.encodeBytes(bytes);
				System.out.println("Reportsession String"+string_Response);
				//response.getWriter().write(out+"");
		} catch (IOException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	public void Report(HttpServletRequest request, HttpServletResponse response) {
		AdminRegistrationBO adminBo = (AdminRegistrationBO)request.getSession().getAttribute("user");
		String f1 ="";
		String f2 ="";
		String f3 ="";
		String f4 ="";
		String f5 ="";
		String flags="";
		String name = request.getParameter("ReportName");
		String fromDate= request.getParameter("fromDate"); 
		String toDate= request.getParameter("toDate");
		String type = request.getParameter("type");
		String tripId = request.getParameter("tripId");
		String cDate = request.getParameter("OR_ENTEREDTIME");
		String voucherNo = request.getParameter("voucherNo");
		String currentDate = request.getParameter("date");
		String output = request.getParameter("output");
		if(request.getParameter("flags")!=null){
			flags= request.getParameter("flags"); 
//			String[] flagArr = flags.split(",");
//			f1 = flagArr[0]!=null?flagArr[0] :"";
//			if(flagArr[0]!=null){
//				f2 = flagArr[1]!=null?flagArr[1] :flagArr[0];
//				f3 = flagArr[2]!=null?flagArr[2] :flagArr[0];
//				f4 = flagArr[3]!=null?flagArr[3] :flagArr[0];
//				f5 = flagArr[4]!=null?flagArr[4] :flagArr[0];
//			}
		}
		try {
			if(request.getParameter("invoiceNo")!=null){
				String invoiceNo =request.getParameter("invoiceNo");
				String reportName = "/Report/"+request.getParameter("ReportName");
				HashMap hm = new HashMap();
				hm.put("reportUnit", reportName);
				hm.put("invoiceNo", invoiceNo);
				hm.put("assocode", adminBo.getAssociateCode());
				hm.put("companyCode",adminBo.getMasterAssociateCode());
				hm.put("timeOffset", adminBo.getTimeZone());
				hm.put("output", output);
				OutputStream out = JasperReportViewer.doProcess(response, hm);
			}
			if(request.getParameter("multipleInvoiceNo")!=null){
				String invoiceNo =request.getParameter("multipleInvoiceNo");
				String reportName = "/Report/"+request.getParameter("ReportName");
				
				//System.out.println("invoiceNo:"+invoiceNo);
				//System.out.println("reportname:"+reportName);
				
				/*HashMap hm = new HashMap();
				hm.put("reportUnit", reportName);
				hm.put("invoiceNo", invoiceNo);
				hm.put("assocode", adminBo.getAssociateCode());
				hm.put("companyCode",adminBo.getMasterAssociateCode());
				hm.put("timeOffset", adminBo.getTimeZone());
				hm.put("output", output);
				OutputStream out = JasperReportViewer.doProcess(response, hm);*/
			}
			if(request.getParameter("routeNum")!=null){
				String routeNum =request.getParameter("routeNum");
				String reportName = "/Report/"+request.getParameter("ReportName");
				HashMap hm = new HashMap();
				hm.put("reportUnit", reportName);
				hm.put("routeNum", routeNum);
				hm.put("assocode", adminBo.getAssociateCode());
				hm.put("timeOffset", adminBo.getTimeZone());
				hm.put("output", output);
				OutputStream out = JasperReportViewer.doProcess(response, hm);
			}
			if(type!=null&&type.equals("fromAndToDate")){
				HashMap hm = new HashMap();
				hm.put("reportUnit", "/Report/"+name);
				hm.put("startDate", fromDate);
				hm.put("endDate", toDate);
				hm.put("assocode", adminBo.getAssociateCode());
				hm.put("timeOffset", adminBo.getTimeZone());
				hm.put("output",output);
				OutputStream out = JasperReportViewer.doProcess(response, hm);
			}else if(type!=null&&type.equals("fromAndToDateNew")){
				HashMap hm = new HashMap();
				hm.put("reportUnit", "/Report/"+name);
				hm.put("startDate", fromDate);
				hm.put("endDate", toDate);
				hm.put("assocode", adminBo.getMasterAssociateCode());
				hm.put("timeOffset", adminBo.getTimeZone());
				hm.put("output",output);
				OutputStream out = JasperReportViewer.doProcess(response, hm);
			}else if(type!=null&&type.equals("oneDate")){
				HashMap hm = new HashMap();
				hm.put("reportUnit", "/Report/"+name);
				hm.put("date", currentDate);
				hm.put("assocode", adminBo.getAssociateCode());
				hm.put("timeOffset", adminBo.getTimeZone());
				hm.put("output", output);
				OutputStream out = JasperReportViewer.doProcess(response, hm);
			}else if(type!=null&&type.equals("basedTrip")){
				HashMap hm = new HashMap();
				hm.put("reportUnit", "/Report/"+name);
				hm.put("tripId", tripId);
				hm.put("assocode", adminBo.getAssociateCode());
				hm.put("timeOffset", adminBo.getTimeZone());
				hm.put("output", output);
				OutputStream out = JasperReportViewer.doProcess(response, hm);
			}else if(type!=null&&type.equals("basedMail")){
				HashMap hm = new HashMap();
				hm.put("reportUnit", "/Report/"+name);
				hm.put("tripId", tripId);
				hm.put("assocode", adminBo.getMasterAssociateCode());
				hm.put("timeOffset", adminBo.getTimeZone());
				hm.put("output", output);
				OutputStream out = JasperReportViewer.doProcess(response, hm);
			}else if(type!=null&&type.equals("flags")){
				HashMap hm = new HashMap();
				hm.put("reportUnit", "/Report/"+name);
				hm.put("startDate", fromDate);
				hm.put("endDate", toDate);
				hm.put("flags", flags);
//				hm.put("f2", f2);
//				hm.put("f3", f3);
//				hm.put("f4", f4);
//				hm.put("f5", f5);
				hm.put("assocode", adminBo.getAssociateCode());
				hm.put("timeOffset", adminBo.getTimeZone());
				hm.put("output", output);
				OutputStream out = JasperReportViewer.doProcess(response, hm);
			}else if(type!=null&&type.equals("fromAndToDateVoucher")){
				HashMap hm = new HashMap();
				hm.put("reportUnit", "/Report/"+name);
				hm.put("startDate", fromDate);
				hm.put("endDate", toDate);
				hm.put("voucherNo", voucherNo);
				hm.put("assocode", adminBo.getAssociateCode());
				hm.put("timeOffset", adminBo.getTimeZone());
				hm.put("output", output);
				JasperReportViewer.doProcess(response, hm);
			}
		} catch (IOException e) {
			// TODO: handle exception
			e.printStackTrace();
		}catch (Exception ex) {
			// TODO: handle exception
			ex.printStackTrace();
		}
	}
	public void getSplFlags(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException{
		AdminRegistrationBO adminBo = (AdminRegistrationBO)request.getSession().getAttribute("user");
		ArrayList<CompanyFlagBean> arList = AdministrationDAO.getSplFlags(adminBo.getAssociateCode());
		JSONArray array = new JSONArray();
		for(int i=0;i<arList.size();i++){
			JSONObject flags = new JSONObject();
				try {
					flags.put("FV",arList.get(i).getFlag1());
					flags.put("FD",arList.get(i).getFlag1_lng_desc());
					array.put(flags);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		String respString = array.toString();
		response.getWriter().write(respString.toString());
	}
}