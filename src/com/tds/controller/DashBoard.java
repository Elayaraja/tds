package com.tds.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.common.util.Messaging;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.charges.bean.ChargesBO;
import com.charges.dao.ChargesDAO;
import com.common.util.Feed;
import com.tds.cmp.bean.DriverCabQueueBean;
import com.tds.cmp.bean.DriverDeactivation;
import com.tds.cmp.bean.DriverVehicleBean;
import com.tds.cmp.bean.SMSBean;
import com.tds.cmp.bean.ZoneTableBeanSP;
import com.tds.dao.AdministrationDAO;
import com.tds.dao.AuditDAO;
import com.tds.dao.ConsoleDAO;
import com.tds.dao.DispatchDAO;
import com.tds.dao.PhoneDAO;
import com.tds.dao.RegistrationDAO;
import com.tds.dao.RequestDAO;
import com.tds.dao.SecurityDAO;
import com.tds.dao.ServiceRequestDAO;
import com.tds.dao.SharedRideDAO;
import com.tds.dao.SystemPropertiesDAO;
import com.tds.dao.UtilityDAO;
import com.tds.dao.VoiceMessageDAO;
import com.tds.dao.ZoneDAO;
import com.tds.ivr.IVRDriverCaller;
import com.common.util.CheckZone;
import com.tds.process.QueueProcess;
import com.tds.security.bean.TagSystemBean;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.ApplicationPoolBO;
import com.tds.tdsBO.ConsoleBO;
import com.tds.tdsBO.DriverLocationBO;
import com.tds.tdsBO.DriverRegistrationBO;
import com.tds.tdsBO.FleetBO;
import com.tds.tdsBO.MessageBO;
import com.tds.tdsBO.OpenRequestBO;
import com.tds.tdsBO.QueueBean;
import com.tds.tdsBO.QueueCoordinatesBO;
import com.tds.tdsBO.ZoneRateBean;
import com.common.util.MessageGenerateJSON;
import com.common.util.sendSMS;
import com.common.util.OpenRequestUtil;
import com.common.util.SystemUtils;
import com.common.util.TDSConstants;
import com.common.util.TDSValidation;
import com.gac.mobile.dao.MobileDAO;
import com.twilio.sdk.TwilioRestException;

/**
 * Servlet implementation class DashBoard
 * 
 * 
 */
public class DashBoard extends HttpServlet {
	private static final long serialVersionUID = 1L;
	ServletConfig serveletConfig;
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DashBoard() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doProcess(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doProcess(request, response);
	}

	protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		String eventParam = "";
		if (request.getParameter(TDSConstants.eventParam) != null) {
			eventParam = request.getParameter(TDSConstants.eventParam);
			//System.out.println("Event in DAshboard: "+eventParam);
		}
		if ((AdminRegistrationBO) request.getSession().getAttribute("user") != null) {
			if (eventParam.equalsIgnoreCase("dispatchValues")) {
				dispatchValues(request, response);
			} else if (eventParam.equalsIgnoreCase("driverDetails")) {
				driverdetails(request, response);
			} else if (eventParam.equalsIgnoreCase("getAllDriverDetails")) {
				getAllDriverDetails(request, response);
			}else if (eventParam.equalsIgnoreCase("search")) {
				searchZone(request, response);
			} else if (eventParam.equalsIgnoreCase("logout")) {
				logoutDriver(request, response);
			} else if (eventParam.equalsIgnoreCase("deleteJob")) {
				deleteJob(request, response);
			} else if (eventParam.equalsIgnoreCase("deleteMJob")) {
				deleteMJob(request, response);
			} else if (eventParam.equalsIgnoreCase("jobDetails")) {
				jobDetails(request, response);
			}else if (eventParam.equalsIgnoreCase("drivComplete")) {
				drivComplete(request, response);
			}else if (eventParam.equalsIgnoreCase("operCancel")) {
				operCancel(request, response);
			} else if (eventParam.equalsIgnoreCase("consoleView")) {
				consoleView(request, response);
			} else if (eventParam.equalsIgnoreCase("messageConsole")) {
				messageConsole(request, response);
			} else if (eventParam.equalsIgnoreCase("deactivate")) {
				deactivate(request, response);
			} else if (eventParam.equalsIgnoreCase("TimeUpdate")) {
				timeUpdate(request, response);
			} else if (eventParam.equalsIgnoreCase("jobCommands")) {
				jobCommands(request, response);
			}else if (eventParam.equalsIgnoreCase("multiplejobCommands")) {
				multiplejobCommands(request, response);
			} else if (eventParam.equalsIgnoreCase("searchDriver")) {
				searchDriver(request, response);
			} else if (eventParam.equalsIgnoreCase("driverAutoComplete")) {
				driverAutoComplete(request, response);
			} else if (eventParam.equalsIgnoreCase("deleteJobSummary")) {
				deleteJobSummary(request, response);
			} else if (eventParam.equalsIgnoreCase("jobCount")) {
				getJobCount(request, response);
			} else if (eventParam.equalsIgnoreCase("sendSMS")) {
				sendIndividualMessageToDriverFromJob(request, response);
			} else if (eventParam.equalsIgnoreCase("getCompanyLogo")) {
				getCompanyLogo(request, response);
			} else if (eventParam.equalsIgnoreCase("sendToNearestDrivers")) {
				sendToNearestDrivers(request, response);
			} else if (eventParam.equalsIgnoreCase("jobsForCurrentDay")) {
				jobsForCurrentDay(request, response);
			} else if (eventParam.equalsIgnoreCase("checkCount")) {
				checkCount(request, response);
				// }else if(eventParam.equalsIgnoreCase("checkUser")) {
				// checkUser(request,response);
			} else if (eventParam.equalsIgnoreCase("bookToZone")) {
				bookToZone(request, response);
			} else if (eventParam.equalsIgnoreCase("messageHistory")) {
				messageHistory(request, response);
			} else if (eventParam.equalsIgnoreCase("jobHistory")) {
				jobHistory(request, response);
			} else if (eventParam.equalsIgnoreCase("makeivrphone")) {
				makeIVRPhoneCall(request, response);
			} else if (eventParam.equalsIgnoreCase("sharedRide")) {
				sharedRide(request, response);
			} else if (eventParam.equalsIgnoreCase("getSharedRide")) {
				getSharedRide(request, response);
			} else if (eventParam.equalsIgnoreCase("getSharedSummary")) {
				getSharedSummary(request, response);
			} else if (eventParam.equalsIgnoreCase("sharedRideAjax")) {
				sharedRideAjax(request, response);
			} else if (eventParam.equalsIgnoreCase("updateDriverForShared")) {
				updateDriverForShared(request, response);
			} else if (eventParam.equalsIgnoreCase("deleteGroup")) {
				deleteGroup(request, response);
			} else if (eventParam.equalsIgnoreCase("updateSharedStatus")) {
				updateSharedStatus(request, response);
			} else if (eventParam.equalsIgnoreCase("ajaxSharedLogs")) {
				ajaxSharedLogs(request, response);
			} else if (eventParam.equalsIgnoreCase("changeJobToAnotherFleet")) {
				changeJobToAnotherFleet(request, response);
			}else if (eventParam.equalsIgnoreCase("changeMultipleJobToAnotherFleet")) {
				changeMultipleJobToAnotherFleet(request, response);
			} else if (eventParam.equalsIgnoreCase("submitCookies")) {
				submitCookies(request, response);
			} else if (eventParam.equalsIgnoreCase("errorReport")) {
				errorReport(request, response);
			} else if (eventParam.equalsIgnoreCase("splitTripFromGroup")) {
				splitTripFromGroup(request, response);
			} else if (eventParam.equalsIgnoreCase("addTripToGroup")) {
				addTripToGroup(request, response);
			} else if (eventParam.equalsIgnoreCase("zoneDetailsForDash")) {
				zoneDetailsForDash(request, response);
			} else if (eventParam.equalsIgnoreCase("allZonesForDash")) {
				try {
					allZonesForDash(request, response);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else if (eventParam.equalsIgnoreCase("createIndividualTrips")) {
				createIndividualTrips(request, response);
			} else if (eventParam.equalsIgnoreCase("messagefordrivers")) {
				messageForDrivers(request, response);
			}

			else if (eventParam.equalsIgnoreCase("positioningDriver")) {
				positioningDriver(request, response);
			} else if (eventParam.equalsIgnoreCase("loginDriverFromDashboard")) {
				loginDriverFromDashboard(request, response);
			} else if (eventParam.equalsIgnoreCase("flagTripFromDash")) {
				flagTripFromDash(request, response);
			} else if (eventParam.equalsIgnoreCase("setDriverAvailability")) {
				setDriverAvailability(request, response);
			} else if (eventParam.equalsIgnoreCase("activateDriver")) {
				activateDriver(request, response);
			} else if (eventParam.equalsIgnoreCase("setEndZone")) {
				setEndZone(request, response);
			} else if (eventParam.equalsIgnoreCase("onRouteFromDash")) {
				onRouteFromDash(request, response);
			} else if (eventParam.equalsIgnoreCase("cannedMsg")) {
				cannedMsg(request, response);
			} else if (eventParam.equalsIgnoreCase("getZonerates")) {
				getZonerates(request, response);
			} else if (eventParam.equalsIgnoreCase("getDriverZoneActivity")) {
				getDriverZoneActivity(request, response);
			} else if (eventParam.equalsIgnoreCase("createMsg")) {
				createMsg(request, response);
			} else if (eventParam.equalsIgnoreCase("removeMsg")) {
				removeMsg(request, response);
			} else if (eventParam.equalsIgnoreCase("resetDriverApp")) {
				resetDriverApp(request, response);
			} else if (eventParam.equalsIgnoreCase("approveCabNumberToDevice")) {
				approveCabNumberToDevice(request, response);
			} else if (eventParam.equalsIgnoreCase("makeMsgRead")) {
				makeMsgRead(request, response);
			}else if (eventParam.equalsIgnoreCase("makeSmsRead")) {
				makeSmsRead(request, response);
			} else if (eventParam.equalsIgnoreCase("individualMsgHistory")) {//mobilesms
				individualMsgHistory(request, response);
			} else if (eventParam.equalsIgnoreCase("splitTrips")) {
				splitTrips(request, response);
			} else if (eventParam.equalsIgnoreCase("getAllValuesForDashboard")) {
				getAllValuesForDashboard(request, response);
			}else if (eventParam.equalsIgnoreCase("changeZoneForDash")) {
				changeZoneForDash(request, response);
			}else if (eventParam.equalsIgnoreCase("changeRatingJobs")) {
				changeRatingJobs(request, response);
			}else if (eventParam.equalsIgnoreCase("remDntDispatchjobs")) {
				remDntDispatchjobs(request, response);
			}else if (eventParam.equalsIgnoreCase("redispatchMjob")) {
				redispatchMjob(request, response);
			}else if (eventParam.equalsIgnoreCase("mobilesms")) {
				mobilesms(request, response);
			}else if (eventParam.equalsIgnoreCase("mobilesmsHistory")) {
				mobilesmsHistory(request, response);
			}

		} else {
			response.getWriter().write("lo");
			if (eventParam.equalsIgnoreCase("dispatchValues")) {
				String forwardURL = TDSConstants.getMainWithoutSessionJSP;
				RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
				requestDispatcher.forward(request, response);
			}
		}

	}

	private void getDriverZoneActivity(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("getDriverZoneActivity");
		// TODO Auto-generated method stub
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		ArrayList<QueueBean> dzaList = new ArrayList<QueueBean>();
		String zoneId = request.getParameter("zoneId")!=null?request.getParameter("zoneId"):"";
		String driverId = request.getParameter("driverId")!=null?request.getParameter("driverId"):"";
		
		String fDate = request.getParameter("fromDate")!=null?request.getParameter("fromDate"):"";
		String tDate = request.getParameter("toDate")!=null?request.getParameter("toDate"):"";
		String fromDateFormatted="";
		String toDateFormatted="";
		if(!fDate.equals("") && !tDate.equals("")){
			fromDateFormatted=TDSValidation.getTimeStampDateFormat(fDate);
			toDateFormatted=TDSValidation.getTimeStampDateFormat(tDate);
		}
		
		System.out.println("ZoneId:"+zoneId+" --- DriverId:"+driverId);
		
		dzaList = ZoneDAO.getDriverZoneActivity(adminBO, zoneId, driverId,fromDateFormatted,toDateFormatted);
		if(dzaList!=null && dzaList.size()>0){
			JSONArray dzaArray = new JSONArray();
			try{
				for(int i=0; i<dzaList.size(); i++){
					QueueBean qBO = dzaList.get(i);
					JSONObject dzaObject = new JSONObject();
					dzaObject.put("D", qBO.getDriverId());
					dzaObject.put("Z", qBO.getQU_NAME());
					dzaObject.put("V", qBO.getVehicleNo());
					dzaObject.put("LI", qBO.getDq_LoginTime());
					dzaObject.put("LO", qBO.getDq_LogoutTime());
					dzaObject.put("S", qBO.getStatus());
					dzaObject.put("R", qBO.getReason());
					dzaArray.put(dzaObject);
				}
			}
			catch(JSONException e){
				e.printStackTrace();
			}
			response.getWriter().write(dzaArray.toString());
		}else{
			response.getWriter().write("");
		}
	}

	private void getZonerates(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// TODO Auto-generated method stub
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		ArrayList<ZoneRateBean> al_list = ZoneDAO.getZoneRate(adminBO.getMasterAssociateCode());
		ArrayList<QueueCoordinatesBO> zonesList = ZoneDAO.getzoneList(adminBO.getMasterAssociateCode());

		JSONArray rateArray = new JSONArray();
		try {
			for(int i=0; i<al_list.size();i++){
				ZoneRateBean znRateBean = al_list.get(i);
				JSONObject rateObject = new JSONObject();
				boolean foundSZ=false,foundEZ=false;
				for(int j=0;j<zonesList.size();){
					if(!foundSZ && znRateBean.getStartzone().equals(zonesList.get(j).getQueueId())){
						rateObject.put("SZDesc", zonesList.get(j).getQDescription());
						foundSZ=true;
					}
					if(!foundEZ && znRateBean.getEndzone().equals(zonesList.get(j).getQueueId())){
						rateObject.put("EZDesc", zonesList.get(j).getQDescription());
						foundEZ=true;
					}
					if(foundSZ && foundEZ){
						j=zonesList.size();
					}else{
						j++;
					}
				}
				rateObject.put("SZ", znRateBean.getStartzone());
				rateObject.put("EZ", znRateBean.getEndzone());
				rateObject.put("Rate", znRateBean.getAmount());

				rateArray.put(rateObject);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//System.out.println("response: "+rateArray.toString());
		response.getWriter().write(rateArray.toString());
	}

	public void dispatchValues(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		if (adminBO.getRoles().contains("1036")) {
			String defZOne = AdministrationDAO.loadDefaultZone(adminBO.getMasterAssociateCode());
			// ArrayList al_list =
			// AdministrationDAO.getOpnReqDash(adminBO.getAssociateCode(), 0,
			// adminBO.getTimeZone(),0, "",0);
			// request.setAttribute("DefaultZone", defZOne);
			// request.setAttribute("Jobs", al_list);
			// ArrayList<DriverLocationBO> drivers =
			// UtilityDAO.getDriverLocationListByStatus(adminBO.getAssociateCode(),1,adminBO.getTimeZone());
			// request.setAttribute("Drivers", drivers);
			HashMap<String, DriverVehicleBean> companyFlags = (HashMap<String, DriverVehicleBean>) getServletContext().getAttribute(adminBO.getMasterAssociateCode() + "Flags");
			if (companyFlags == null) {
				SystemUtils.reloadCompanyFlags(this, adminBO.getMasterAssociateCode());
				companyFlags = (HashMap<String, DriverVehicleBean>) getServletContext().getAttribute(adminBO.getMasterAssociateCode() + "Flags");
			}
			request.setAttribute("companyFlags", companyFlags);
			ArrayList drFlag = new ArrayList();
			ArrayList vehFlag = new ArrayList();
			Set hashMapEntries = companyFlags.entrySet();
			Iterator it = hashMapEntries.iterator();
			while (it.hasNext()) {
				Map.Entry<String, DriverVehicleBean> companyFlag = (Map.Entry<String, DriverVehicleBean>) it.next();
				if (companyFlag.getValue().getDriverVehicleSW() == 1) {
					drFlag.add(companyFlag.getValue().getShortDesc());
					drFlag.add(companyFlag.getValue().getKey());
					drFlag.add(companyFlag.getValue().getLongDesc());
				} else {
					vehFlag.add(companyFlag.getValue().getShortDesc());
					vehFlag.add(companyFlag.getValue().getKey());
					vehFlag.add(companyFlag.getValue().getLongDesc());
				}
			}
			ArrayList<TagSystemBean> al_listCookie = RequestDAO.readCookiesForAll(adminBO.getUid(), adminBO.getMasterAssociateCode());

			request.setAttribute("al_q", CheckZone.returnZonesForAssociatoinCode(getServletContext(), adminBO.getMasterAssociateCode()));

			VoiceMessageDAO.updateAllMessagesAsRead(adminBO.getMasterAssociateCode(), adminBO.getUid());
			ArrayList<ChargesBO> chargeType = ChargesDAO.getChargeTypes(adminBO.getMasterAssociateCode(),0);
	        Collections.rotate(chargeType, -1);
			request.setAttribute("chargeTypes", chargeType);
			request.setAttribute("vecFlag", vehFlag);
			request.setAttribute("drFlag", drFlag);
			request.setAttribute("CookieValues", al_listCookie);

			if (request.getParameter("map") != null && request.getParameter("map").equalsIgnoreCase("osm")) {
				request.setAttribute("dashboard", "OSM");
				getServletContext().getRequestDispatcher("/Company/DashBoardOSM.jsp").forward(request, response);
			} else {
				request.setAttribute("dashboard", "google");
				getServletContext().getRequestDispatcher("/Company/DashBoard.jsp").forward(request, response);

			}
		}
	}

	public void driverdetails(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		ArrayList<DriverLocationBO> drivers = new ArrayList<DriverLocationBO>();
		// long value="";
		long lastTime = -1;
		if (request.getParameter("status").equals("4")) {
			drivers = UtilityDAO.getDriverLocationListByStatus(adminBO.getAssociateCode(), 4, adminBO.getTimeZone(), "");
		} else if (request.getParameter("status").equals("3")) {
			drivers = UtilityDAO.getDriverLocationListByStatus(adminBO.getAssociateCode(), 3, adminBO.getTimeZone(), "");
		} else if (request.getParameter("status").equals("5")) {
			drivers = UtilityDAO.getDriverLocationListByStatus(adminBO.getAssociateCode(), 5, adminBO.getTimeZone(), "");
		} else {
			drivers = UtilityDAO.getDriverLocationListByStatus(adminBO.getAssociateCode(), 1, adminBO.getTimeZone(), "");
		}
		if (getServletConfig().getServletContext().getAttribute(adminBO.getAssociateCode() + "Timer") != null) {
			// value=(String)getServletConfig().getServletContext().getAttribute("lastRunTime");
			QueueProcess queueProcess = (QueueProcess) this.getServletConfig().getServletContext().getAttribute(adminBO.getAssociateCode() + "Process");
			if (queueProcess.getLastRunTime() > 0) {
				lastTime = (System.currentTimeMillis() - queueProcess.getLastRunTime()) / 1000;
				// System.out.println("AssocCode" + adminBO.getAssociateCode()+
				// "LastRunTime" + queueProcess.getLastRunTime() + ":"+
				// System.currentTimeMillis());
			} else {
				// System.out.println("AssocCode" + adminBO.getAssociateCode()+
				// "Dispatch not running");

			}
			// lastTime = (System.currentTimeMillis()-lastTime)/1000;
		}
		JSONArray driverList = new JSONArray();
		JSONObject driverFirst = new JSONObject();
		try {
			driverFirst.put("varDr", "");
			driverFirst.put("varLt", "");
			driverFirst.put("varLg", "");
			driverFirst.put("varSms", "");
			driverFirst.put("varPh", "");
			driverFirst.put("varDp", "");
			driverFirst.put("varSwh", "");
			driverFirst.put("varVNo", "");
			driverFirst.put("varDorV", "");
			driverFirst.put("varDrNa", "");
			driverFirst.put("varAg", "");
			driverFirst.put("varLT", "");
			driverFirst.put("varAppV", "");

			driverFirst.put("lastRunTime", lastTime);
			// driverFirst.put("lastUpDate","");
			driverList.put(driverFirst);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		for (int i = 0; i < drivers.size(); i++) {
			JSONObject driver = new JSONObject();
			try {
				driver.put("varDr", drivers.get(i).getDriverid());
				driver.put("varLt", drivers.get(i).getLatitude());
				driver.put("varLg", drivers.get(i).getLongitude());
				driver.put("varSms", drivers.get(i).getSmsid());
				driver.put("varPh", drivers.get(i).getPhone());

				String driverFlagValue = SystemUtils.getCompanyFlagDescription(getServletConfig(), adminBO.getMasterAssociateCode(), drivers.get(i).getProfile() != null ? drivers.get(i).getProfile() : "", ";", 0);

				driver.put("varDp", driverFlagValue);
				driver.put("varSwh", drivers.get(i).getStatus());
				driver.put("varVNo", drivers.get(i).getVehicleNo());
				driver.put("varDrNa", drivers.get(i).getDrName());
				driver.put("varAg", drivers.get(i).getAge());
				driver.put("varAppV", drivers.get(i).getAppVersion() != null ? drivers.get(i).getAppVersion() : "");

				// driver.put("varDorV",
				// adminBO.getDispatchBasedOnVehicleOrDriver());
				driver.put("lastUpDate", drivers.get(i).getLastUpdatedMilliSeconds());
				driver.put("DD", drivers.get(i).getDistance());
				
				int loginTime = drivers.get(i).getLoginTime() / 60;
				int logintMinutes = drivers.get(i).getLoginTime() % 60;
				driver.put("varLT", "Total LoggedIn Time : " + loginTime + ":" + logintMinutes + " : Last LoggedIn Time : " + drivers.get(i).getLastLoginTime());

				// System.out.println("check value:"+drivers.get(i).getLastUpdatedMilliSeconds());
				driverList.put(driver);
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}
		response.getWriter().write(driverList.toString());
		/*
		 * request.setAttribute("Drivers",drivers);
		 * getServletContext().getRequestDispatcher
		 * ("/Company/DriverDetails.jsp").forward(request, response);
		 */
	}

	public void searchZone(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		String driver1 = request.getParameter("driver");
		QueueBean queue = new QueueBean();
		if (adminBO.getDispatchBasedOnVehicleOrDriver() == 1) {
			queue.setDriverId(driver1);
		} else {
			queue.setVehicleNo(driver1);
		}
		ArrayList<QueueBean> al_list = new ArrayList<QueueBean>();
		al_list = AdministrationDAO.getDriverinQDash(adminBO.getAssociateCode(), 0, queue, adminBO.getMasterAssociateCode());
		request.setAttribute("al_list", al_list);
		getServletContext().getRequestDispatcher("/Company/ZoneWaiting.jsp").forward(request, response);
	}

	public void logoutDriver(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
//		int result = 0;
		String driverid = "";
		String result1="";
		driverid = request.getParameter("driverid") == null ? "" : request.getParameter("driverid");
		String reason = request.getParameter("answer")==null?"":request.getParameter("answer");
		int status = (request.getParameter("driverStatus")!=null && !request.getParameter("driverStatus").equals(""))?(Integer.parseInt(request.getParameter("driverStatus"))):1;
//		String[] message = new String[2];
//		long msgID = System.currentTimeMillis();
//		ApplicationPoolBO poolBO = (ApplicationPoolBO) getServletContext().getAttribute("poolBO");
		DriverCabQueueBean cabQueueBean = new DriverCabQueueBean();
		cabQueueBean = DispatchDAO.getDriverByDriverID(adminBO.getAssociateCode(), driverid, status);
		result1 = ServiceRequestDAO.getQueueIdByDriverIDandAssoccode(adminBO.getAssociateCode(), driverid);
		if(!result1.equals("")){
			ServiceRequestDAO.Update_DriverLogOutfromQueue(driverid, cabQueueBean.getVehicleNo(), result1, adminBO.getAssociateCode(), "2", "Driver("+driverid+") logged out of the zone:"+result1+". Due to driver logged out by Operator "+adminBO.getUid());
		} 
		MobileDAO.removeSessionObjects("", driverid);
		MobileDAO.insertLogoutDetail("", driverid, adminBO.getUid(), "Logout From Dashboard By Operator",adminBO.getMasterAssociateCode());
		DispatchDAO.sendMessage("You are logged out for."+reason,driverid,adminBO,2);
		ServiceRequestDAO.deleteDriverFromQueue(driverid, adminBO.getMasterAssociateCode());
		RegistrationDAO.updateLogoutDriver(driverid, adminBO.getAssociateCode());
		ServiceRequestDAO.updateDriverAvailabilty("L", driverid, adminBO.getAssociateCode());
		
		//Send Push message
		System.out.println("Goig to send Logout message");
		ApplicationPoolBO poolBO = (ApplicationPoolBO) getServletContext().getAttribute("poolBO");
		String[] message = new String[2];
		message = MessageGenerateJSON.generatePrivateMessage("You are logged out by "+adminBO.getUname(), "LO", System.currentTimeMillis());
		if(cabQueueBean.getDriverid()!=null && !cabQueueBean.getDriverid().equals("")){
			Messaging oneSMS = new Messaging(getServletContext(), cabQueueBean, message, poolBO, adminBO.getAssociateCode());
			oneSMS.start();
		}
	}

	public void resetDriverApp(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String[] message = new String[2];
		long msgID = System.currentTimeMillis();
		message = MessageGenerateJSON.resetDriverApp(msgID);
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession(false).getAttribute("user");
		ApplicationPoolBO poolBO = (ApplicationPoolBO) getServletContext().getAttribute("poolBO");
		String driverID = request.getParameter("driverId");
		DriverCabQueueBean cabQueueBean = new DriverCabQueueBean();
		cabQueueBean = DispatchDAO.getDriverByDriverID(adminBO.getAssociateCode(), driverID, 1);
		if (driverID != null && !driverID.equals("")) {
			Messaging oneSMS = new Messaging(getServletContext(), cabQueueBean, message, poolBO, adminBO.getAssociateCode());
			oneSMS.start();
		}
	}

	public void approveCabNumberToDevice(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession(false).getAttribute("user");

		//System.out.println("approve cab numbe rto tag");
		//System.out.println("adminbo.gettagdevicecab"+adminBO.getTagDeviceCab());
		//if (adminBO.getTagDeviceCab() == 1) {
		String[] message = new String[2];
		long msgID = System.currentTimeMillis();
		String driverID = (request.getParameter("driverId") != "" ? request.getParameter("driverId") : null);
		String cabNumber = (request.getParameter("cabNo") != "" ? request.getParameter("cabNo") : null);
		System.out.println("Driver Id : CabNo-" + driverID + ":" + cabNumber);
		if (cabNumber != null && driverID != null) {
			message = MessageGenerateJSON.approveDevice(msgID, cabNumber);
			ApplicationPoolBO poolBO = (ApplicationPoolBO) getServletContext().getAttribute("poolBO");
			DriverCabQueueBean cabQueueBean = new DriverCabQueueBean();
			cabQueueBean = DispatchDAO.getDriverByDriverID(adminBO.getAssociateCode(), driverID, 1);
			Messaging oneSMS = new Messaging(getServletContext(), cabQueueBean, message, poolBO, adminBO.getAssociateCode());
			oneSMS.start();
			response.getWriter().write("1");
		} else {
			response.getWriter().write("0");
		}
		/*} else {
			response.getWriter().write("0");
		}*/
	}

	public void deleteJob(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		String reason = request.getParameter("answer");
		String tripID = request.getParameter("tripId");
		ApplicationPoolBO poolBO = (ApplicationPoolBO) getServletContext().getAttribute("poolBO");

		AdministrationDAO.updateOpenRequest(tripID, adminBO.getAssociateCode(), "1", "55", reason, adminBO.getUid());

		OpenRequestBO openBo = RequestDAO.getOpenRequestByTripID(tripID, adminBO.getAssociateCode(), adminBO.getTimeZone());
		try {
			if (openBo.getDriverid() != null && !openBo.getDriverid().equals("")) {
				DriverCabQueueBean cabQueueBean = DispatchDAO.getDriverByDriverID(adminBO.getAssociateCode(), openBo.getDriverid(), 1);
				String[] message = MessageGenerateJSON.generateMessage(openBo, "OC", System.currentTimeMillis(), adminBO.getAssociateCode());
				Messaging oneSMS = new Messaging(getServletContext(), cabQueueBean, message, poolBO, adminBO.getAssociateCode());
				oneSMS.start();
			}
		} catch (Exception e) {
			System.out.println("Error sending mesage to driver" + e.toString());
		}
		String jobsCode = DispatchDAO.getCompCode(tripID, adminBO.getMasterAssociateCode());
		if(jobsCode!=null && !jobsCode.equals("") && jobsCode.equals(adminBO.getMasterAssociateCode())){
			DispatchDAO.moveToHistory(tripID, adminBO.getAssociateCode(), "",adminBO.getCompanyList());
		} else {
			ArrayList<String> myString = new ArrayList<String>();
			DispatchDAO.moveToHistory(tripID, adminBO.getAssociateCode(), "",myString);
		}
//		DispatchDAO.moveToHistory(tripID, adminBO.getAssociateCode(), "");
		AuditDAO.insertJobLogs("Operator Cancel", request.getParameter("tripId"), adminBO.getAssociateCode(), 55, adminBO.getUid(), "", "", adminBO.getMasterAssociateCode());
		ArrayList al_list = AdministrationDAO.getOpnReqDash(adminBO.getAssociateCode(), 0, adminBO.getTimeZone(), 0, "", 0, 0);
		request.setAttribute("Jobs", al_list);
		getServletContext().getRequestDispatcher("/Company/JobDetails.jsp").forward(request, response);

	}

	public void deleteMJob(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		String reason = request.getParameter("answer");
		String tripID = request.getParameter("tripid");
		int status=0;
		ArrayList<OpenRequestBO> al_list = new ArrayList<OpenRequestBO>();
		String tripid[]=tripID.split(";");
		for(int i=0;i<tripid.length;i++){
			String trip=tripid[i];
           String routeNumber = DispatchDAO.getRouteNoUsingTripId(adminBO.getAssociateCode(), trip);
			SharedRideDAO.splitTripFromGroup(adminBO.getAssociateCode(), trip, routeNumber, "JC");
			status = AdministrationDAO.updateOpenRequest(trip, adminBO.getAssociateCode(), "1", TDSConstants.customerCancelledRequest + "", "CC", adminBO.getUid());
			ApplicationPoolBO poolBO = (ApplicationPoolBO) getServletContext().getAttribute("poolBO");
			OpenRequestBO openBo = RequestDAO.getOpenRequestByTripID(trip, adminBO.getAssociateCode(), adminBO.getTimeZone());

			if (openBo.getDriverid() != null && !openBo.getDriverid().equals("")) {
				DriverCabQueueBean cabQueueBean = DispatchDAO.getDriverByDriverID(adminBO.getAssociateCode(), openBo.getDriverid(), 1);
				String[] message = MessageGenerateJSON.generateMessage(openBo, "CC", System.currentTimeMillis(), adminBO.getAssociateCode());
				Messaging oneSMS = new Messaging(getServletContext(), cabQueueBean, message, poolBO, adminBO.getAssociateCode());
				oneSMS.start();
			}
			al_list = AdministrationDAO.getOpnReqDash(adminBO.getAssociateCode(), 0, adminBO.getTimeZone(), 0, "", 0, 0);
			String jobsCode = DispatchDAO.getCompCode(trip, adminBO.getMasterAssociateCode());
			if(jobsCode!=null && !jobsCode.equals("") && jobsCode.equals(adminBO.getMasterAssociateCode())){
				DispatchDAO.moveToHistory(trip, adminBO.getAssociateCode(), "",adminBO.getCompanyList());
			} else {
				ArrayList<String> myString = new ArrayList<String>();
				DispatchDAO.moveToHistory(trip, adminBO.getAssociateCode(), "",myString);
			}
//			DispatchDAO.moveToHistory(trip, adminBO.getAssociateCode(), "");
			AuditDAO.insertJobLogs("Customer Cancel", trip, adminBO.getAssociateCode(), 50, adminBO.getUid(), "", "", adminBO.getMasterAssociateCode());
		}
		response.getWriter().write(Integer.toString(status));

	}


	public void drivComplete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		ArrayList<OpenRequestBO> al_list = new ArrayList<OpenRequestBO>();
		int status=0;
		String reason = request.getParameter("answer");

		String tripId = request.getParameter("tripId");
		String tripid[]=tripId.split(";");
		for(int i=0;i<tripid.length;i++){
			String trip=tripid[i];

			String routeNumber = DispatchDAO.getRouteNoUsingTripId(adminBO.getAssociateCode(), trip);

			SharedRideDAO.splitTripFromGroup(adminBO.getAssociateCode(), trip, routeNumber, "JC");
			status = AdministrationDAO.updateOpenRequest(trip, adminBO.getAssociateCode(), "1", TDSConstants.tripCompleted + "", reason, adminBO.getUid());
			AuditDAO.insertJobLogs("Driver Completed", trip, adminBO.getAssociateCode(), 61, adminBO.getUid(), "", "", adminBO.getMasterAssociateCode());
			ApplicationPoolBO poolBO = (ApplicationPoolBO) getServletContext().getAttribute("poolBO");
			OpenRequestBO openBo = RequestDAO.getOpenRequestByTripID(trip, adminBO.getAssociateCode(), adminBO.getTimeZone());
			String driverid = openBo.getDriverid();
			if (driverid != null && !driverid.equals("")) {
				DriverCabQueueBean cabQueueBean = DispatchDAO.getDriverByDriverID(adminBO.getAssociateCode(), driverid, 1);
				String[] message = MessageGenerateJSON.generateMessage(openBo, "DC", System.currentTimeMillis(), adminBO.getAssociateCode());
				if (cabQueueBean.getDriverid() != null && !cabQueueBean.getDriverid().equals("")) {
					Messaging oneSMS = new Messaging(getServletContext(), cabQueueBean, message, poolBO, adminBO.getAssociateCode());
					oneSMS.start();
				}
			}
			al_list = AdministrationDAO.getOpnReqDash(adminBO.getAssociateCode(), 0, adminBO.getTimeZone(), 0, "", 0, 0);
			String jobsCode = DispatchDAO.getCompCode(trip, adminBO.getMasterAssociateCode());
			if(jobsCode!=null && !jobsCode.equals("") && jobsCode.equals(adminBO.getMasterAssociateCode())){
				DispatchDAO.moveToHistory(trip, adminBO.getAssociateCode(), "",adminBO.getCompanyList());
			} else {
				ArrayList<String> myString = new ArrayList<String>();
				DispatchDAO.moveToHistory(trip, adminBO.getAssociateCode(), "",myString);
			}
//			DispatchDAO.moveToHistory(trip, adminBO.getAssociateCode(), "");
		}
		response.getWriter().write(Integer.toString(status));

	}


	public void operCancel(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		String reason = request.getParameter("answer");
		String tripID = request.getParameter("tripId");
		int status=0;
		ArrayList<OpenRequestBO> al_list = new ArrayList<OpenRequestBO>();
		String tripid[]=tripID.split(";");
		for(int i=0;i<tripid.length;i++){
			String trip=tripid[i];

			String routeNumber = DispatchDAO.getRouteNoUsingTripId(adminBO.getAssociateCode(), trip);
			SharedRideDAO.splitTripFromGroup(adminBO.getAssociateCode(), trip, routeNumber, "JC");
			status = AdministrationDAO.updateOpenRequest(trip, adminBO.getAssociateCode(), "1", TDSConstants.companyCouldntService + "", reason, adminBO.getUid());
			ApplicationPoolBO poolBO = (ApplicationPoolBO) getServletContext().getAttribute("poolBO");
			OpenRequestBO openBo = RequestDAO.getOpenRequestByTripID(trip, adminBO.getAssociateCode(), adminBO.getTimeZone());

			if (openBo.getDriverid() != null && !openBo.getDriverid().equals("")) {
				DriverCabQueueBean cabQueueBean = DispatchDAO.getDriverByDriverID(adminBO.getAssociateCode(), openBo.getDriverid(), 1);
				String[] message = MessageGenerateJSON.generateMessage(openBo, "CC", System.currentTimeMillis(), adminBO.getAssociateCode());
				Messaging oneSMS = new Messaging(getServletContext(), cabQueueBean, message, poolBO, adminBO.getAssociateCode());
				oneSMS.start();
			}
			al_list = AdministrationDAO.getOpnReqDash(adminBO.getAssociateCode(), 0, adminBO.getTimeZone(), 0, "", 0, 0);
			String jobsCode = DispatchDAO.getCompCode(trip, adminBO.getMasterAssociateCode());
			if(jobsCode!=null && !jobsCode.equals("") && jobsCode.equals(adminBO.getMasterAssociateCode())){
				DispatchDAO.moveToHistory(trip, adminBO.getAssociateCode(), "",adminBO.getCompanyList());
			} else {
				ArrayList<String> myString = new ArrayList<String>();
				DispatchDAO.moveToHistory(trip, adminBO.getAssociateCode(), "",myString);
			}
//			DispatchDAO.moveToHistory(trip, adminBO.getAssociateCode(), "");
			AuditDAO.insertJobLogs("Couldn't Service", trip, adminBO.getAssociateCode(), TDSConstants.companyCouldntService, adminBO.getUid(), "", "", adminBO.getMasterAssociateCode());
		}
		response.getWriter().write(Integer.toString(status));

	}




	public void jobDetails(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		long comingInAt = System.currentTimeMillis();
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		JSONArray array = new JSONArray();
		ArrayList<OpenRequestBO> Address = null;
		int status = 0;
		ArrayList al_list = null;
		if (request.getParameter("status") != null) {
			status = Integer.parseInt(request.getParameter("status"));
		}
		int statusRtJobs = 0;
		if (request.getParameter("rtJobStatus") != null) {
			statusRtJobs = Integer.parseInt(request.getParameter("rtJobStatus"));
		}
		int route = 0;
		if (request.getParameter("routeJob") != null) {
			route = Integer.parseInt(request.getParameter("routeJob"));
		}
		if (request.getParameter("zoneList") != null && request.getParameter("zoneList") != "") {
			Address = AdministrationDAO.getOpnReqDash(adminBO.getAssociateCode(), 0, adminBO.getTimeZone(), status, request.getParameter("zoneList"), statusRtJobs, route);
		} else {
			Address = AdministrationDAO.getOpnReqDash(adminBO.getAssociateCode(), 0, adminBO.getTimeZone(), status, "", statusRtJobs, route);
		}
		if (request.getParameter("jsp") != null && request.getParameter("jsp").equalsIgnoreCase("yes")) {
			request.setAttribute("jobList", Address);
			getServletContext().getRequestDispatcher("/Company/JobDetailsExpand.jsp").forward(request, response);
		} else {
			for (int i = 0; i < Address.size(); i++) {
				JSONObject address = new JSONObject();
				try {
					address.put("D", Address.get(i).getDriverid() == null ? "" : Address.get(i).getDriverid());
					address.put("V", Address.get(i).getVehicleNo() == null ? "" : Address.get(i).getVehicleNo());
					address.put("N", Address.get(i).getName() == null ? "" : Address.get(i).getName());
					address.put("T", Address.get(i).getTripid() == null ? "" : Address.get(i).getTripid());
					address.put("Q", Address.get(i).getQueueno() == null ? "" : Address.get(i).getQueueno());
					address.put("A", Address.get(i).getScity() == null ? "" : Address.get(i).getScity());
					address.put("PT", Address.get(i).getPendingTime() == null ? "" : Address.get(i).getPendingTime());
					address.put("P", Address.get(i).getPhone() == null ? "" : Address.get(i).getPhone());
					address.put("S", Address.get(i).getTripStatus() == null ? "" : Address.get(i).getTripStatus());
					address.put("ST", Address.get(i).getRequestTime() == null ? "" : Address.get(i).getRequestTime());
					address.put("SD", Address.get(i).getSdate() == null ? "" : Address.get(i).getSdate());
					address.put("LA", Address.get(i).getSlat() == null ? "" : Address.get(i).getSlat());
					address.put("LO", Address.get(i).getSlong() == null ? "" : Address.get(i).getSlong());
					address.put("DD", Address.get(i).getDontDispatch());
					address.put("SR", Address.get(i).getTypeOfRide() == null ? "" : Address.get(i).getTypeOfRide());
					address.put("AC", Address.get(i).getPaytype() == null ? "" : Address.get(i).getPaytype());
					address.put("PC", Address.get(i).getPremiumCustomer() == 0 ? "" : Address.get(i).getPremiumCustomer());
					address.put("RT", Address.get(i).getRouteNumber() == null ? "" : Address.get(i).getRouteNumber());

					address.put("EDQ", Address.get(i).getEndQueueno() == null ? "" : Address.get(i).getEndQueueno());
					address.put("LMN", Address.get(i).getSlandmark() == null ? "" : Address.get(i).getSlandmark());

					String driverFlagValue = SystemUtils.getCompanyFlagDescription(getServletConfig(), adminBO.getMasterAssociateCode(), Address.get(i).getDrProfile(), ";", 0);

					address.put("DP", driverFlagValue);
					array.put(address);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			String responseString = array.toString();
			response.getWriter().write(responseString.toString());
			// System.out.println("job details sent in " +
			// (System.currentTimeMillis() - comingInAt));
		}
	}

	public void consoleView(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		ArrayList<ConsoleBO> Address = ConsoleDAO.consoleDbAccess(adminBO.getMasterAssociateCode());
		if (request.getParameter("jsp") != null && request.getParameter("jsp").equalsIgnoreCase("yes")) {
			request.setAttribute("jobList", Address);
			getServletContext().getRequestDispatcher("/Consoleview.jsp").forward(request, response);
		}
	}

	public void messageConsole(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		ArrayList<ConsoleBO> Address = ConsoleDAO.consoleDbAccess(adminBO.getMasterAssociateCode());
		if (request.getParameter("jsp") != null && request.getParameter("jsp").equalsIgnoreCase("yes")) {
			request.setAttribute("jobList", Address);
			getServletContext().getRequestDispatcher("/TextMessage.jsp").forward(request, response);
		}
	}

	public void deactivate(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		if (adminBO.getRoles().contains("2002")) {
			DriverDeactivation deactivation = new DriverDeactivation();
			deactivation.setDriver_id(request.getParameter("driver"));
			deactivation.setFrom_date(request.getParameter("fdate"));
			deactivation.setTo_date(request.getParameter("tdate"));
			deactivation.setFrom_time(request.getParameter("ftime"));
			deactivation.setTo_time(request.getParameter("ttime"));
			deactivation.setDesc(request.getParameter("answer"));
			deactivation.setHour(request.getParameter("hour"));
			deactivation.setAssoccode(adminBO.getAssociateCode());
			AdministrationDAO.insertDriverDeactivate(deactivation, adminBO.getTimeZone());
		} else {
			StringBuffer buffer = new StringBuffer();
			buffer.append("You doesn't have this access");
			response.getWriter().write(buffer.toString());

		}

	}

	public void timeUpdate(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		int status = AdministrationDAO.updateOpenRequestTime(request.getParameter("pickup"), request.getParameter("trip_id"), adminBO.getAssociateCode(), 0);
	}

	public void changeZoneForDash(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		String tripId = request.getParameter("tripid");
		String zoneId = request.getParameter("zoneId");
		int status = AdministrationDAO.updateOpenRequestzone( zoneId,tripId, adminBO.getAssociateCode());
		response.getWriter().write(Integer.toString(status));
	}

	public void redispatchMjob(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		ArrayList<OpenRequestBO> al_list = new ArrayList<OpenRequestBO>();
		String tripId = request.getParameter("tripid");
		String tripid[]=tripId.split(";");
		int	status=0;
		for(int i=0;i<tripid.length;i++){
			String trip=tripid[i];
			OpenRequestBO openBo = RequestDAO.getOpenRequestByTripID(trip, adminBO.getAssociateCode(), adminBO.getTimeZone());

			String routeNumber = DispatchDAO.getRouteNoUsingTripId(adminBO.getAssociateCode(), trip);
			if (routeNumber != null && !routeNumber.equals("")) {
				status = AdministrationDAO.updateOpenRequest(routeNumber, adminBO.getAssociateCode(), "4", TDSConstants.newRequestDispatchProcessesNotStarted + "", "REDISPATCH", adminBO.getUid());
			} else {
				status = AdministrationDAO.updateOpenRequest(trip, adminBO.getAssociateCode(), "2", TDSConstants.newRequestDispatchProcessesNotStarted + "", "REDISPATCH", adminBO.getUid());
			}
			ApplicationPoolBO poolBO = (ApplicationPoolBO) getServletContext().getAttribute("poolBO");
			if (openBo.getDriverid() != null && !openBo.getDriverid().equals("")) {
				DriverCabQueueBean cabQueueBean = DispatchDAO.getDriverByDriverID(adminBO.getAssociateCode(), openBo.getDriverid(), 1);
				String[] message = MessageGenerateJSON.generateMessage(openBo, "OC", System.currentTimeMillis(), adminBO.getAssociateCode());
				Messaging oneSMS = new Messaging(getServletContext(), cabQueueBean, message, poolBO, adminBO.getAssociateCode());
				oneSMS.start();
			}
			al_list = AdministrationDAO.getOpnReqDash(adminBO.getAssociateCode(), 0, adminBO.getTimeZone(), 0, "", 0, 0);
			AuditDAO.insertJobLogs("ReDispatch", trip, adminBO.getAssociateCode(), 8, adminBO.getUid(), "", "", adminBO.getMasterAssociateCode());

		}
		response.getWriter().write(Integer.toString(status));
	}

	public void jobCommands(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String command = request.getParameter("command");
		String reason = request.getParameter("answer");
		JSONArray array = new JSONArray();
		ArrayList<OpenRequestBO> al_list = new ArrayList<OpenRequestBO>();
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		String routeNumber = DispatchDAO.getRouteNoUsingTripId(adminBO.getAssociateCode(), request.getParameter("tripId"));
		ApplicationPoolBO poolBO = (ApplicationPoolBO) getServletContext().getAttribute("poolBO");
		int status = 0;
		if (command.equalsIgnoreCase("r")) {
			status = AdministrationDAO.updateOpenRequestTime(request.getParameter("pickup"), request.getParameter("tripId"), adminBO.getAssociateCode(), 1);
			al_list = AdministrationDAO.getOpnReqDash(adminBO.getAssociateCode(), 0, adminBO.getTimeZone(), status, "", 0, 0);
		} else if (command.equalsIgnoreCase("couldn'Service") || command.equalsIgnoreCase("cs")) {
			status = AdministrationDAO.updateOpenRequest(request.getParameter("tripId"), adminBO.getAssociateCode(), "1", TDSConstants.companyCouldntService + "", reason, adminBO.getUid());
			al_list = AdministrationDAO.getOpnReqDash(adminBO.getAssociateCode(), 0, adminBO.getTimeZone(), 0, "", 0, 0);
			String jobsCode = DispatchDAO.getCompCode(request.getParameter("tripId"), adminBO.getMasterAssociateCode());
			if(jobsCode!=null && !jobsCode.equals("") && jobsCode.equals(adminBO.getMasterAssociateCode())){
				DispatchDAO.moveToHistory(request.getParameter("tripId"), adminBO.getAssociateCode(), "",adminBO.getCompanyList());
			} else {
				ArrayList<String> myString = new ArrayList<String>();
				DispatchDAO.moveToHistory(request.getParameter("tripId"), adminBO.getAssociateCode(), "",myString);
			}
//			DispatchDAO.moveToHistory(request.getParameter("tripId"), adminBO.getAssociateCode(), "");
			AuditDAO.insertJobLogs("Couldn't Service", request.getParameter("tripId"), adminBO.getAssociateCode(), TDSConstants.companyCouldntService, adminBO.getUid(), "", "", adminBO.getMasterAssociateCode());
		} else if (command.equalsIgnoreCase("cancel") || command.equalsIgnoreCase("cc")) {
			SharedRideDAO.splitTripFromGroup(adminBO.getAssociateCode(), request.getParameter("tripId"), routeNumber, "JC");
			status = AdministrationDAO.updateOpenRequest(request.getParameter("tripId"), adminBO.getAssociateCode(), "1", TDSConstants.customerCancelledRequest + "", reason, adminBO.getUid());
			String tripId = request.getParameter("tripId");
			OpenRequestBO openBo = RequestDAO.getOpenRequestByTripID(tripId, adminBO.getAssociateCode(), adminBO.getTimeZone());
			if (openBo.getDriverid() != null && !openBo.getDriverid().equals("")) {
				DriverCabQueueBean cabQueueBean = DispatchDAO.getDriverByDriverID(adminBO.getAssociateCode(), openBo.getDriverid(), 1);
				String[] message = MessageGenerateJSON.generateMessage(openBo, "CC", System.currentTimeMillis(), adminBO.getAssociateCode());
				Messaging oneSMS = new Messaging(getServletContext(), cabQueueBean, message, poolBO, adminBO.getAssociateCode());
				oneSMS.start();
			}
			al_list = AdministrationDAO.getOpnReqDash(adminBO.getAssociateCode(), 0, adminBO.getTimeZone(), 0, "", 0, 0);
			String jobsCode = DispatchDAO.getCompCode(request.getParameter("tripId"), adminBO.getMasterAssociateCode());
			if(jobsCode!=null && !jobsCode.equals("") && jobsCode.equals(adminBO.getMasterAssociateCode())){
				DispatchDAO.moveToHistory(request.getParameter("tripId"),  adminBO.getAssociateCode(), "",adminBO.getCompanyList());
			} else {
				ArrayList<String> myString = new ArrayList<String>();
				DispatchDAO.moveToHistory(request.getParameter("tripId"),  adminBO.getAssociateCode(), "",myString);
			}
//			DispatchDAO.moveToHistory(request.getParameter("tripId"), adminBO.getAssociateCode(), "");
			AuditDAO.insertJobLogs("Customer Cancel", request.getParameter("tripId"), adminBO.getAssociateCode(), 50, adminBO.getUid(), "", "", adminBO.getMasterAssociateCode());
		} else if (command.equalsIgnoreCase("driverCompleted") || command.equalsIgnoreCase("dc")) {
			SharedRideDAO.splitTripFromGroup(adminBO.getAssociateCode(), request.getParameter("tripId"), routeNumber, "JC");
			status = AdministrationDAO.updateOpenRequest(request.getParameter("tripId"), adminBO.getAssociateCode(), "1", TDSConstants.tripCompleted + "", reason, adminBO.getUid());
			AuditDAO.insertJobLogs("Driver Completed", request.getParameter("tripId"), adminBO.getAssociateCode(), 61, adminBO.getUid(), "", "", adminBO.getMasterAssociateCode());
			String tripId = request.getParameter("tripId");
			OpenRequestBO openBo = RequestDAO.getOpenRequestByTripID(tripId, adminBO.getAssociateCode(), adminBO.getTimeZone());
			String driverid = openBo.getDriverid();
			if (driverid != null && !driverid.equals("")) {
				DriverCabQueueBean cabQueueBean = DispatchDAO.getDriverByDriverID(adminBO.getAssociateCode(), driverid, 1);
				String[] message = MessageGenerateJSON.generateMessage(openBo, "DC", System.currentTimeMillis(), adminBO.getAssociateCode());
				if (cabQueueBean.getDriverid() != null && !cabQueueBean.getDriverid().equals("")) {
					Messaging oneSMS = new Messaging(getServletContext(), cabQueueBean, message, poolBO, adminBO.getAssociateCode());
					oneSMS.start();
				}
			}
			al_list = AdministrationDAO.getOpnReqDash(adminBO.getAssociateCode(), 0, adminBO.getTimeZone(), 0, "", 0, 0);
			String jobsCode = DispatchDAO.getCompCode(request.getParameter("tripId"), adminBO.getMasterAssociateCode());
			if(jobsCode!=null && !jobsCode.equals("") && jobsCode.equals(adminBO.getMasterAssociateCode())){
				DispatchDAO.moveToHistory(request.getParameter("tripId"),  adminBO.getAssociateCode(), "",adminBO.getCompanyList());
			} else {
				ArrayList<String> myString = new ArrayList<String>();
				DispatchDAO.moveToHistory(request.getParameter("tripId"),  adminBO.getAssociateCode(), "",myString);
			}
//			DispatchDAO.moveToHistory(request.getParameter("tripId"), adminBO.getAssociateCode(), "");
		} else if (command.equalsIgnoreCase("BroadCast") || command.equalsIgnoreCase("bc")) {
			status = AdministrationDAO.updateOpenRequest(request.getParameter("tripId"), adminBO.getAssociateCode(), "3", TDSConstants.broadcastRequest + "", reason, adminBO.getUid());
			AuditDAO.insertJobLogs("Broadcast Request", request.getParameter("tripId"), adminBO.getAssociateCode(), 30, adminBO.getUid(), "", "", adminBO.getMasterAssociateCode());

			OpenRequestBO openRequestBO = new OpenRequestBO();
			openRequestBO.setTripid(request.getParameter("tripId"));
			String jobsCode = DispatchDAO.getCompCode(openRequestBO.getTripid(), adminBO.getMasterAssociateCode());
			if(jobsCode!=null && !jobsCode.equals("") && jobsCode.equals(adminBO.getMasterAssociateCode())){
				openRequestBO = RequestDAO.getOpenRequestBean(openRequestBO, "", "0", adminBO,adminBO.getCompanyList());
			} else {
				ArrayList<String> myString = new ArrayList<String>();
				openRequestBO = RequestDAO.getOpenRequestBean(openRequestBO, "", "0", adminBO, myString);
			}
//			openRequestBO = RequestDAO.getOpenRequestBean(openRequestBO, "", "0", adminBO);
			//If Job Is Broadcasted Send To All Drivers Within The Distance 
			ArrayList<String> fleets = new ArrayList<String>();
			if(openRequestBO.getAssociateCode().equals(adminBO.getMasterAssociateCode())){
				fleets = SecurityDAO.getAllAssoccode(adminBO.getMasterAssociateCode());
			} else {
				fleets.add(openRequestBO.getAssociateCode());
			}
			if (openRequestBO != null && adminBO.getDispatchBroadcast()==1) {
				openRequestBO.setDispatchDistance(ZoneDAO.getMaxDistance(adminBO.getMasterAssociateCode(), openRequestBO.getQueueno().equals("")?"000":openRequestBO.getQueueno()));
				ArrayList<DriverCabQueueBean> allLoggedDrivers= DispatchDAO.allDriversWithInADistanceWithSpLFLAGS(fleets, Double.parseDouble(openRequestBO.getSlat()), Double.parseDouble(openRequestBO.getSlong()),openRequestBO.getDispatchDistance(), openRequestBO.getDrProfile(), null, openRequestBO.getJobRating(),ZoneDAO.getMaxDistance(adminBO.getMasterAssociateCode(),openRequestBO.getQueueno()));
				String[] finalMessage = MessageGenerateJSON.generateMessage(openRequestBO, "BCJ", System.currentTimeMillis(), adminBO.getAssociateCode());
				Messaging messageDriver = new Messaging(getServletContext(), allLoggedDrivers, finalMessage, poolBO, adminBO.getAssociateCode());
				messageDriver.start();
			}
		} else if (command.equalsIgnoreCase("onBoard") || command.equalsIgnoreCase("ob")) {
			if(reason==null || reason.equals("")){
				reason = "Operator force OnBoard";
			}
			status = AdministrationDAO.updateOpenRequest(request.getParameter("tripId"), adminBO.getAssociateCode(), "1", TDSConstants.tripStarted + "", reason, adminBO.getUid());
			al_list = AdministrationDAO.getOpnReqDash(adminBO.getAssociateCode(), 0, adminBO.getTimeZone(), 0, "", 0, 0);
			AuditDAO.insertJobLogs("Operator force OnBoard", request.getParameter("tripId"), adminBO.getAssociateCode(), TDSConstants.tripStarted, adminBO.getUid(), "", "", adminBO.getMasterAssociateCode());
			
			OpenRequestBO openBo = RequestDAO.getOpenRequestByTripID(request.getParameter("tripId"), adminBO.getAssociateCode(), adminBO.getTimeZone());
			if (openBo.getDriverid() != null && !openBo.getDriverid().equals("")) {
				DriverCabQueueBean cabQueueBean = DispatchDAO.getDriverByDriverID(adminBO.getAssociateCode(), openBo.getDriverid(), 1);
				String[] message = MessageGenerateJSON.generateMessage(openBo, "MO", System.currentTimeMillis(), adminBO.getAssociateCode());
				Messaging oneSMS = new Messaging(getServletContext(), cabQueueBean, message, poolBO, adminBO.getAssociateCode());
				oneSMS.start();
			}

		} else if (command.equalsIgnoreCase("onSite") || command.equalsIgnoreCase("os")) {
			if(reason==null || reason.equals("")){
				reason = "Operator force OnSite";
			}
			status = AdministrationDAO.updateOpenRequest(request.getParameter("tripId"), adminBO.getAssociateCode(), "1", TDSConstants.onSite + "", reason, adminBO.getUid());
			al_list = AdministrationDAO.getOpnReqDash(adminBO.getAssociateCode(), 0, adminBO.getTimeZone(), 0, "", 0, 0);
			AuditDAO.insertJobLogs("Operator force OnSite", request.getParameter("tripId"), adminBO.getAssociateCode(), TDSConstants.onSite, adminBO.getUid(), "", "", adminBO.getMasterAssociateCode());
			
			OpenRequestBO openBo = RequestDAO.getOpenRequestByTripID(request.getParameter("tripId"), adminBO.getAssociateCode(), adminBO.getTimeZone());
			if (openBo.getDriverid() != null && !openBo.getDriverid().equals("")) {
				DriverCabQueueBean cabQueueBean = DispatchDAO.getDriverByDriverID(adminBO.getAssociateCode(), openBo.getDriverid(), 1);
				String[] message = MessageGenerateJSON.generateMessage(openBo, "MO", System.currentTimeMillis(), adminBO.getAssociateCode());
				Messaging oneSMS = new Messaging(getServletContext(), cabQueueBean, message, poolBO, adminBO.getAssociateCode());
				oneSMS.start();
			}

		} else if (command.equalsIgnoreCase("onRoute") || command.equalsIgnoreCase("or")) {
			if(reason==null || reason.equals("")){
				reason = "Operator force Route";
			}
			status = AdministrationDAO.updateOpenRequest(request.getParameter("tripId"), adminBO.getAssociateCode(), "1", TDSConstants.onRotueToPickup + "", reason, adminBO.getUid());
			al_list = AdministrationDAO.getOpnReqDash(adminBO.getAssociateCode(), 0, adminBO.getTimeZone(), 0, "", 0, 0);
			AuditDAO.insertJobLogs("Operator force On Route", request.getParameter("tripId"), adminBO.getAssociateCode(), TDSConstants.onRotueToPickup, adminBO.getUid(), "", "", adminBO.getMasterAssociateCode());
			
			OpenRequestBO openBo = RequestDAO.getOpenRequestByTripID(request.getParameter("tripId"), adminBO.getAssociateCode(), adminBO.getTimeZone());
			if (openBo.getDriverid() != null && !openBo.getDriverid().equals("")) {
				DriverCabQueueBean cabQueueBean = DispatchDAO.getDriverByDriverID(adminBO.getAssociateCode(), openBo.getDriverid(), 1);
				String[] message = MessageGenerateJSON.generateMessage(openBo, "MO", System.currentTimeMillis(), adminBO.getAssociateCode());
				Messaging oneSMS = new Messaging(getServletContext(), cabQueueBean, message, poolBO, adminBO.getAssociateCode());
				oneSMS.start();
			}

		} else if (command.equalsIgnoreCase("endTrip") || command.equalsIgnoreCase("et")) {
			if(reason==null || reason.equals("")){
				reason = "Operator force Endtrip";
			}
			SharedRideDAO.splitTripFromGroup(adminBO.getAssociateCode(), request.getParameter("tripId"), routeNumber, "JC");

			status = AdministrationDAO.updateOpenRequest(request.getParameter("tripId"), adminBO.getAssociateCode(), "1", TDSConstants.tripCompleted + "", reason, adminBO.getUid());
			al_list = AdministrationDAO.getOpnReqDash(adminBO.getAssociateCode(), 0, adminBO.getTimeZone(), 0, "", 0, 0);
			
			OpenRequestBO openBo = RequestDAO.getOpenRequestByTripID(request.getParameter("tripId"), adminBO.getAssociateCode(), adminBO.getTimeZone());
			if (openBo.getDriverid() != null && !openBo.getDriverid().equals("")) {
				DriverCabQueueBean cabQueueBean = DispatchDAO.getDriverByDriverID(adminBO.getAssociateCode(), openBo.getDriverid(), 1);
				String[] message = MessageGenerateJSON.generateMessage(openBo, "MO", System.currentTimeMillis(), adminBO.getAssociateCode());
				Messaging oneSMS = new Messaging(getServletContext(), cabQueueBean, message, poolBO, adminBO.getAssociateCode());
				oneSMS.start();
			}
			
			String jobsCode = DispatchDAO.getCompCode(request.getParameter("tripId"), adminBO.getMasterAssociateCode());
			if(jobsCode!=null && !jobsCode.equals("") && jobsCode.equals(adminBO.getMasterAssociateCode())){
				DispatchDAO.moveToHistory(request.getParameter("tripId"), adminBO.getAssociateCode(), "",adminBO.getCompanyList());
			} else {
				ArrayList<String> myString = new ArrayList<String>();
				DispatchDAO.moveToHistory(request.getParameter("tripId"), adminBO.getAssociateCode(), "",myString);
			}
			//			DispatchDAO.moveToHistory(request.getParameter("tripId"), adminBO.getAssociateCode(), "");
			AuditDAO.insertJobLogs("Operator force End Trip", request.getParameter("tripId"), adminBO.getAssociateCode(), TDSConstants.tripCompleted, adminBO.getUid(), "", "", adminBO.getMasterAssociateCode());
			
		} else if (command.equalsIgnoreCase("ReDispatch") || command.equalsIgnoreCase("rd") || command.equalsIgnoreCase("")) {
			String routeNo = request.getParameter("routeNo");
			String tripId = request.getParameter("tripId");
			OpenRequestBO openBo = RequestDAO.getOpenRequestByTripID(tripId, adminBO.getAssociateCode(), adminBO.getTimeZone());

			if (routeNo != null && !routeNo.equals("")) {
				status = AdministrationDAO.updateOpenRequest(routeNo, adminBO.getAssociateCode(), "4", TDSConstants.newRequestDispatchProcessesNotStarted + "", reason, adminBO.getUid());
			} else {
				status = AdministrationDAO.updateOpenRequest(tripId, adminBO.getAssociateCode(), "2", TDSConstants.newRequestDispatchProcessesNotStarted + "", reason, adminBO.getUid());
			}
			if (openBo.getDriverid() != null && !openBo.getDriverid().equals("")) {
				DriverCabQueueBean cabQueueBean = DispatchDAO.getDriverByDriverID(adminBO.getAssociateCode(), openBo.getDriverid(), 1);
				String[] message = MessageGenerateJSON.generateMessage(openBo, "OC", System.currentTimeMillis(), adminBO.getAssociateCode());
				Messaging oneSMS = new Messaging(getServletContext(), cabQueueBean, message, poolBO, adminBO.getAssociateCode());
				oneSMS.start();
			}
			al_list = AdministrationDAO.getOpnReqDash(adminBO.getAssociateCode(), 0, adminBO.getTimeZone(), 0, "", 0, 0);
			AuditDAO.insertJobLogs("ReDispatch", request.getParameter("tripId"), adminBO.getAssociateCode(), 8, adminBO.getUid(), "", "", adminBO.getMasterAssociateCode());
		} else if (command.equalsIgnoreCase("noShow") || command.equalsIgnoreCase("ns")) {
			String tripId = request.getParameter("tripId");
			status = AdministrationDAO.updateOpenRequest(tripId, adminBO.getAssociateCode(), "1", TDSConstants.noShowRequest + "", reason, adminBO.getUid());
			OpenRequestBO openBo = RequestDAO.getOpenRequestByTripID(tripId, adminBO.getAssociateCode(), adminBO.getTimeZone());
			String driverid = openBo.getDriverid();
			if (openBo.getDriverid() != null && !openBo.getDriverid().equals("")) {
				DriverCabQueueBean cabQueueBean = DispatchDAO.getDriverByDriverID(adminBO.getAssociateCode(), driverid, 1);
				String[] message = MessageGenerateJSON.generateMessage(openBo, "NS", System.currentTimeMillis(), adminBO.getMasterAssociateCode());
				Messaging oneSMS = new Messaging(getServletContext(), cabQueueBean, message, poolBO, adminBO.getAssociateCode());
				oneSMS.start();
			}
			al_list = AdministrationDAO.getOpnReqDash(adminBO.getAssociateCode(), 0, adminBO.getTimeZone(), 0, "", 0, 0);
			String jobsCode = DispatchDAO.getCompCode(request.getParameter("tripId"), adminBO.getMasterAssociateCode());
			if(jobsCode!=null && !jobsCode.equals("") && jobsCode.equals(adminBO.getMasterAssociateCode())){
				DispatchDAO.moveToHistory(request.getParameter("tripId"), adminBO.getAssociateCode(), "",adminBO.getCompanyList());
			} else {
				ArrayList<String> myString = new ArrayList<String>();
				DispatchDAO.moveToHistory(request.getParameter("tripId"), adminBO.getAssociateCode(), "",myString);
			}
//			DispatchDAO.moveToHistory(request.getParameter("tripId"), adminBO.getAssociateCode(), "");
			AuditDAO.insertJobLogs("No Show", request.getParameter("tripId"), adminBO.getAssociateCode(), 51, adminBO.getUid(), "", "", adminBO.getMasterAssociateCode());
			/*try {
				Thread.sleep(15000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}*/
			
			String returnValue = SystemPropertiesDAO.getParameter(adminBO.getMasterAssociateCode(), "ns_Login");
			System.out.println("ns - rebook:"+returnValue);
			int reLoginDriver = (returnValue!=null && !returnValue.equals(""))?Integer.parseInt(returnValue):1;
			if(reLoginDriver==1){
				ArrayList<String> zoneKey=ServiceRequestDAO.getLastZone(adminBO.getMasterAssociateCode(),openBo.getDriverid());
				String zoneName = SystemPropertiesDAO.getZoneName(openBo.getQueueno(), adminBO);
				JSONArray resultArray = new JSONArray();
				JSONArray subArray = new JSONArray();
				if((zoneKey!=null && zoneKey.size() > 0) || (zoneName!=null && !zoneName.equals(""))){
					String[] messageZone = new String[2];
					JSONObject message = new JSONObject();
					JSONObject header = new JSONObject();
					try {
						header.put("ZNS", "TRUE");
						header.put("Command","SPLZONE");
						header.put("rV", 1.2);
				    	header.put("rC", 200);
						header.put("msgID", System.currentTimeMillis());
						if(zoneKey.size()>1){
							message.put("OK", zoneKey.get(0));
							message.put("OZ", zoneKey.get(1));
						}
						if(!zoneName.equals("")){
							message.put("JZ", zoneName);
							message.put("JK", openBo.getQueueno());
						}
						message.put("msgID",System.currentTimeMillis());
						subArray.put(header);
						resultArray.put(0, header);
						resultArray.put(1,message);
						messageZone[0] = resultArray.toString();
						messageZone[1] = subArray.toString();
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					DriverCabQueueBean cabQueueBean = DispatchDAO.getDriverByDriverID(adminBO.getAssociateCode(), driverid, 1);
					Messaging oneSMS = new Messaging(getServletContext(), cabQueueBean, messageZone, poolBO, adminBO.getAssociateCode());
					oneSMS.start();
				}
			}
		} else if (command.equalsIgnoreCase("reStart") || command.equalsIgnoreCase("rs")) {
			String tripId = request.getParameter("tripId");
			DriverCabQueueBean cabQueueBean = null;
			OpenRequestBO openBo = null;
			String[] message = new String[2];
			String driverId = request.getParameter("driverId");
			if (!tripId.equals("")) {
				openBo = DispatchDAO.getOpenRequestStatus(tripId, adminBO, driverId);
			}
			if (openBo != null && openBo.getDriverid() != null && !openBo.getDriverid().equals("")) {
				if (openBo.getTripStatus().equals(TDSConstants.performingDispatchProcesses + "")) {
					if (!openBo.getQueueno().equals("")) {
						List<QueueCoordinatesBO> queuList = SystemPropertiesDAO.getQueueCoOrdinateList(openBo.getQueueno(), adminBO.getMasterAssociateCode(), "");
						openBo.setAcceptedTime(queuList.get(0).getSmsResponseTime() == null ? "" : queuList.get(0).getSmsResponseTime());
					}
					message = MessageGenerateJSON.generateMessage(openBo, "A", System.currentTimeMillis(), adminBO.getAssociateCode());
				} else if (openBo.getTripStatus().equals(TDSConstants.jobAllocated + "") || openBo.getTripStatus().equals(TDSConstants.onRotueToPickup + "") || openBo.getTripStatus().equals(TDSConstants.onSite + "") || openBo.getTripStatus().equals(TDSConstants.tripStarted + "") || openBo.getTripStatus().equals(TDSConstants.driverNoShow + "")) {
					message = MessageGenerateJSON.generateMessage(openBo, "AA", System.currentTimeMillis(), adminBO.getAssociateCode());
				} else if (openBo.getTripStatus().equals(TDSConstants.tripOnHold + "")||openBo.getTripStatus().equals(TDSConstants.srAllocatedToCab + "")) {
					status = AdministrationDAO.updateOpenRequest(tripId, adminBO.getAssociateCode(), "5", TDSConstants.performingDispatchProcesses + "", reason, adminBO.getUid());
					openBo.setTripStatus(TDSConstants.performingDispatchProcesses + "");
					message = MessageGenerateJSON.generateMessage(openBo, "A", System.currentTimeMillis(), adminBO.getAssociateCode());
				}
				String driverid = openBo.getDriverid();
				if (openBo.getDriverid() != null && !openBo.getDriverid().equals("")) {
					cabQueueBean = DispatchDAO.getDriverByDriverID(adminBO.getAssociateCode(), driverid, 1);
					Messaging oneSMS = new Messaging(getServletContext(), cabQueueBean, message, poolBO, adminBO.getAssociateCode());
					oneSMS.start();
				}
				al_list = AdministrationDAO.getOpnReqDash(adminBO.getAssociateCode(), 0, adminBO.getTimeZone(), 0, "", 0, 0);
				AuditDAO.insertJobLogs("Resend the job", request.getParameter("tripId"), adminBO.getAssociateCode(), 4, adminBO.getUid(), "", "", adminBO.getMasterAssociateCode());
			}
		} else if (command.equalsIgnoreCase("jobOnHold") || command.equalsIgnoreCase("h")) {
//			status = AdministrationDAO.updateOpenRequest(request.getParameter("tripId"), adminBO.getAssociateCode(), "1", "99", reason, adminBO.getUid());
			String routeNo = request.getParameter("routeNo");
			String tripId = request.getParameter("tripId");
			OpenRequestBO openBo = RequestDAO.getOpenRequestByTripID(tripId, adminBO.getAssociateCode(), adminBO.getTimeZone());

			if (routeNo != null && !routeNo.equals("")) {
				status = AdministrationDAO.updateOpenRequest(routeNo, adminBO.getAssociateCode(), "4", "99" , reason, adminBO.getUid());
			} else {
				status = AdministrationDAO.updateOpenRequest(tripId, adminBO.getAssociateCode(), "2", "99", reason, adminBO.getUid());
			}
			if (openBo.getDriverid() != null && !openBo.getDriverid().equals("")) {
				DriverCabQueueBean cabQueueBean = DispatchDAO.getDriverByDriverID(adminBO.getAssociateCode(), openBo.getDriverid(), 1);
				String[] message = MessageGenerateJSON.generateMessage(openBo, "OC", System.currentTimeMillis(), adminBO.getAssociateCode());
				Messaging oneSMS = new Messaging(getServletContext(), cabQueueBean, message, poolBO, adminBO.getAssociateCode());
				oneSMS.start();
			}
			al_list = AdministrationDAO.getOpnReqDash(adminBO.getAssociateCode(), 0, adminBO.getTimeZone(), 0, "", 0, 0);
			AuditDAO.insertJobLogs("Hold the job", request.getParameter("tripId"), adminBO.getAssociateCode(), 4, adminBO.getUid(), "", "", adminBO.getMasterAssociateCode());
		}
		for (int i = 0; i < al_list.size(); i++) {
			JSONObject address = new JSONObject();
			try {
				address.put("D", al_list.get(i).getDriverid() == null ? "" : al_list.get(i).getDriverid());
				address.put("V", al_list.get(i).getVehicleNo() == null ? "" : al_list.get(i).getVehicleNo());
				address.put("N", al_list.get(i).getName() == null ? "" : al_list.get(i).getName());
				address.put("T", al_list.get(i).getTripid() == null ? "" : al_list.get(i).getTripid());
				address.put("Q", al_list.get(i).getQueueno() == null ? "" : al_list.get(i).getQueueno());
				address.put("A", al_list.get(i).getScity() == null ? "" : al_list.get(i).getScity());
				address.put("PT", al_list.get(i).getPendingTime() == null ? "" : al_list.get(i).getPendingTime());
				address.put("P", al_list.get(i).getPhone() == null ? "" : al_list.get(i).getPhone());
				address.put("S", al_list.get(i).getTripStatus() == null ? "" : al_list.get(i).getTripStatus());
				address.put("ST", al_list.get(i).getRequestTime() == null ? "" : al_list.get(i).getRequestTime());
				address.put("SD", al_list.get(i).getSdate() == null ? "" : al_list.get(i).getSdate());
				address.put("LA", al_list.get(i).getSlat() == null ? "" : al_list.get(i).getSlat());
				address.put("LO", al_list.get(i).getSlong() == null ? "" : al_list.get(i).getSlong());
				address.put("DD", al_list.get(i).getDontDispatch());
				address.put("SR", al_list.get(i).getTypeOfRide() == null ? "" : al_list.get(i).getTypeOfRide());
				address.put("AC", al_list.get(i).getPaytype() == null ? "" : al_list.get(i).getPaytype());
				address.put("PC", al_list.get(i).getPremiumCustomer() == 0 ? "" : al_list.get(i).getPremiumCustomer());
				address.put("RT", al_list.get(i).getRouteNumber() == null ? "" : al_list.get(i).getRouteNumber());
				address.put("EDQ", al_list.get(i).getEndQueueno() == null ? "" : al_list.get(i).getEndQueueno());
				address.put("LMN", al_list.get(i).getSlandmark() == null ? "" : al_list.get(i).getSlandmark());

				String driverFlagValue = SystemUtils.getCompanyFlagDescription(getServletConfig(), adminBO.getMasterAssociateCode(), al_list.get(i).getDrProfile(), ";", 0);
				address.put("DP", driverFlagValue);
				array.put(address);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		String responseString = array.toString();
		response.getWriter().write(responseString.toString());

	}



	public void multiplejobCommands(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String command = request.getParameter("command");
		String reason = request.getParameter("answer");
		String tripId = request.getParameter("tripid");
		//JSONArray array = new JSONArray();
		ArrayList<OpenRequestBO> al_list = new ArrayList<OpenRequestBO>();
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		int status = 0;
		if (command.equalsIgnoreCase("couldn'Service") || command.equalsIgnoreCase("cs")) {
			status = AdministrationDAO.updateOpenRequest(tripId, adminBO.getAssociateCode(), "1", TDSConstants.companyCouldntService + "", reason, adminBO.getUid());
			al_list = AdministrationDAO.getOpnReqDash(adminBO.getAssociateCode(), 0, adminBO.getTimeZone(), 0, "", 0, 0);
			String jobsCode = DispatchDAO.getCompCode(tripId, adminBO.getMasterAssociateCode());
			if(jobsCode!=null && !jobsCode.equals("") && jobsCode.equals(adminBO.getMasterAssociateCode())){
				DispatchDAO.moveToHistory(tripId, adminBO.getAssociateCode(), "",adminBO.getCompanyList());
			} else {
				ArrayList<String> myString = new ArrayList<String>();
				DispatchDAO.moveToHistory(tripId, adminBO.getAssociateCode(), "",myString);
			}
//			DispatchDAO.moveToHistory(tripId, adminBO.getAssociateCode(), "");
			AuditDAO.insertJobLogs("Couldn't Service", tripId, adminBO.getAssociateCode(), TDSConstants.companyCouldntService, adminBO.getUid(), "", "", adminBO.getMasterAssociateCode());
		} else if (command.equalsIgnoreCase("cancel") || command.equalsIgnoreCase("cc")) {
			status = AdministrationDAO.updateOpenRequest(tripId, adminBO.getAssociateCode(), "1", TDSConstants.customerCancelledRequest + "", reason, adminBO.getUid());
			//String tripId = request.getParameter("tripId");
			ApplicationPoolBO poolBO = (ApplicationPoolBO) getServletContext().getAttribute("poolBO");
			String tripid[]=tripId.split(";");
			for(int i=0;i<tripid.length;i++){
				String trip=tripid[i];
				String routeNumber = DispatchDAO.getRouteNoUsingTripId(adminBO.getAssociateCode(), trip);
				SharedRideDAO.splitTripFromGroup(adminBO.getAssociateCode(), tripId, routeNumber, "JC");
				OpenRequestBO openBo = RequestDAO.getOpenRequestByTripID(trip, adminBO.getAssociateCode(), adminBO.getTimeZone());
				if (openBo.getDriverid() != null && !openBo.getDriverid().equals("")) {
					DriverCabQueueBean cabQueueBean = DispatchDAO.getDriverByDriverID(adminBO.getAssociateCode(), openBo.getDriverid(), 1);
					String[] message = MessageGenerateJSON.generateMessage(openBo, "CC", System.currentTimeMillis(), adminBO.getAssociateCode());
					Messaging oneSMS = new Messaging(getServletContext(), cabQueueBean, message, poolBO, adminBO.getAssociateCode());
					oneSMS.start();
				}
				al_list = AdministrationDAO.getOpnReqDash(adminBO.getAssociateCode(), 0, adminBO.getTimeZone(), 0, "", 0, 0);
				String jobsCode = DispatchDAO.getCompCode(trip, adminBO.getMasterAssociateCode());
				if(jobsCode!=null && !jobsCode.equals("") && jobsCode.equals(adminBO.getMasterAssociateCode())){
					DispatchDAO.moveToHistory(trip, adminBO.getAssociateCode(), "",adminBO.getCompanyList());
				} else {
					ArrayList<String> myString = new ArrayList<String>();
					DispatchDAO.moveToHistory(trip, adminBO.getAssociateCode(), "",myString);
				}
//				DispatchDAO.moveToHistory(trip, adminBO.getAssociateCode(), "");
				AuditDAO.insertJobLogs("Customer Cancel", trip, adminBO.getAssociateCode(), 50, adminBO.getUid(), "", "", adminBO.getMasterAssociateCode());
			}	
		} else if (command.equalsIgnoreCase("driverCompleted") || command.equalsIgnoreCase("dc")) {
			status = AdministrationDAO.updateOpenRequest(tripId, adminBO.getAssociateCode(), "1", TDSConstants.tripCompleted + "", reason, adminBO.getUid());
			ApplicationPoolBO poolBO = (ApplicationPoolBO) getServletContext().getAttribute("poolBO");
			String tripid[]=tripId.split(";");
			for(int i=0;i<tripid.length;i++){
				String trip=tripid[i];
				String routeNumber = DispatchDAO.getRouteNoUsingTripId(adminBO.getAssociateCode(), trip);
				SharedRideDAO.splitTripFromGroup(adminBO.getAssociateCode(), tripId, routeNumber, "JC");
				OpenRequestBO openBo = RequestDAO.getOpenRequestByTripID(trip, adminBO.getAssociateCode(), adminBO.getTimeZone());
				String driverid = openBo.getDriverid();
				AuditDAO.insertJobLogs("Driver Completed", trip, adminBO.getAssociateCode(), 61, adminBO.getUid(), "", "", adminBO.getMasterAssociateCode());
				if (driverid != null && !driverid.equals("")) {
					DriverCabQueueBean cabQueueBean = DispatchDAO.getDriverByDriverID(adminBO.getAssociateCode(), driverid, 1);
					String[] message = MessageGenerateJSON.generateMessage(openBo, "DC", System.currentTimeMillis(), adminBO.getAssociateCode());
					if (cabQueueBean.getDriverid() != null && !cabQueueBean.getDriverid().equals("")) {
						Messaging oneSMS = new Messaging(getServletContext(), cabQueueBean, message, poolBO, adminBO.getAssociateCode());
						oneSMS.start();
					}
				}
				al_list = AdministrationDAO.getOpnReqDash(adminBO.getAssociateCode(), 0, adminBO.getTimeZone(), 0, "", 0, 0);
				String jobsCode = DispatchDAO.getCompCode(trip, adminBO.getMasterAssociateCode());
				if(jobsCode!=null && !jobsCode.equals("") && jobsCode.equals(adminBO.getMasterAssociateCode())){
					DispatchDAO.moveToHistory(trip,  adminBO.getAssociateCode(), "",adminBO.getCompanyList());
				} else {
					ArrayList<String> myString = new ArrayList<String>();
					DispatchDAO.moveToHistory(trip,  adminBO.getAssociateCode(), "",myString);
				}
//				DispatchDAO.moveToHistory(trip, adminBO.getAssociateCode(), "");
			}
		} else if (command.equalsIgnoreCase("BroadCast") || command.equalsIgnoreCase("bc")) {
			status = AdministrationDAO.updateOpenRequest(tripId, adminBO.getAssociateCode(), "3", TDSConstants.broadcastRequest + "", reason, adminBO.getUid());
			ApplicationPoolBO poolBO = (ApplicationPoolBO) getServletContext().getAttribute("poolBO");
			AuditDAO.insertJobLogs("Broadcast Request", tripId, adminBO.getAssociateCode(), 30, adminBO.getUid(), "", "", adminBO.getMasterAssociateCode());
			OpenRequestBO openRequestBO = new OpenRequestBO();
			String tripid[]=tripId.split(";");
			ArrayList<String> fleets = SecurityDAO.getAllAssoccode(adminBO.getMasterAssociateCode());
			for(int i=0;i<tripid.length;i++){
				String trip=tripid[i];
				openRequestBO.setTripid(trip);
				String jobsCode = DispatchDAO.getCompCode(openRequestBO.getTripid(), adminBO.getMasterAssociateCode());
				if(jobsCode!=null && !jobsCode.equals("") && jobsCode.equals(adminBO.getMasterAssociateCode())){
					openRequestBO = RequestDAO.getOpenRequestBean(openRequestBO, "", "0", adminBO,adminBO.getCompanyList());
				} else {
					ArrayList<String> myString = new ArrayList<String>();
					openRequestBO = RequestDAO.getOpenRequestBean(openRequestBO, "", "0", adminBO, myString);
				}
//				openRequestBO = RequestDAO.getOpenRequestBean(openRequestBO, "", "0", adminBO);
				//If Job Is Broadcasted Send To All Drivers Within The Distance 
				ArrayList<String> compToLook = new ArrayList<String>();	
				if(openRequestBO.getAssociateCode().equals(adminBO.getMasterAssociateCode())){
					compToLook = fleets;
				} else {
					compToLook.add(openRequestBO.getAssociateCode());
				}
				if (openRequestBO != null && adminBO.getDispatchBroadcast()==1) {
					openRequestBO.setDispatchDistance(ZoneDAO.getMaxDistance(adminBO.getMasterAssociateCode(), openRequestBO.getQueueno().equals("")?"000":openRequestBO.getQueueno()));
					ArrayList<DriverCabQueueBean> allLoggedDrivers= DispatchDAO.allDriversWithInADistanceWithSpLFLAGS(compToLook, Double.parseDouble(openRequestBO.getSlat()), Double.parseDouble(openRequestBO.getSlong()),openRequestBO.getDispatchDistance(), openRequestBO.getDrProfile(), null, openRequestBO.getJobRating(),ZoneDAO.getMaxDistance(adminBO.getMasterAssociateCode(),openRequestBO.getQueueno()));
					String[] finalMessage = MessageGenerateJSON.generateMessage(openRequestBO, "BCJ", System.currentTimeMillis(), adminBO.getAssociateCode());
					Messaging messageDriver = new Messaging(getServletContext(), allLoggedDrivers, finalMessage, poolBO, adminBO.getAssociateCode());
					messageDriver.start();
				}
			}
		} else if (command.equalsIgnoreCase("ReDispatch") || command.equalsIgnoreCase("rd") || command.equalsIgnoreCase("")) {

			String tripid[]=tripId.split(";");
			for(int i=0;i<tripid.length;i++){
				String trip=tripid[i];
				String routeNo = DispatchDAO.getRouteNoUsingTripId(adminBO.getAssociateCode(), trip);
				OpenRequestBO openBo = RequestDAO.getOpenRequestByTripID(trip, adminBO.getAssociateCode(), adminBO.getTimeZone());

				if (routeNo != null && !routeNo.equals("")) {
					status = AdministrationDAO.updateOpenRequest(routeNo, adminBO.getAssociateCode(), "4", TDSConstants.newRequestDispatchProcessesNotStarted + "", reason, adminBO.getUid());
				} else {
					status = AdministrationDAO.updateOpenRequest(trip, adminBO.getAssociateCode(), "2", TDSConstants.newRequestDispatchProcessesNotStarted + "", reason, adminBO.getUid());
				}
				ApplicationPoolBO poolBO = (ApplicationPoolBO) getServletContext().getAttribute("poolBO");
				if (openBo.getDriverid() != null && !openBo.getDriverid().equals("")) {
					DriverCabQueueBean cabQueueBean = DispatchDAO.getDriverByDriverID(adminBO.getAssociateCode(), openBo.getDriverid(), 1);
					String[] message = MessageGenerateJSON.generateMessage(openBo, "OC", System.currentTimeMillis(), adminBO.getAssociateCode());
					Messaging oneSMS = new Messaging(getServletContext(), cabQueueBean, message, poolBO, adminBO.getAssociateCode());
					oneSMS.start();
				}
				al_list = AdministrationDAO.getOpnReqDash(adminBO.getAssociateCode(), 0, adminBO.getTimeZone(), 0, "", 0, 0);
				AuditDAO.insertJobLogs("ReDispatch", trip, adminBO.getAssociateCode(), 8, adminBO.getUid(), "", "", adminBO.getMasterAssociateCode());
			}
		} else if (command.equalsIgnoreCase("noShow") || command.equalsIgnoreCase("ns")) {

			ApplicationPoolBO poolBO = (ApplicationPoolBO) getServletContext().getAttribute("poolBO");
			String tripid[]=tripId.split(";");
			for(int i=0;i<tripid.length;i++){
				String trip=tripid[i];

				status = AdministrationDAO.updateOpenRequest(trip, adminBO.getAssociateCode(), "1", TDSConstants.noShowRequest + "", reason, adminBO.getUid());
				OpenRequestBO openBo = RequestDAO.getOpenRequestByTripID(trip, adminBO.getAssociateCode(), adminBO.getTimeZone());

				String driverid = openBo.getDriverid();
				if (openBo.getDriverid() != null && !openBo.getDriverid().equals("")) {
					DriverCabQueueBean cabQueueBean = DispatchDAO.getDriverByDriverID(adminBO.getAssociateCode(), driverid, 1);
					String[] message = MessageGenerateJSON.generateMessage(openBo, "NS", System.currentTimeMillis(), adminBO.getMasterAssociateCode());
					Messaging oneSMS = new Messaging(getServletContext(), cabQueueBean, message, poolBO, adminBO.getAssociateCode());
					oneSMS.start();
				}
				al_list = AdministrationDAO.getOpnReqDash(adminBO.getAssociateCode(), 0, adminBO.getTimeZone(), 0, "", 0, 0);
				String jobsCode = DispatchDAO.getCompCode(trip, adminBO.getMasterAssociateCode());
				if(jobsCode!=null && !jobsCode.equals("") && jobsCode.equals(adminBO.getMasterAssociateCode())){
					DispatchDAO.moveToHistory(trip, adminBO.getAssociateCode(), "",adminBO.getCompanyList());
				} else {
					ArrayList<String> myString = new ArrayList<String>();
					DispatchDAO.moveToHistory(trip, adminBO.getAssociateCode(), "",myString);
				}
//				DispatchDAO.moveToHistory(trip, adminBO.getAssociateCode(), "");
				AuditDAO.insertJobLogs("No Show", trip, adminBO.getAssociateCode(), 51, adminBO.getUid(), "", "", adminBO.getMasterAssociateCode());
			}
		} 	
		response.getWriter().write(Integer.toString(status));

	}




	public void searchDriver(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		String driver1 = request.getParameter("driver");
		DriverLocationBO driverLocBo = new DriverLocationBO();
		if (adminBO.getDispatchBasedOnVehicleOrDriver() == 1) {
			driverLocBo.setDriverid(driver1);
		} else {
			driverLocBo.setVehicleNo(driver1);
		}
		ArrayList<DriverLocationBO> al_list = new ArrayList<DriverLocationBO>();
		al_list = UtilityDAO.searchDrivers(adminBO.getAssociateCode(), driverLocBo);
		request.setAttribute("Drivers", al_list);
		getServletContext().getRequestDispatcher("/Company/DriverDetails.jsp").forward(request, response);
	}

	public void driverAutoComplete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		DriverLocationBO driverLocBo = new DriverLocationBO();
		ArrayList<DriverLocationBO> al_list = UtilityDAO.searchDrivers(adminBO.getAssociateCode(), driverLocBo);
		StringBuffer buffer = new StringBuffer();
		for (int i = 0; i < al_list.size(); i++) {
			if (adminBO.getDispatchBasedOnVehicleOrDriver() == 1) {
				buffer.append("" + al_list.get(i).getDriverid() + "^^^");
			} else {
				buffer.append("" + al_list.get(i).getVehicleNo() + "^^^");
			}
		}
		response.getWriter().write(buffer.toString());
	}

	public void deleteJobSummary(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		AdministrationDAO.deleteOpenRequestSummary(request.getParameter("tripId"), adminBO.getAssociateCode(), "1", adminBO.getMasterAssociateCode());
		AuditDAO.insertJobLogs("Customer Cancel", request.getParameter("tripId"),adminBO.getAssociateCode(),TDSConstants.customerCancelledRequest, adminBO.getUid(),"","",adminBO.getMasterAssociateCode());
		getServletContext().getRequestDispatcher("/Company/openRequestSummary.jsp").forward(request, response);
	}

	public void getJobCount(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int count=0;
		StringBuffer buffer = new StringBuffer();
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		ArrayList<String> al_list = DispatchDAO.getJobCount(adminBO.getAssociateCode(), adminBO.getTimeZone());
		int total = DispatchDAO.getFlagJobCount(adminBO.getAssociateCode(), adminBO.getTimeZone());
		buffer.append("Flag trips:" + total + "<br>");
		for (int i = 0; i < al_list.size(); i = i + 2) {
			if (al_list.get(i).equals(TDSConstants.tripCompleted + "") || al_list.get(i).equals("70")) {
				count=count+Integer.parseInt(al_list.get(i + 1));
				//				buffer.append("Completed:" + al_list.get(i + 1) + "<br>");
			} else if (al_list.get(i).equals("50")) {
				buffer.append("CustomerCancelled:" + al_list.get(i + 1) + "<br>");
			} else if (al_list.get(i).equals("55")) {
				buffer.append("Operator Cancelled:" + al_list.get(i + 1) + "<br>");
			} else if (al_list.get(i).equals("51")) {
				buffer.append("NoShow:" + al_list.get(i + 1) + "<br>");
			} else if (al_list.get(i).equals("52")) {
				buffer.append("CouldntService:" + al_list.get(i) + "<br>");
			} else if (al_list.get(i).equals("40")) {
				buffer.append("Allocated:" + al_list.get(i + 1) + "<br>");
			} else if (al_list.get(i).equals("43")) {
				buffer.append("OnRotue:" + al_list.get(i + 1) + "<br>");
			} else if (al_list.get(i).equals("47")) {
				buffer.append("TripStarted:" + al_list.get(i + 1) + "<br>");
			} else if (al_list.get(i).equals("4")) {
				buffer.append("PerformingDispatch:" + al_list.get(i + 1) + "<br>");
			} else if (al_list.get(i).equals("8")) {
				buffer.append("DispatchNotStarted:" + al_list.get(i + 1) + "<br>");
			} else if (al_list.get(i).equals("30")) {
				buffer.append("BroadcastRequest:" + al_list.get(i + 1) + "<br>");
			} else if (al_list.get(i).equals("99")) {
				buffer.append("OnHold:" + al_list.get(i + 1) + "<br>");
			} else if (al_list.get(i).equals("49")) {
				buffer.append("Status49:" + al_list.get(i + 1) + "<br>");
			} else if (al_list.get(i).equals("25")) {
				buffer.append("couldntFindDrivers:" + al_list.get(i + 1) + "<br>");
			} else if (al_list.get(i).equals("90")) {
				buffer.append("SrHoldJob:" + al_list.get(i + 1) + "<br>");
			}

		}
		if(count!=0){
			buffer.append("Completed:" + count + "<br>");
		}
		response.getWriter().write(buffer.toString());
	}

	public void sendIndividualMessageToDriverFromJob(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String tripID = request.getParameter("tripId");
		String driverID = request.getParameter("driverId");
		String msg = request.getParameter("msg");
		String riderName = request.getParameter("riderName");
		ApplicationPoolBO poolBO = (ApplicationPoolBO) getServletContext().getAttribute("poolBO");

		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		msg = "TripID:" + tripID + "For the Rider:" + riderName + "-->" + msg;

		String vMessage = request.getParameter("vMsg");
		String[] finalMessage;
		if (vMessage == "true") {
			finalMessage = MessageGenerateJSON.generatePrivateMessage(msg, "VMLO", System.currentTimeMillis());
		} else {
			finalMessage = MessageGenerateJSON.generatePrivateMessage(msg, "TXLO", System.currentTimeMillis());
		}
		String msgForSave = "TripID:" + tripID + ": " + request.getParameter("msg").replace("'", "");
		DispatchDAO.sendMessage(msgForSave, driverID, adminBO, 0);
		if (driverID != null && !driverID.equals("")) {
			DriverCabQueueBean cabQueueBean = DispatchDAO.getDriverByDriverID(adminBO.getMasterAssociateCode(), driverID, 1);
			Messaging messageDriver = new Messaging(getServletContext(), cabQueueBean, finalMessage, poolBO, adminBO.getMasterAssociateCode());
			messageDriver.start();
		}
	}

	public void getCompanyLogo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		StringBuffer buffer = new StringBuffer();
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		byte[] imageBlob = AdministrationDAO.getCompanyLogo(adminBO.getMasterAssociateCode());
		if (imageBlob != null) {
			response.setContentType("image/gif");
			response.addHeader("Content-Disposition", "attachment; filename=" + adminBO.getMasterAssociateCode() + ".jpg");
			response.setContentLength((int) imageBlob.length);
			ServletOutputStream o = response.getOutputStream();
			o.write(imageBlob);
			o.flush();
			o.close();
		}
	}

	public void sendToNearestDrivers(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		StringBuffer buffer = new StringBuffer();
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		ApplicationPoolBO poolBO = (ApplicationPoolBO) getServletContext().getAttribute("poolBO");

		double lat = Double.valueOf(request.getParameter("lat"));
		double longi = Double.valueOf(request.getParameter("longi"));
		double distance = Double.valueOf(request.getParameter("distance"));
		String msg = request.getParameter("msg").replace("'", "");
		String vMessage = request.getParameter("vMsg");
		String[] finalMessage;
		if (vMessage == "true") {
			finalMessage = MessageGenerateJSON.generatePrivateMessage(msg, "VMLO", System.currentTimeMillis());
		} else {
			finalMessage = MessageGenerateJSON.generatePrivateMessage(msg, "TXLO", System.currentTimeMillis());
		}
		ArrayList<String> fleets = new ArrayList<String>();
		if(adminBO.getAssociateCode().equals(adminBO.getMasterAssociateCode()) && adminBO.getFleetDispatch()==1){
			fleets = adminBO.getCompanyList();
		} else {
			fleets.add(adminBO.getAssociateCode());
		}
		ArrayList<DriverCabQueueBean> listOfDriver = DispatchDAO.allDriversWithInADistance(fleets, lat, longi, distance, null, 0,distance);
		Messaging messageDriver = new Messaging(getServletContext(), listOfDriver, finalMessage, poolBO, adminBO.getMasterAssociateCode());
		messageDriver.start();
		for (int i = 0; i < listOfDriver.size(); i++) {
			DispatchDAO.sendMessage(msg, listOfDriver.get(i).getDriverid(), adminBO, 0);
		}
	}

	public void jobsForCurrentDay(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		ArrayList<OpenRequestBO> jobs = AdministrationDAO.getJobsForToday(adminBO.getAssociateCode(), request.getParameter("driverId"), adminBO.getTimeZone());
		JSONArray jobsList = new JSONArray();

		for (int i = 0; i < jobs.size(); i++) {
			JSONObject todayJobs = new JSONObject();
			try {
				todayJobs.put("DrId", jobs.get(i).getDriverid());
				todayJobs.put("VehNo", jobs.get(i).getVehicleNo());
				todayJobs.put("Nam", jobs.get(i).getName());
				todayJobs.put("Trip", jobs.get(i).getTripid());
				todayJobs.put("Qno", jobs.get(i).getQueueno());
				todayJobs.put("Flag", jobs.get(i).getDrProfile());
				todayJobs.put("Add1", jobs.get(i).getScity());
				todayJobs.put("Stat", jobs.get(i).getTripStatus());
				todayJobs.put("SerTime", jobs.get(i).getRequestTime());
				// todayJobs.put("Lat", jobs.get(i).getSlat());
				// todayJobs.put("Long", jobs.get(i).getSlong());
				todayJobs.put("SerDat", jobs.get(i).getSdate());
				jobsList.put(todayJobs);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		response.getWriter().write(jobsList.toString());
	}

	public void checkCount(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		ArrayList<OpenRequestBO> totalJobs = AdministrationDAO.getCountJobsForDriver(adminBO.getAssociateCode(), adminBO.getUid(), adminBO.getTimeZone());
		JSONArray jobsList = new JSONArray();

		for (int i = 0; i < totalJobs.size(); i++) {
			JSONObject todayJobs = new JSONObject();
			try {
				todayJobs.put("DrId", totalJobs.get(i).getDriverid());
				todayJobs.put("totalJobs", totalJobs.get(i).getSetTotalJobsPerDriver());
				jobsList.put(todayJobs);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		response.getWriter().write(jobsList.toString());
	}

	public void bookToZone(HttpServletRequest request, HttpServletResponse reponse) throws ServletException, IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		String zone = request.getParameter("zoneName");
		String driver = request.getParameter("driverZone");
		int success = 0;
		boolean currentQueue = ServiceRequestDAO.checkQueueIsAvailableForDriver(adminBO.getMasterAssociateCode(), zone, driver);
		if (currentQueue) {
			RegistrationDAO.deleteDriverFromQueueWithoutQueueID(driver, adminBO.getAssociateCode());
			success = RegistrationDAO.insertzonedetails(driver, adminBO.getAssociateCode(), zone, adminBO.getMasterAssociateCode());
			//ServiceRequestDAO.insertDriver_BooktoZoneHistory(adminBO.getUid(), adminBO.getVehicleNo(), zone, "0", "Driver forcely logged into Zone by ", adminBO.getMasterAssociateCode());
			//ServiceRequestDAO.Update_BtZ_ZoneLogin(driver, "", zone, "0", "Driver Forcely Logged into Zone by "+adminBO.getUid(), adminBO.getAssociateCode(),adminBO.getMasterAssociateCode());
			ServiceRequestDAO.insert_DriverLoginToQueue(driver, "", zone, adminBO.getAssociateCode(), adminBO.getMasterAssociateCode(), "1", "Driver("+driver+") forcely logged into zone:"+zone+" by Operator "+adminBO.getUid());
			if(ZoneDAO.getShortDistanceType(adminBO.getMasterAssociateCode(), zone)==1){
				ServiceRequestDAO.updateDriverAvailability("Z", driver, adminBO.getAssociateCode());
			}
		} else {
			// success = RegistrationDAO.insertzonedetails(driver,
			// (adminBO.getAssociateCode()),
			// zone,adminBO.getMasterAssociateCode());
		}

	}

	public void messageHistory(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBo = (AdminRegistrationBO) request.getSession().getAttribute("user");
		String update = "";
		String lastKey = "";
		if (request.getParameter("update") != null) {
			update = request.getParameter("update");
		} else {
			update = "0";
		}
		if (request.getParameter("lastKey") != null) {
			lastKey = request.getParameter("lastKey");
		} else {
			lastKey = "0";
		}
		ArrayList<MessageBO> messageList = DispatchDAO.getAllMessage(adminBo.getMasterAssociateCode(), update, lastKey, adminBo.getTimeZone());
		JSONArray messageArray = new JSONArray();
		for (int i = 0; i < messageList.size(); i++) {
			JSONObject msgObj = new JSONObject();
			try {
				msgObj.put("dri", messageList.get(i).getDriverid());
				msgObj.put("msgText", messageList.get(i).getMessage() != null ? messageList.get(i).getMessage().replace(" ", "&#32;") : "");
				msgObj.put("sender", messageList.get(i).getEnteredBy());
				msgObj.put("time", messageList.get(i).getTime());
				msgObj.put("date", messageList.get(i).getMsgDate());
				msgObj.put("key", messageList.get(i).getMsgKey());
				msgObj.put("EMC", messageList.get(i).getEmergencyCheck());
				messageArray.put(msgObj);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		//System.out.println("resp:"+messageArray.toString());
		response.getWriter().write(messageArray.toString());

	}

	public void mobilesms(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBo = (AdminRegistrationBO) request.getSession().getAttribute("user");
		String update = "";
		String lastKey = "";
		if (request.getParameter("update") != null) {
			update = request.getParameter("update");
		} else {
			update = "0";
		}
		if (request.getParameter("lastKey") != null) {
			lastKey = request.getParameter("lastKey");
		} else {
			lastKey = "0";
		}
		ArrayList<SMSBean> messageList = PhoneDAO.getAllMessage(adminBo.getAssociateCode(), update, lastKey, adminBo.getTimeZone());
		JSONArray messageArray = new JSONArray();
		for (int i = 0; i < messageList.size(); i++) {
			JSONObject msgObj = new JSONObject();
			try {
				msgObj.put("Pno", messageList.get(i).getFromno());
				msgObj.put("mess", messageList.get(i).getMessage());
				msgObj.put("time", messageList.get(i).getTime());
				msgObj.put("key", messageList.get(i).getKey());
				messageArray.put(msgObj);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		response.getWriter().write(messageArray.toString());
		//  System.out.println("list"+messageArray.toString());
	}

	public void mobilesmsHistory(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBo = (AdminRegistrationBO) request.getSession().getAttribute("user");
		ArrayList<SMSBean> messageList = PhoneDAO.getAllReadMessage(adminBo.getAssociateCode(), adminBo.getTimeZone());
		JSONArray messageArray = new JSONArray();
		for (int i = 0; i < messageList.size(); i++) {
			JSONObject msgObj = new JSONObject();
			try {
				msgObj.put("Pno", messageList.get(i).getFromno());
				msgObj.put("mess", messageList.get(i).getMessage());
				msgObj.put("time", messageList.get(i).getTime());
				msgObj.put("key", messageList.get(i).getKey());
				messageArray.put(msgObj);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		response.getWriter().write(messageArray.toString());
		//  System.out.println("list"+messageArray.toString());
	}

	public void jobHistory(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBo = (AdminRegistrationBO) request.getSession().getAttribute("user");
		String phoneNo = TDSValidation.getDBPhoneFormat(request.getParameter("phoneNo"));
		ArrayList<OpenRequestBO> jobList = RegistrationDAO.getpreviousDetails(phoneNo, adminBo);
		JSONArray jobsArray = new JSONArray();
		for (int i = 0; i < jobList.size(); i++) {
			JSONObject jobsObj = new JSONObject();
			try {
				jobsObj.put("tripId", jobList.get(i).getTripid());
				jobsObj.put("driverId", jobList.get(i).getDriverid());
				jobsObj.put("phone", jobList.get(i).getPhone());
				jobsObj.put("sadd1", jobList.get(i).getSadd1());
				jobsObj.put("sadd2", jobList.get(i).getSadd2());
				jobsObj.put("scity", jobList.get(i).getScity());
				jobsObj.put("sstate", jobList.get(i).getSstate());
				jobsObj.put("szip", jobList.get(i).getSzip());
				jobsObj.put("sspl", jobList.get(i).getSpecialIns());
				jobsObj.put("eadd1", jobList.get(i).getEadd1());
				jobsObj.put("eadd2", jobList.get(i).getEadd2());
				jobsObj.put("ecity", jobList.get(i).getEcity());
				jobsObj.put("estate", jobList.get(i).getEstate());
				jobsObj.put("ezip", jobList.get(i).getEzip());
				jobsObj.put("slat", jobList.get(i).getSlat());
				jobsObj.put("slong", jobList.get(i).getSlong());
				jobsObj.put("elat", jobList.get(i).getEdlatitude());
				jobsObj.put("elong", jobList.get(i).getEdlongitude());
				jobsObj.put("comments", jobList.get(i).getComments());
				jobsObj.put("land", jobList.get(i).getSlandmark());
				jobsObj.put("payType", jobList.get(i).getPaytype());
				jobsObj.put("payAcc", jobList.get(i).getAcct());
				jobsObj.put("drProf", jobList.get(i).getDrProfile());
				jobsObj.put("queueNo", jobList.get(i).getQueueno());
				jobsObj.put("shared", jobList.get(i).getTypeOfRide());
				jobsObj.put("repeat", jobList.get(i).getRepeatGroup());
				jobsObj.put("eland", jobList.get(i).getElandmark());
				jobsObj.put("noOfPass", jobList.get(i).getNumOfPassengers());
				jobsObj.put("drop", jobList.get(i).getDropTime());
				jobsObj.put("date", jobList.get(i).getSdate());
				jobsObj.put("time", jobList.get(i).getSttime());
				jobsObj.put("name", jobList.get(i).getName());
				jobsArray.put(jobsObj);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		response.getWriter().write(jobsArray.toString());

	}

	public void makeIVRPhoneCall(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBo = (AdminRegistrationBO) request.getSession().getAttribute("user");
		String tripId = request.getParameter("trip_id");
		String driverId = request.getParameter("driverId");
		String cabNo = request.getParameter("cabNO");
		DriverLocationBO drBo = DispatchDAO.getPhoneForDriverID(driverId, adminBo.getAssociateCode());
		if (drBo != null) {
			String timeZoneOffset = adminBo.getTimeZone();
			String phone = adminBo.getPhonePrefix() + drBo.getPhone();
			IVRDriverCaller ivr = new IVRDriverCaller(driverId, tripId, phone, "", "", "", cabNo, adminBo.getAssociateCode(), timeZoneOffset, adminBo.getMasterAssociateCode(),this);
			ivr.run();
		}
	}

	// Following are Shared Ride Functions
	public void sharedRide(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		AdminRegistrationBO adminBo = (AdminRegistrationBO) request.getSession().getAttribute("user");
		String tripId = request.getParameter("tripId");
		String exRouteNoList = request.getParameter("routeNo");
		String cabNo = request.getParameter("cabNo");
		String driverId = "";
		String statusForLogs = "8";
		String dateValue = "";
		int indPay = 0;
		String screenFrom = "";
		if (request.getParameter("dateValue") != null && request.getParameter("dateValue") != "") {
			dateValue = request.getParameter("dateValue");
		}
		if (request.getParameter("driverId") != null && request.getParameter("driverId") != "") {
			driverId = request.getParameter("driverId");
			statusForLogs = "40";
		}
		if (request.getParameter("indPay") != null && !request.getParameter("indPay").equalsIgnoreCase("")) {
			indPay = Integer.parseInt(request.getParameter("indPay"));
		}
		if (request.getParameter("screenFrom") != null && !request.getParameter("screenFrom").equalsIgnoreCase("")) {
			screenFrom = "individual";
		}
		ArrayList<OpenRequestBO> arList = new ArrayList<OpenRequestBO>();
		String[] joblist = tripId.split(",");
		String total = Integer.toString(joblist.length);
		for (int i = 0; i < joblist.length; i++) {
			OpenRequestBO orBo = new OpenRequestBO();
			String[] tripIdWithOrder = joblist[i].split(";");
			orBo.setTripid(tripIdWithOrder[0]);
			orBo.setPickupOrder(tripIdWithOrder[1]);
			orBo.setPickupOrderSwitch(Integer.parseInt(tripIdWithOrder[2]));
			arList.add(orBo);
		}
		String[] exRouteNo = exRouteNoList.split(",");
		int routeNo = SharedRideDAO.insertSharedRide(adminBo, total, driverId, cabNo);
		String routeNoValue = Integer.toString(routeNo);
		int result2 = SharedRideDAO.updateOpenrequestForShared(adminBo.getAssociateCode(), routeNoValue, arList, exRouteNo, driverId, cabNo, indPay);
		int result = SharedRideDAO.insertToSharedDetails(adminBo.getAssociateCode(), routeNoValue, arList, indPay);
		SharedRideDAO.insertSharedLogEntries(adminBo, routeNoValue, "Group Created", statusForLogs);
		// SharedRideDAO.insertSharedLogs(adminBo.getAssociateCode(),
		// routeNoValue, arList ,driverId);
		ArrayList<OpenRequestBO> al_list = AdministrationDAO.getOpnReqDashForShared(adminBo.getAssociateCode(), 0, adminBo.getTimeZone(), 40, dateValue);
		request.setAttribute("JobShared", al_list);
		if (al_list.size() > 0 && al_list.get(0).getSdate() != null) {
			request.setAttribute("serviceDate", al_list.get(0).getSdate());
		}
		if (!screenFrom.equalsIgnoreCase("individual")) {
			getServletContext().getRequestDispatcher("/Company/SharedRide.jsp").forward(request, response);
		}
	}

	public void getSharedSummary(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBo = (AdminRegistrationBO) request.getSession().getAttribute("user");
		String date = request.getParameter("date") != null ? request.getParameter("date") : "";
		ArrayList<OpenRequestBO> al_list = SharedRideDAO.getSharedRideGroups(adminBo.getAssociateCode(), adminBo.getTimeZone(), date);
		request.setAttribute("JobsListDetails", al_list);
		getServletContext().getRequestDispatcher("/Company/SharedRideDetails.jsp").forward(request, response);
	}

	public void sharedRideAjax(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBo = (AdminRegistrationBO) request.getSession().getAttribute("user");
		String routeNo = request.getParameter("routeNo") != null ? request.getParameter("routeNo") : "";
		String date = request.getParameter("date") != null ? request.getParameter("date") : "";
		ArrayList<OpenRequestBO> al_list = SharedRideDAO.getSharedRideDetails(adminBo.getAssociateCode(), routeNo, date, adminBo.getTimeZone());
		String statusValue = "";
		if (al_list.size() == 0) {
			response.getWriter().write("No Records Found");
		} else {
			response.getWriter().write("<table id=\"details\"><tr><th class=\"firstCol\">TripId</th><th class=\"firstCol\">Pickup Order</th><th class=\"firstCol\">Start Address</th><th class=\"firstCol\">St Zone</th><th class=\"firstCol\">End Address</th><th class=\"firstCol\">Ed Zone</th><th class=\"firstCol\">Status</th></tr>");
			for (int i = 0; i < al_list.size(); i++) {
				if (al_list.get(i).getStatus().equalsIgnoreCase(TDSConstants.tripCompleted + "")) {
					statusValue = "Trip Completed";
				} else if (al_list.get(i).getStatus().equalsIgnoreCase(TDSConstants.customerCancelledRequest + "")) {
					statusValue = "Customer Canceled";
				} else if (al_list.get(i).getStatus().equalsIgnoreCase(TDSConstants.operatorCancelledRequest + "")) {
					statusValue = "Operator Canceled";
				} else if (al_list.get(i).getStatus().equalsIgnoreCase(TDSConstants.noShowRequest + "")) {
					statusValue = "No Show";
				} else if (al_list.get(i).getStatus().equalsIgnoreCase(TDSConstants.operatorCancelledRequest + "")) {
					statusValue = "Operator Canceled";
				} else if (al_list.get(i).getStatus().equalsIgnoreCase(TDSConstants.tripStarted + "")) {
					statusValue = "Trip Started";
				} else if (al_list.get(i).getStatus().equalsIgnoreCase(TDSConstants.tripOnHold + "")) {
					statusValue = "Trip On Hold";
				} else if (al_list.get(i).getStatus().equalsIgnoreCase(TDSConstants.companyCouldntService + "")) {
					statusValue = "Couldn't Service";
				} else if (al_list.get(i).getStatus().equalsIgnoreCase(TDSConstants.jobAllocated + "")) {
					statusValue = "Job Allocated";
				} else if (al_list.get(i).getStatus().equalsIgnoreCase(TDSConstants.onRotueToPickup + "")) {
					statusValue = "On Route";
				} else if (al_list.get(i).getStatus().equalsIgnoreCase(TDSConstants.performingDispatch + "")) {
					statusValue = "Performing Dispatch";
				} else if (al_list.get(i).getStatus().equalsIgnoreCase(TDSConstants.newRequestDispatchProcessesNotStarted + "")) {
					statusValue = "Dispatch Not processed Yet";
				} else if (al_list.get(i).getStatus().equalsIgnoreCase(TDSConstants.broadcastRequest + "")) {
					statusValue = "Broadcasted";
				} else if (al_list.get(i).getStatus().equalsIgnoreCase(TDSConstants.onSite + "")) {
					statusValue = "OnSite";
				} else if (al_list.get(i).getStatus().equalsIgnoreCase(TDSConstants.srAllocatedToCab + "")) {
					statusValue = "SR Allocated";
				} else if (al_list.get(i).getStatus().equalsIgnoreCase(TDSConstants.driverReportedNoShow + "")) {
					statusValue = "Driver Reported NoShow";
				} else if (al_list.get(i).getStatus().equalsIgnoreCase(TDSConstants.dummyTripId + "")) {
					statusValue = "Dummy TripId";
				} else if (al_list.get(i).getStatus().equalsIgnoreCase(TDSConstants.staleCall + "")) {
					statusValue = "Stall Call";
				} else if (al_list.get(i).getStatus().equalsIgnoreCase(TDSConstants.srJobHold + "")) {
					statusValue = "SR Job On Hold";
				} else if (al_list.get(i).getStatus().equalsIgnoreCase(TDSConstants.tripOnHold + "")) {
					statusValue = "Trip On Hold";
				} else if (al_list.get(i).getStatus().equalsIgnoreCase(TDSConstants.performingDispatchProcesses + "")) {
					statusValue = "Performing Dispatch Process";
				} else {
					statusValue = "Unkonown Status";
				}
				String statusOfJob = "";
				String color = "";
				if (al_list.get(i).getPickupOrderSwitch() == 0) {
					statusOfJob = "Pickup";
					color = "lightgreen";
				} else {
					statusOfJob = "DropOff";
					color = "red";
				}
				response.getWriter().write("<tr><td align=\"center\" style=\"padding:3px;background-color: " + color + ";color:white;border-radius:15px 3px 3px 15px;cursor:pointer\"><font color=\"black\" onclick=\"directionShow('" + al_list.get(i).getScity() + "','" + al_list.get(i).getDriverid() + "','" + al_list.get(i).getSlat() + "','" + al_list.get(i).getSlong() + "','" + al_list.get(i).getEdlatitude() + "','" + al_list.get(i).getEdlongitude() + "')\">" + al_list.get(i).getTripid() + "</font></td><td align=\"center\" style=\"background-color: lightgrey;border-radius:3px\"><font color=\"black\">" + al_list.get(i).getPickupOrder() + "</font></td><td align=\"center\" style=\"background-color: lightgrey\"><font color=\"black\">" + al_list.get(i).getScity() + "</font></td><td align=\"center\" style=\"background-color: lightgrey\"><font color=\"black\">" + al_list.get(i).getQueueno() + "</font></td><td align=\"center\" style=\"background-color: lightgrey\"><font color=\"black\">" + al_list.get(i).getEcity() + "</font></td><td align=\"center\" style=\"background-color: lightgrey\"><font color=\"black\">" + al_list.get(i).getEndQueueno() + "</font></td><td align=\"center\" style=\"background-color: lightgrey\"><font color=\"black\">" + statusValue + "</font></td><td align=\"center\" style=\"background-color: lightgrey\"><font color=\"black\">" + statusOfJob + "</font></td><td><input type='hidden' name='routeNoAjax' id='routeNoAjax' value='" + al_list.get(i).getRouteNumber() + "'><input type=\"button\" name=\"splitFromGroup\"id=\"splitFromGroup\" value=\"Split\"   onclick='splitFromGroup(" + al_list.get(i).getTripid() + "," + al_list.get(i).getRouteNumber() + "," + al_list.get(i).getPickupOrder() + ")'></td></tr>");
			}
			response.getWriter().write("</table>");

		}
	}

	public void updateDriverForShared(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		AdminRegistrationBO adminBo = (AdminRegistrationBO) request.getSession().getAttribute("user");
		ApplicationPoolBO poolBO = (ApplicationPoolBO) getServletContext().getAttribute("poolBO");
		long msgID = System.currentTimeMillis();
		String[] message = new String[2];
		DriverCabQueueBean cabQueueBean = null;
		String driver = request.getParameter("driverId");
		String routeNo = request.getParameter("routeNo");
		String cabNo = request.getParameter("cabNo");
		int jobStatus=TDSConstants.performingDispatchProcesses;
		if(!driver.equals("") && driver.substring(0,1).equalsIgnoreCase("F")){
			jobStatus = TDSConstants.jobAllocated;
			driver = driver.substring(1);
			cabQueueBean = DispatchDAO.getDriverByDriverID(adminBo.getAssociateCode(), driver, 1);
		}
		OpenRequestBO openBo = ServiceRequestDAO.getDriverIdByTripId(adminBo.getAssociateCode(), routeNo, 1);
		SharedRideDAO.allocateDriverForShared(adminBo.getAssociateCode(), driver, routeNo, cabNo,jobStatus);
		if (driver != null && !driver.equals("")) {
			SharedRideDAO.insertSharedLogEntries(adminBo, routeNo, "Updating Driver", "40");
			ArrayList<OpenRequestBO> jobs = SharedRideDAO.getOpenRequestForRouteByTripID(routeNo, adminBo, "1");
			cabQueueBean = DispatchDAO.getDriverByDriverID(adminBo.getAssociateCode(), driver, 1);
			if(jobStatus == TDSConstants.performingDispatchProcesses){
				message = MessageGenerateJSON.generateMessage(jobs.get(0), "A", msgID, adminBo.getMasterAssociateCode());
			} else if ((jobStatus == TDSConstants.jobAllocated)){
				message = MessageGenerateJSON.generateMessageAfterAcceptance(jobs.get(0), "FA", msgID, adminBo.getMasterAssociateCode());
			}
			//			message = MessageGenerateJSON.generateMessageAfterAcceptance(jobs.get(0), "A", msgID, adminBo.getMasterAssociateCode());
			Messaging oneSMS = new Messaging(getServletContext(), cabQueueBean, message, poolBO, adminBo.getAssociateCode());
			oneSMS.start();
		} else {
			SharedRideDAO.insertSharedLogEntries(adminBo, routeNo, "Taking Job Away From Driver", "40");
			ServiceRequestDAO.updateOpenRequestStatus(adminBo.getAssociateCode(), openBo.getTripid(), TDSConstants.performingDispatchProcesses+"", "", "", "", "");
			if (openBo.getDriverid() != null && !openBo.getDriverid().equals("")) {
				cabQueueBean = DispatchDAO.getDriverByDriverID(adminBo.getAssociateCode(), openBo.getDriverid(), 1);
				message = MessageGenerateJSON.generateMessage(openBo, "OC", System.currentTimeMillis(), adminBo.getAssociateCode());
				Messaging oneSMS = new Messaging(getServletContext(), cabQueueBean, message, poolBO, adminBo.getAssociateCode());
				oneSMS.start();
			}
		}
		String date = request.getParameter("date") != null ? request.getParameter("date") : "";
		ArrayList<OpenRequestBO> al_list = SharedRideDAO.getSharedRideGroups(adminBo.getAssociateCode(), adminBo.getTimeZone(), date);
		request.setAttribute("JobsListDetails", al_list);
		getServletContext().getRequestDispatcher("/Company/SharedRideDetails.jsp").forward(request, response);
	}

	public void deleteGroup(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBo = (AdminRegistrationBO) request.getSession().getAttribute("user");
		String routeNo = request.getParameter("routeNo");
		SharedRideDAO.deletegroup(adminBo.getAssociateCode(), routeNo);
		SharedRideDAO.insertSharedLogEntries(adminBo, routeNo, "Delete Group", "61");
		String date = request.getParameter("date") != null ? request.getParameter("date") : "";
		ArrayList<OpenRequestBO> al_list = SharedRideDAO.getSharedRideGroups(adminBo.getAssociateCode(), adminBo.getTimeZone(), date);
		request.setAttribute("JobsListDetails", al_list);
		getServletContext().getRequestDispatcher("/Company/SharedRideDetails.jsp").forward(request, response);
	}

	public void getSharedRide(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		int status = 0;
		String date = "";
		if (request.getParameter("date") != null && request.getParameter("date") != "") {
			date = request.getParameter("date");
		}
		if (request.getParameter("status") != null) {
			status = Integer.parseInt(request.getParameter("status"));
		}
		ArrayList<OpenRequestBO> al_list = AdministrationDAO.getOpnReqDashForShared(adminBO.getAssociateCode(), 0, adminBO.getTimeZone(), status, date);
		request.setAttribute("JobShared", al_list);
		if (al_list.size() > 0 && al_list.get(0).getSdate() != null) {
			request.setAttribute("serviceDate", al_list.get(0).getSdate());
		}
		getServletContext().getRequestDispatcher("/Company/SharedRide.jsp").forward(request, response);
	}

	public void updateSharedStatus(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		String routeNo = request.getParameter("routeNo");
		String status = request.getParameter("status");
		int result = SharedRideDAO.updateSharedStatus(adminBO.getAssociateCode(), routeNo, status);
		SharedRideDAO.insertSharedLogEntries(adminBO, routeNo, "Updating Status", status);
		String results = Integer.toString(result);
		response.getWriter().write(results);

	}

	public void createIndividualTrips(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		String tripId = request.getParameter("tripId");
		String[] trips = tripId.split(",");
		for (int k = 0; k < trips.length; k++) {
			trips[k] = trips[k].replaceAll("[^0-9]", "");
		}
		SharedRideDAO.createIndividualTrips(trips, adminBO.getAssociateCode());
		int status = 0;
		String date = "";
		if (request.getParameter("dateValue") != null && request.getParameter("dateValue") != "") {
			date = request.getParameter("dateValue");
		}
		if (request.getParameter("status") != null) {
			status = Integer.parseInt(request.getParameter("status"));
		}
		ArrayList<OpenRequestBO> al_list = AdministrationDAO.getOpnReqDashForShared(adminBO.getAssociateCode(), 0, adminBO.getTimeZone(), status, date);
		request.setAttribute("JobShared", al_list);
		if (al_list.size() > 0 && al_list.get(0).getSdate() != null) {
			request.setAttribute("serviceDate", al_list.get(0).getSdate());
		}
		getServletContext().getRequestDispatcher("/Company/SharedRide.jsp").forward(request, response);
	}

	// Above Are Shared Ride Functions
	public void zoneDetailsForDash(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		ArrayList<QueueBean> al_list = new ArrayList<QueueBean>();
		QueueBean queue = new QueueBean();
		al_list = AdministrationDAO.getDriverinQDash(adminBO.getAssociateCode(), 0, queue, adminBO.getMasterAssociateCode());
		request.setAttribute("al_list", al_list);
		getServletContext().getRequestDispatcher("/Company/ZoneWaiting.jsp").forward(request, response);
	}

	public void allZonesForDash(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, JSONException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		ArrayList<ZoneTableBeanSP> allZonesForCompany = (ArrayList<ZoneTableBeanSP>) getServletConfig().getServletContext().getAttribute((adminBO.getMasterAssociateCode() + "Zones"));
		String jsonResponse = Feed.getAllZones(adminBO.getMasterAssociateCode(), allZonesForCompany);
		response.getWriter().write(jsonResponse);
	}

	public void loginDriverFromDashboard(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String driverId = request.getParameter("driverId");
		String cabNo = request.getParameter("cabNo");
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		int status = AdministrationDAO.loginDriverDashboard(driverId, adminBO.getAssociateCode(), cabNo,adminBO.getMasterAssociateCode());
		if (status > 0) {
			AdministrationDAO.loginDriverDashboardForLD(driverId, adminBO.getAssociateCode(), cabNo, adminBO.getMasterAssociateCode());
		}
		String statusv = Integer.toString(status);
		response.getWriter().write(statusv);
	}

	public void positioningDriver(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String driverId = request.getParameter("driverId");
		String lat = request.getParameter("lat");
		String longi = request.getParameter("longi");
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		int status = AdministrationDAO.updateDriverLocation(driverId, adminBO.getAssociateCode(), lat, longi);
		String statusv = Integer.toString(status);
		response.getWriter().write(statusv);
	}

	public void ajaxSharedLogs(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		String routeNo = request.getParameter("routeNo");
		ArrayList<OpenRequestBO> orList = SharedRideDAO.getSharedLogs(adminBO.getAssociateCode(), routeNo);
		JSONArray array = new JSONArray();
		if (orList.size() > 0) {
			for (int i = 0; i < orList.size(); i++) {
				JSONObject sharedLogs = new JSONObject();
				try {
					sharedLogs.put("TI", orList.get(i).getTripid());
					sharedLogs.put("SA", orList.get(i).getStatus());
					sharedLogs.put("CT", orList.get(i).getCreatedTime());
					sharedLogs.put("CB", orList.get(i).getCreatedBy());
					array.put(sharedLogs);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		String responseString = array.toString();
		response.getWriter().write(responseString.toString());
	}

	public void changeMultipleJobToAnotherFleet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		String tripId = request.getParameter("tripId");
		String fleetNo = request.getParameter("fleetNo");
		String fromFleetName = "";
		String toFleetName = "";
		ArrayList<FleetBO> fleetList = (ArrayList) request.getSession().getAttribute("fleetList");
		for (int j = 0; j < fleetList.size(); j++) {
			if (fromFleetName.equals("")) {
				fromFleetName = fleetList.get(j).getFleetNumber().equals(adminBO.getAssociateCode()) ? fleetList.get(j).getFleetName() : "";
			}
			if (toFleetName.equals("")) {
				toFleetName = fleetList.get(j).getFleetNumber().equals(fleetNo) ? fleetList.get(j).getFleetName() : "";
			}
		}
		AuditDAO.insertJobLogs("Job Changed from Fleet " + fromFleetName + " to " + toFleetName, tripId, adminBO.getAssociateCode(), 0, adminBO.getUid(), "", "", adminBO.getMasterAssociateCode());
		int status = AdministrationDAO.changeMultipleJobToAnotherFleet(tripId, fleetNo, adminBO.getAssociateCode(), adminBO.getUid(), adminBO.getMasterAssociateCode());
		response.getWriter().write(Integer.toString(status));
	}

	public void changeJobToAnotherFleet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		String tripId = request.getParameter("tripId");
		System.out.println("tripid"+request.getParameter("tripId"));
		String fleetNo = request.getParameter("fleetNo");
		String fromFleetName = "";
		String toFleetName = "";

		ArrayList<FleetBO> fleetList = (ArrayList) request.getSession().getAttribute("fleetList");
		for (int j = 0; j < fleetList.size(); j++) {
			if (fromFleetName.equals("")) {
				fromFleetName = fleetList.get(j).getFleetNumber().equals(adminBO.getAssociateCode()) ? fleetList.get(j).getFleetName() : "";
			}
			if (toFleetName.equals("")) {
				toFleetName = fleetList.get(j).getFleetNumber().equals(fleetNo) ? fleetList.get(j).getFleetName() : "";
			}
		}
		AuditDAO.insertJobLogs("Job Changed from Fleet " + fromFleetName + " to " + toFleetName, tripId, adminBO.getAssociateCode(), 0, adminBO.getUid(), "", "", adminBO.getMasterAssociateCode());
		int status = AdministrationDAO.changeJobToAnotherFleet(tripId, fleetNo, adminBO.getAssociateCode(), adminBO.getUid(), adminBO.getMasterAssociateCode());
		response.getWriter().write(Integer.toString(status));
	}


	public void submitCookies(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		Enumeration e = request.getParameterNames();
		ArrayList names = new ArrayList();
		ArrayList values = new ArrayList();
		while (e.hasMoreElements()) {
			String name = (String) e.nextElement();
			String value = request.getParameter(name).toString();
			if (name.contains("GAC")) {
				names.add(name);
				values.add(value);
			}
		}
		RequestDAO.updateCookies(names, values, adminBO.getUid(), adminBO.getMasterAssociateCode());
		response.sendRedirect("control");
	}

	public void setDriverAvailability(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		ApplicationPoolBO poolBO = (ApplicationPoolBO) getServletContext().getAttribute("poolBO");
		String driverId = request.getParameter("driverId");
		String cabNo = request.getParameter("cabNo");
		String statusValue = request.getParameter("status");
		if (statusValue.equalsIgnoreCase("N")) {
			statusValue = "Y";
		} else {
			statusValue = "N";
		}
		int status = AdministrationDAO.setDriverAvailability(cabNo, driverId, adminBO.getAssociateCode(), statusValue);
		String value = status > 0 ? "Not available" : "available";
		if (driverId != null && !driverId.equals("")) {
			DriverCabQueueBean cabQueueBean = DispatchDAO.getDriverByDriverID(adminBO.getAssociateCode(), driverId, 2);
			String[] message = new String[1];
			JSONArray mainArray = new JSONArray();
			JSONObject header = new JSONObject();
			try {
				header.put("rV", 1.2);
				header.put("rC", 200);
				header.put("Command", "SPLMSG");	    	
				header.put("STATUS",statusValue); 
				mainArray.put(0, header);
			}catch (Exception e) {
				e.printStackTrace();
			}			
			message[0]= mainArray.toString();
			Messaging oneSMS = new Messaging(getServletContext(), cabQueueBean, message, poolBO, adminBO.getAssociateCode());
			oneSMS.start();
		}
		// System.out.println("Driver is "+value);
	}

	public void activateDriver(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		boolean status = AdministrationDAO.deleteDriverDeactivation(request.getParameter("driverId"),adminBO);
		//System.out.println("sttus:"+status);
		response.getWriter().write(status?"Success":"Failure");
	}

	public void setEndZone(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		String tripId = request.getParameter("tripId");
		String endZone = request.getParameter("zoneKey");
		AdministrationDAO.updateOPenRequestForEndZone(endZone, adminBO.getAssociateCode(), tripId);
	}

	public void flagTripFromDash(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO m_adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		OpenRequestBO m_openRequestBO = new OpenRequestBO();

		String driverId = request.getParameter("driverId");
		String cabNo = request.getParameter("cabNo");
		// String driverName = request.getParameter("driverName");

		m_openRequestBO.setDriverid(driverId);
		m_openRequestBO.setAssociateCode(m_adminBO.getAssociateCode());
		m_openRequestBO.setDriverUName("FlagTrip");
		m_openRequestBO.setVehicleNo(cabNo);
		Calendar currentDate = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		String dateNow = sdf.format(currentDate.getTime());
		m_openRequestBO.setShrs("2525");
		m_openRequestBO.setPhone("0000000000");
		m_openRequestBO.setName("Flag Trip");
		m_openRequestBO.setSdate(dateNow);
		m_openRequestBO.setDays("0000000");
		m_openRequestBO.setTripSource(TDSConstants.Company);
		m_openRequestBO.setTypeOfRide("0");
		m_openRequestBO.setSadd1("Flag Trip");
		m_openRequestBO.setAmt(new BigDecimal("0"));
		m_openRequestBO.setChckTripStatus(TDSConstants.tripStarted);
		request.setAttribute("openrequestBOfromAction", m_openRequestBO);
		String errorReturn = OpenRequestUtil.saveOpenRequest(request, response, getServletConfig());
		int status = AdministrationDAO.setDriverAvailability(cabNo, driverId, m_adminBO.getAssociateCode(), "N");
		String value = status > 0 ? "Not available" : "available";
		// System.out.println("Driver is "+value);
	}

	public void onRouteFromDash(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO m_adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		OpenRequestBO m_openRequestBO = new OpenRequestBO();

		String driverId = request.getParameter("driverId");
		String cabNo = request.getParameter("cabNo");

		m_openRequestBO.setDriverid(driverId);
		m_openRequestBO.setAssociateCode(m_adminBO.getAssociateCode());
		m_openRequestBO.setDriverUName("OnRoute");
		m_openRequestBO.setVehicleNo(cabNo);
		Calendar currentDate = Calendar.getInstance();
		SimpleDateFormat formatter = new SimpleDateFormat("HHmm");
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		String dateNow = sdf.format(currentDate.getTime());
		String timeNow = formatter.format(currentDate.getTime());
		m_openRequestBO.setShrs(timeNow);
		m_openRequestBO.setPhone("0000000000");
		m_openRequestBO.setName("OnRoute");
		m_openRequestBO.setSdate(dateNow);
		m_openRequestBO.setDays("0000000");
		m_openRequestBO.setTypeOfRide("0");
		m_openRequestBO.setSadd1("OnRoute");
		m_openRequestBO.setAmt(new BigDecimal("0"));
		m_openRequestBO.setChckTripStatus(TDSConstants.tripStarted);
		request.setAttribute("openrequestBOfromAction", m_openRequestBO);
		String errorReturn = OpenRequestUtil.saveOpenRequest(request, response, getServletConfig());
		int status = AdministrationDAO.setDriverAvailability(cabNo, driverId, m_adminBO.getAssociateCode(), "N");
		String value = status > 0 ? "Not available" : "available";
		// System.out.println("Driver is "+value);
	}

	public void cannedMsg(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		ArrayList<MessageBO> arlist = DispatchDAO.getCannedMessages(adminBO.getMasterAssociateCode());
		String html = "";
		if (request.getParameter("from") != null && request.getParameter("from").equalsIgnoreCase("dash")) {
			html = "<img alt='' align='right' src='images/Dashboard/close.png' onclick='removeMessageTemplate()'><div style='width:700px;height:400px;max-height:700px;overflow:auto'><table style='width:100%;'>";
			for (int i = 0; i < arlist.size(); i++) {
				html += "<tr class='msgStyle'><td id=" + i + ">" + arlist.get(i).getMessage() + "<input type='hidden' name='cat" + i + "' id='cat" + i + "'value='" + arlist.get(i).getCategory() + "'></td><td><input type='radio' onClick=\"appendMsg('" + arlist.get(i).getMessage() + "')\" name='msgTemplate' id='msgTemplate' value='' ></td></tr>";
			}
			html += "</table></div>";
			response.getWriter().write(html);
		} else {
			request.setAttribute("messages", arlist);
			getServletContext().getRequestDispatcher("/jsp/cannedMessages.jsp").forward(request, response);
		}
	}

	public void createMsg(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		String msg = request.getParameter("msg");
		int category = Integer.parseInt(request.getParameter("category"));
		int status = DispatchDAO.createMsgTemplate(adminBO.getAssociateCode(), msg, category,adminBO.getMasterAssociateCode());
		response.getWriter().write(Integer.toString(status));
	}

	public void removeMsg(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		String msgKey = request.getParameter("msgKey");
		int status = DispatchDAO.removeMessageTemplate(adminBO.getAssociateCode(), msgKey);
		response.getWriter().write(Integer.toString(status));
	}

	public void errorReport(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ArrayList<String> alist = AdministrationDAO.loadErrors();
		request.setAttribute("alist", alist);
		getServletContext().getRequestDispatcher("/Company/ErrorReports.jsp").forward(request, response);
	}

	public void splitTripFromGroup(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		String tripId = request.getParameter("tripId");
		String routeNo = request.getParameter("routeNo");
		// String pickupOrder = request.getParameter("pickup");
		OpenRequestBO openBo = RequestDAO.getOpenRequestByTripID(tripId, adminBO.getAssociateCode(), adminBO.getTimeZone());
		int status = SharedRideDAO.splitTripFromGroup(adminBO.getAssociateCode(), tripId, routeNo, "SH");
		ApplicationPoolBO poolBO = (ApplicationPoolBO) getServletContext().getAttribute("poolBO");
		if (openBo.getDriverid()!=null && !openBo.getDriverid().equals("")) {
			DriverCabQueueBean cabQueueBean = DispatchDAO.getDriverByDriverID(adminBO.getAssociateCode(), openBo.getDriverid(), 1);
			String[] message = MessageGenerateJSON.generateMessage(openBo, "OC", System.currentTimeMillis(), adminBO.getAssociateCode());
			Messaging oneSMS = new Messaging(getServletContext(), cabQueueBean, message, poolBO, adminBO.getAssociateCode());
			oneSMS.start();
		}
		response.getWriter().write(Integer.toString(status));
	}

	public void addTripToGroup(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBo = (AdminRegistrationBO) request.getSession().getAttribute("user");
		String tripId = request.getParameter("tripId");
		String routeNo = request.getParameter("routeNo");
		ApplicationPoolBO poolBO = (ApplicationPoolBO) getServletContext().getAttribute("poolBO");
		String[] message = new String[2];
		DriverCabQueueBean cabQueueBean = null;
		int puOrder = Integer.parseInt(request.getParameter("pOrder").equals("") ? "0" : request.getParameter("pOrder"));
		int doOrder = Integer.parseInt(request.getParameter("dOrder").equals("") ? "0" : request.getParameter("dOrder"));
		int status = SharedRideDAO.addTripToGroup(adminBo.getAssociateCode(), tripId, routeNo, puOrder, doOrder);
		if (status > 0) {
			OpenRequestBO openBo = ServiceRequestDAO.getDriverIdByTripId(adminBo.getAssociateCode(), routeNo, 1);
			SharedRideDAO.insertSharedLogEntries(adminBo, routeNo, "Send Route Again To Driver", "40");
			if (openBo.getDriverid() != null && !openBo.getDriverid().equals("")) {
				cabQueueBean = DispatchDAO.getDriverByDriverID(adminBo.getAssociateCode(), openBo.getDriverid(), 1);
				message = MessageGenerateJSON.generateMessage(openBo, "AA", System.currentTimeMillis(), adminBo.getAssociateCode());
				Messaging oneSMS = new Messaging(getServletContext(), cabQueueBean, message, poolBO, adminBo.getAssociateCode());
				oneSMS.start();
			}
		}
		response.getWriter().write(Integer.toString(status));
	}

	public void makeMsgRead(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		String msgId = request.getParameter("msgId");
		int status = UtilityDAO.makeMsgRead(msgId, adminBO.getMasterAssociateCode());
		response.getWriter().write(Integer.toString(status));
	}

	public void makeSmsRead(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		String msgId = request.getParameter("msgId");
		int status = PhoneDAO.makeMsgRead(msgId, adminBO.getAssociateCode());
		response.getWriter().write(Integer.toString(status));

	}

	public void individualMsgHistory(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		String sender = request.getParameter("sender");
		String reciever = request.getParameter("receiver");
		ArrayList<MessageBO> messageList = DispatchDAO.getIndividualMessage(adminBO.getMasterAssociateCode(), sender, reciever, adminBO.getTimeZone());
		JSONArray messageArray = new JSONArray();
		for (int i = 0; i < messageList.size(); i++) {
			JSONObject msgObj = new JSONObject();
			try {
				msgObj.put("dri", messageList.get(i).getDriverid());
				msgObj.put("msgText", messageList.get(i).getMessage() != null ? messageList.get(i).getMessage().replace(" ", "&#32;") : "");
				msgObj.put("sender", messageList.get(i).getEnteredBy());
				msgObj.put("time", messageList.get(i).getTime());
				msgObj.put("date", messageList.get(i).getMsgDate());
				msgObj.put("key", messageList.get(i).getMsgKey());
				msgObj.put("EMC", messageList.get(i).getEmergencyCheck());
				messageArray.put(msgObj);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		response.getWriter().write(messageArray.toString());

	}

	public void splitTrips(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		String tripId = request.getParameter("tripId");
		String routeNo = request.getParameter("routeNo");

		String[] trips = tripId.split(",");
		for (int k = 0; k < trips.length; k++) {
			trips[k] = trips[k].replaceAll("[^0-9]", "");
		}
		String[] routes = routeNo.split(";");

		// String pickupOrder = request.getParameter("pickup");
		int status = SharedRideDAO.splitTripFromGroupForIndividual(adminBO.getAssociateCode(), trips, routes[0], "SH");
		response.getWriter().write(Integer.toString(status));
	}

	public void getAllValuesForDashboard(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/*
		 * @author Baskar
		 * 
		 * @param params,status,rtJobStatus,ZoneList-- For jobs
		 * 
		 * @param drStatus -- For Driver Details
		 * 
		 * @param shDate -- For Shared Ride
		 * 
		 * @param shDDate -- For Shared Ride Details
		 * 
		 * @return jobDetails,DriverDetails,Zone Waiting,SharedRide,Review
		 * Shared Ride
		 */
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		JSONArray totalArray = new JSONArray();
		JSONObject totalObject = new JSONObject();
		JSONArray array = new JSONArray();
		JSONArray driverList = new JSONArray();
		JSONArray zoneJsonArr = new JSONArray();
		JSONArray shArray = new JSONArray();
		JSONArray shDArray = new JSONArray();

		// JobDetails JSon Start
		ArrayList<OpenRequestBO> Address = null;
		int status = 0;
		String paramValues = "";
		String[] params = new String[5];
		if (request.getParameter("params") != null) {
			paramValues = request.getParameter("params");
		}
		if (request.getParameter("status") != null) {
			status = Integer.parseInt(request.getParameter("status"));
		}
		for (int i = 0; i < paramValues.length(); i++) {
			params[i] = paramValues.charAt(i) + "";

		}
		if (params[0] == "1") {
			int statusRtJobs = 0;
			if (request.getParameter("rtJobStatus") != null) {
				statusRtJobs = Integer.parseInt(request.getParameter("rtJobStatus"));
			}
			int route = 0;
			if (request.getParameter("routeJob") != null) {
				route = Integer.parseInt(request.getParameter("routeJob"));
			}
			if (request.getParameter("zoneList") != null && request.getParameter("zoneList") != "") {
				Address = AdministrationDAO.getOpnReqDash(adminBO.getAssociateCode(), 0, adminBO.getTimeZone(), status, request.getParameter("zoneList"), statusRtJobs, route);
			} else {
				Address = AdministrationDAO.getOpnReqDash(adminBO.getAssociateCode(), 0, adminBO.getTimeZone(), status, "", statusRtJobs, route);
			}
			if (request.getParameter("jsp") != null && request.getParameter("jsp").equalsIgnoreCase("yes")) {
				request.setAttribute("jobList", Address);
				getServletContext().getRequestDispatcher("/Company/JobDetailsExpand.jsp").forward(request, response);
			} else {
				for (int i = 0; i < Address.size(); i++) {
					JSONObject jobs = new JSONObject();
					try {
						jobs.put("D", Address.get(i).getDriverid() == null ? "" : Address.get(i).getDriverid());
						jobs.put("V", Address.get(i).getVehicleNo() == null ? "" : Address.get(i).getVehicleNo());
						jobs.put("N", Address.get(i).getName() == null ? "" : Address.get(i).getName());
						jobs.put("T", Address.get(i).getTripid() == null ? "" : Address.get(i).getTripid());
						jobs.put("Q", Address.get(i).getQueueno() == null ? "" : Address.get(i).getQueueno());
						jobs.put("A", Address.get(i).getScity() == null ? "" : Address.get(i).getScity());
						jobs.put("PT", Address.get(i).getPendingTime() == null ? "" : Address.get(i).getPendingTime());
						jobs.put("P", Address.get(i).getPhone() == null ? "" : Address.get(i).getPhone());
						jobs.put("S", Address.get(i).getTripStatus() == null ? "" : Address.get(i).getTripStatus());
						jobs.put("ST", Address.get(i).getRequestTime() == null ? "" : Address.get(i).getRequestTime());
						jobs.put("SD", Address.get(i).getSdate() == null ? "" : Address.get(i).getSdate());
						jobs.put("LA", Address.get(i).getSlat() == null ? "" : Address.get(i).getSlat());
						jobs.put("LO", Address.get(i).getSlong() == null ? "" : Address.get(i).getSlong());
						jobs.put("DD", Address.get(i).getDontDispatch());
						jobs.put("SR", Address.get(i).getTypeOfRide() == null ? "" : Address.get(i).getTypeOfRide());
						jobs.put("AC", Address.get(i).getPaytype() == null ? "" : Address.get(i).getPaytype());
						jobs.put("PC", Address.get(i).getPremiumCustomer() == 0 ? "" : Address.get(i).getPremiumCustomer());
						jobs.put("RT", Address.get(i).getRouteNumber() == null ? "" : Address.get(i).getRouteNumber());

						jobs.put("EDQ", Address.get(i).getEndQueueno() == null ? "" : Address.get(i).getEndQueueno());
						jobs.put("LMN", Address.get(i).getSlandmark() == null ? "" : Address.get(i).getSlandmark());
						jobs.put("SCTY", Address.get(i).getScity() == null ? "" : Address.get(i).getScity());
						jobs.put("ECTY", Address.get(i).getEcity() == null ? "" : Address.get(i).getEcity());

						String driverFlagValue = SystemUtils.getCompanyFlagDescription(getServletConfig(), adminBO.getAssociateCode(), Address.get(i).getDrProfile(), ";", 0);

						jobs.put("DP", driverFlagValue);
						array.put(jobs);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}

			}
		}
		// JobDetails Json End

		// Driver Details Json
		if (params[1] == "1") {

			ArrayList<DriverLocationBO> drivers = new ArrayList<DriverLocationBO>();
			long lastTime = -1;
			if (request.getParameter("drStatus").equals("4")) {
				drivers = UtilityDAO.getDriverLocationListByStatus(adminBO.getAssociateCode(), 4, adminBO.getTimeZone(), "");
			} else if (request.getParameter("drStatus").equals("3")) {
				drivers = UtilityDAO.getDriverLocationListByStatus(adminBO.getAssociateCode(), 3, adminBO.getTimeZone(), "");
			} else if (request.getParameter("drStatus").equals("5")) {
				drivers = UtilityDAO.getDriverLocationListByStatus(adminBO.getAssociateCode(), 5, adminBO.getTimeZone(), "");
			} else {
				drivers = UtilityDAO.getDriverLocationListByStatus(adminBO.getAssociateCode(), 1, adminBO.getTimeZone(), "");
			}

			for (int i = 0; i < drivers.size(); i++) {
				JSONObject driver = new JSONObject();
				try {
					driver.put("varDr", drivers.get(i).getDriverid());
					driver.put("varLt", drivers.get(i).getLatitude());
					driver.put("varLg", drivers.get(i).getLongitude());
					driver.put("varSms", drivers.get(i).getSmsid());
					driver.put("varPh", drivers.get(i).getPhone());

					String driverFlagValue = SystemUtils.getCompanyFlagDescription(getServletConfig(), adminBO.getMasterAssociateCode(), drivers.get(i).getProfile() != null ? drivers.get(i).getProfile() : "", ";", 0);

					driver.put("varDp", driverFlagValue);
					driver.put("varSwh", drivers.get(i).getStatus());
					driver.put("varVNo", drivers.get(i).getVehicleNo());
					driver.put("varDrNa", drivers.get(i).getDrName());
					driver.put("varAg", drivers.get(i).getAge());
					driver.put("varAppV", drivers.get(i).getAppVersion() != null ? drivers.get(i).getAppVersion() : "");
					driver.put("lastUpDate", drivers.get(i).getLastUpdatedMilliSeconds());

					int loginTime = drivers.get(i).getLoginTime() / 60;
					int logintMinutes = drivers.get(i).getLoginTime() % 60;
					driver.put("varLT", "Total LoggedIn Time : " + loginTime + ":" + logintMinutes);
					driverList.put(driver);
				} catch (JSONException e) {
					e.printStackTrace();
				}

			}
		}
		// Driver Details Json End

		// For Zone Waiting
		if (params[2] == "1") {

			ArrayList<QueueBean> al_list = new ArrayList<QueueBean>();
			QueueBean queue = new QueueBean();
			al_list = AdministrationDAO.getDriverinQDash(adminBO.getAssociateCode(), 0, queue, adminBO.getMasterAssociateCode());

			for (int i = 0; i < al_list.size(); i++) {
				JSONObject zoneObj = new JSONObject();
				try {
					zoneObj.put("Dr", al_list.get(i).getQU_DRIVERID() == null ? "" : al_list.get(i).getQU_DRIVERID());
					zoneObj.put("QN", al_list.get(i).getQU_NAME() == null ? "" : al_list.get(i).getQU_NAME());
					zoneObj.put("LT", al_list.get(i).getQU_LOGINTIME() == null ? "" : al_list.get(i).getQU_LOGINTIME());
					zoneObj.put("DRP", al_list.get(i).getDriverProfile() == null ? "" : al_list.get(i).getDriverProfile());
					zoneObj.put("F", al_list.get(i).getFlg());
					zoneObj.put("QD", al_list.get(i).getQueueDescription() == null ? "" : al_list.get(i).getQueueDescription());
					zoneObj.put("Veh", al_list.get(i).getVehicleNo() == null ? "" : al_list.get(i).getVehicleNo());
					zoneJsonArr.put(zoneObj);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		// Zone Waiting End

		// SharedRide first Screen Json
		if (params[3] == "1") {

			String date = request.getParameter("shDate");
			ArrayList<OpenRequestBO> shAddress = AdministrationDAO.getOpnReqDashForShared(adminBO.getAssociateCode(), 0, adminBO.getTimeZone(), 0, date);
			for (int i = 0; i < shAddress.size(); i++) {
				if (shAddress.get(i).getTypeOfRide() != null && shAddress.get(i).getTypeOfRide().equalsIgnoreCase("1")) {
					JSONObject shAddres = new JSONObject();
					try {
						shAddres.put("D", shAddress.get(i).getDriverid() == null ? "" : shAddress.get(i).getDriverid());
						shAddres.put("V", shAddress.get(i).getVehicleNo() == null ? "" : shAddress.get(i).getVehicleNo());
						shAddres.put("N", shAddress.get(i).getName() == null ? "" : shAddress.get(i).getName());
						shAddres.put("T", shAddress.get(i).getTripid() == null ? "" : shAddress.get(i).getTripid());
						shAddres.put("Q", shAddress.get(i).getQueueno() == null ? "" : shAddress.get(i).getQueueno());
						shAddres.put("A", shAddress.get(i).getSadd1() == null ? "" : shAddress.get(i).getSadd1());
						shAddres.put("EA", shAddress.get(i).getEadd1() == null ? "" : shAddress.get(i).getEadd1());

						shAddres.put("PT", shAddress.get(i).getPendingTime() == null ? "" : shAddress.get(i).getPendingTime());
						shAddres.put("P", shAddress.get(i).getPhone() == null ? "" : shAddress.get(i).getPhone());
						shAddres.put("S", shAddress.get(i).getTripStatus() == null ? "" : shAddress.get(i).getTripStatus());
						shAddres.put("ST", shAddress.get(i).getRequestTime() == null ? "" : shAddress.get(i).getRequestTime());
						shAddres.put("SD", shAddress.get(i).getSdate() == null ? "" : shAddress.get(i).getSdate());
						shAddres.put("LA", shAddress.get(i).getSlat() == null ? "" : shAddress.get(i).getSlat());
						shAddres.put("LO", shAddress.get(i).getSlong() == null ? "" : shAddress.get(i).getSlong());
						shAddres.put("DD", shAddress.get(i).getDontDispatch());
						shAddres.put("SR", shAddress.get(i).getTypeOfRide() == null ? "" : shAddress.get(i).getTypeOfRide());
						shAddres.put("AC", shAddress.get(i).getPaytype() == null ? "" : shAddress.get(i).getPaytype());
						shAddres.put("PC", shAddress.get(i).getPremiumCustomer() == 0 ? "" : shAddress.get(i).getPremiumCustomer());
						shAddres.put("RT", shAddress.get(i).getRouteNumber() == null ? "" : shAddress.get(i).getRouteNumber());

						shAddres.put("EDQ", shAddress.get(i).getEndQueueno() == null ? "" : shAddress.get(i).getEndQueueno());
						shAddres.put("LMN", shAddress.get(i).getSlandmark() == null ? "" : shAddress.get(i).getSlandmark());
						shAddres.put("SCTY", shAddress.get(i).getScity() == null ? "" : shAddress.get(i).getScity());
						shAddres.put("ECTY", shAddress.get(i).getEcity() == null ? "" : shAddress.get(i).getEcity());

						String driverFlagValue = SystemUtils.getCompanyFlagDescription(getServletConfig(), adminBO.getMasterAssociateCode(), shAddress.get(i).getDrProfile(), ";", 0);

						shAddres.put("DP", driverFlagValue);
						shArray.put(shAddres);

					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}

		}
		// End Of SharedRide

		// Shared Summary
		if (params[4] == "1") {

			String shdate = request.getParameter("shDDate") != null ? request.getParameter("shDDate") : "";
			ArrayList<OpenRequestBO> shlist = SharedRideDAO.getSharedRideGroups(adminBO.getAssociateCode(), adminBO.getTimeZone(), shdate);
			for (int i = 0; i < shlist.size(); i++) {
				JSONObject shDAddress = new JSONObject();
				try {
					shDAddress.put("RT", shlist.get(i).getRouteNumber() != null ? shlist.get(i).getRouteNumber() : "");
					shDAddress.put("CB", shlist.get(i).getCreatedBy() != null ? shlist.get(i).getCreatedBy() : "");
					shDAddress.put("T", shlist.get(i).getCreatedTime() != null ? shlist.get(i).getCreatedTime() : "");
					shDAddress.put("D", shlist.get(i).getCreatedDate() != null ? shlist.get(i).getCreatedDate() : "");
					shDAddress.put("TT", shlist.get(i).getTotal() != null ? shlist.get(i).getTotal() : "");
					shDAddress.put("DRI", shlist.get(i).getDriverid() != null ? shlist.get(i).getDriverid() : "");
					shDAddress.put("STA", shlist.get(i).getShRideStatus() != null ? shlist.get(i).getShRideStatus() : "");
					shDAddress.put("V", shlist.get(i).getVehicleNo() != null ? shlist.get(i).getVehicleNo() : "");
					shDAddress.put("DES", shlist.get(i).getRouteDesc() != null ? shlist.get(i).getRouteDesc() : "");
					shDArray.put(shDAddress);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}

		// End Of Shared Summary
		// forLastRuntime
		long lastTime = -1;
		if (getServletConfig().getServletContext().getAttribute(adminBO.getAssociateCode() + "Timer") != null) {
			QueueProcess queueProcess = (QueueProcess) this.getServletConfig().getServletContext().getAttribute(adminBO.getAssociateCode() + "Process");
			if (queueProcess.getLastRunTime() > 0) {
				lastTime = (System.currentTimeMillis() - queueProcess.getLastRunTime()) / 1000;
			} else {

			}
		}
		JSONObject lastRun = new JSONObject();
		try {
			lastRun.put("lastRunTime", lastTime);
			// driverFirst.put("lastUpDate","");
			// driverList.put(lastRun);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		// End of Last run time

		// Common for All jsonArrays
		try {
			totalObject.put("jobsList", array);
			totalObject.put("driverList", driverList);
			totalObject.put("zonesList", zoneJsonArr);
			totalObject.put("shList", shArray);
			totalObject.put("shDList", shDArray);
			totalObject.put("lastRun", lastRun);
			totalArray.put(totalObject);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		response.setContentType("application/json");
		response.getWriter().write(totalArray.toString());
	}

	public void messageForDrivers(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//System.out.println("Send message for drivers");
		StringBuffer buffer = new StringBuffer();
		ArrayList al_list = null;
		DriverRegistrationBO driverBO = new DriverRegistrationBO();
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		ApplicationPoolBO poolBO = (ApplicationPoolBO) getServletContext().getAttribute("poolBO");
		String vMessage = request.getParameter("message");
		//System.out.println("vMessage:"+vMessage);
		al_list = RegistrationDAO.getDriverDetailSearch(driverBO,adminBO.getAssociateCode(),"","","","","");
		String smsPrefix = SystemPropertiesDAO.getParameter(adminBO.getMasterAssociateCode(), "prefixSMS");
		SMSBean smsBean=PhoneDAO.getSMSParameter(adminBO.getMasterAssociateCode(),1);
		for(int i=0;i<al_list.size();i++) {
			DriverRegistrationBO driverBO1 = (DriverRegistrationBO)al_list.get(i);
			String phoneNumber=driverBO1.getPhone();
			//System.out.println("driver phone nu,ber:"+phoneNumber);
			phoneNumber = phoneNumber.replace("-", "");
			phoneNumber = phoneNumber.replace(" ", "");
			phoneNumber = smsPrefix + phoneNumber;
			System.out.println("Driver ID:"+driverBO1.getUid()+" ---- PhoneNumber:"+phoneNumber+" ---- AssocCode:"+adminBO.getMasterAssociateCode());
			try {
				sendSMS.main1(vMessage, phoneNumber, adminBO.getMasterAssociateCode(),smsBean);
			} catch (TwilioRestException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}	
	
	public void changeRatingJobs(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		ArrayList<OpenRequestBO> al_list = new ArrayList<OpenRequestBO>();
		String tripId = request.getParameter("tripid");
		String jobRate = request.getParameter("ratings");
		String tripid[]=tripId.split(";");
		int	status=0;
		for(int i=0;i<tripid.length;i++){
			String trip=tripid[i];
		    status = AdministrationDAO.updateOpenRequestforMjobs(trip, adminBO.getAssociateCode(), "2", TDSConstants.newRequestDispatchProcessesNotStarted + "", "Ratings", adminBO.getUid(),jobRate);
			al_list = AdministrationDAO.getOpnReqDash(adminBO.getAssociateCode(), 0, adminBO.getTimeZone(), 0, "", 0, 0);
			AuditDAO.insertJobLogs("Ratings", trip, adminBO.getAssociateCode(), 8, adminBO.getUid(), "", "", adminBO.getMasterAssociateCode());

		}
		response.getWriter().write(Integer.toString(status));
	}
	public void remDntDispatchjobs(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		ArrayList<OpenRequestBO> al_list = new ArrayList<OpenRequestBO>();
		String tripId = request.getParameter("tripid");
		String tripid[]=tripId.split(";");
		int	status=0;
		for(int i=0;i<tripid.length;i++){
			String trip=tripid[i];
			OpenRequestBO openBo = RequestDAO.getOpenRequestByTripID(trip, adminBO.getAssociateCode(), adminBO.getTimeZone());
           status = AdministrationDAO.updateOpenRequestforMjobs(trip, adminBO.getAssociateCode(), "1", TDSConstants.newRequestDispatchProcessesNotStarted + "", "DNTDISPATCH", adminBO.getUid(),"");
			al_list = AdministrationDAO.getOpnReqDash(adminBO.getAssociateCode(), 0, adminBO.getTimeZone(), 0, "", 0, 0);
			AuditDAO.insertJobLogs("DNTDISPATCH", trip, adminBO.getAssociateCode(), 8, adminBO.getUid(), "", "", adminBO.getMasterAssociateCode());

		}
		response.getWriter().write(Integer.toString(status));
	}

	private void getAllDriverDetails(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// TODO Auto-generated method stub
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		ArrayList<String> fleets = new ArrayList<String>();
		if(adminBO.getAssociateCode().equals(adminBO.getMasterAssociateCode()) && adminBO.getFleetDispatch()==1){
			fleets = adminBO.getCompanyList();
		} else {
			fleets.add(adminBO.getAssociateCode());
		}
		/*ArrayList<DriverCabQueueBean> listOfDriver = DispatchDAO.getDriverDetails(request.getParameter("assocode"));
		JSONArray messageArray = new JSONArray();
		for (int i = 0; i < listOfDriver.size(); i++) {
			JSONObject msgObj = new JSONObject();
			try {
				msgObj.put("dri", listOfDriver.get(i).getDriverid());
				msgObj.put("assocode", listOfDriver.get(i).getAssocode());
				messageArray.put(msgObj);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		*/
		JSONArray array = new JSONArray();
		JSONArray driverList = new JSONArray();
		ArrayList<DriverLocationBO> drivers = new ArrayList<DriverLocationBO>();
		long lastTime = -1;
		String compCode = adminBO.getAssociateCode();
		if(adminBO.getAssociateCode().equals(adminBO.getMasterAssociateCode())){
			compCode = adminBO.getMasterAssociateCode();
		}
		/*	if (drStatus.equals("4")) {
				drivers = UtilityDAO.getDriverLocationListByStatus(compCode, 4,adminBO.getTimeZone(),"");
			} else if (drStatus.equals("3")) {
				drivers = UtilityDAO.getDriverLocationListByStatus(compCode, 3,adminBO.getTimeZone(),"");
			} else if (drStatus.equals("5")) {
				drivers = UtilityDAO.getDriverLocationListByStatus(compCode, 5,adminBO.getTimeZone(),"");
			} else {
				drivers = UtilityDAO.getDriverLocationListByStatus(compCode, 1,adminBO.getTimeZone(),"");
		}*/
			drivers = UtilityDAO.getDriverLocationListByStatus(compCode, 5,adminBO.getTimeZone(),"");
		for (int i = 0; i < drivers.size(); i++) {
			JSONObject driver = new JSONObject();
			try {
				driver.put("dri", drivers.get(i).getDriverid());
				driver.put("assocode", compCode);
			/*	driver.put("varDr", drivers.get(i).getDriverid());
				driver.put("varLt", drivers.get(i).getLatitude());
				driver.put("varLg", drivers.get(i).getLongitude());
				driver.put("varSms", drivers.get(i).getSmsid());
				driver.put("varPh", drivers.get(i).getPhone());
				//System.out.println("id:"+drivers.get(i).getDriverid()+"flag:"+drivers.get(i).getProfile());
				String driverFlagValue = SystemUtils.getCompanyFlagDescription(serveletConfig, adminBO.getAssociateCode(), drivers.get(i).getProfile()!=null?drivers.get(i).getProfile():"", ";", 0);

				driver.put("varDp", driverFlagValue);
				driver.put("varSwh", drivers.get(i).getStatus());
				driver.put("varAvS", drivers.get(i).getProvider());
				driver.put("varVNo", drivers.get(i).getVehicleNo());
				driver.put("varDrNa", drivers.get(i).getDrName());
				driver.put("varAg", drivers.get(i).getAge());
				driver.put("varAppV", drivers.get(i).getAppVersion()!=null?drivers.get(i).getAppVersion():"");
				driver.put("lastUpDate", drivers.get(i).getLastUpdatedMilliSeconds());

				int loginTime  = drivers.get(i).getLoginTime()/60;
				int logintMinutes = drivers.get(i).getLoginTime()%60;
				driver.put("varLT", "Total LoggedIn Time : "+loginTime+":"+logintMinutes);
				driver.put("st", drivers.get(i).getDriverActivateStatus());
				
				driver.put("DD", drivers.get(i).getDistance());*/
				driverList.put(driver);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		
		
		
		
		//System.out.println("response--->"+driverList.toString());
		response.getWriter().write(driverList.toString());

}
	
}