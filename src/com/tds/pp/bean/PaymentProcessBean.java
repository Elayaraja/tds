package com.tds.pp.bean;

import java.sql.Blob;
import java.util.List;

public class PaymentProcessBean {
	
	private String b_driverid="";
	private String b_trip_id="";
	private String b_card_issuer="";
	private String b_card_type="";
	private String b_paymentType = "";
	private String b_card_no="";
	private String b_amount = "";
	private String b_cardExpiryDate = "";
	private String b_trans_id = "";
	private String b_trans_id2 = "";
	private String b_approval="";
	private String b_tip_amt="";
	private String b_voucherNo = "";
	private String b_voucherName = "";
	private String b_approval_status="";
	private String b_approval_code="";
	private String b_desc=""; 
	private String b_operator="";
	private String b_cap_trans="";
	private String b_cap_date="";
	private String b_flg="";
	private String b_ret_transid="";
	
	private String b_cardHolderName="";
	
	public String getB_cardHolderName() {
		return b_cardHolderName;
	}
	public void setB_cardHolderName(String bCardHolderName) {
		b_cardHolderName = bCardHolderName;
	}
	private boolean returnById=false;
	private String process_date="";
	private String txn_prec="";
	private String txn_amt="";
	private String txn_total="";
	private String total="";
	private String status_code = "";
	
	private String adj_amount;
	private String reason;
	private String merchid;
	private String serviceid;
	private String type;
	
	private String error_log="";
	
	
	
	
	
	private String add1="";
	private String add2="";
	private String city="";
	private String state="";
	private String zip="";
	private String cvv="";
	private String avs="";
	private String authCapFlag="";
	private int cc_Invoice=0;
	
	
	
	public int getCc_Invoice() {
		return cc_Invoice;
	}
	public void setCc_Invoice(int cc_Invoice) {
		this.cc_Invoice = cc_Invoice;
	}
	public String getB_trans_id2() {
		return b_trans_id2;
	}
	public void setB_trans_id2(String bTransId2) {
		b_trans_id2 = bTransId2;
	}
	public String getAdj_amount() {
		return adj_amount;
	}
	public void setAdj_amount(String adj_amount) {
		this.adj_amount = adj_amount;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getAdd1() {
		return add1;
	}
	public void setAdd1(String add1) {
		this.add1 = add1;
	}
	public String getAdd2() {
		return add2;
	}
	public void setAdd2(String add2) {
		this.add2 = add2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getCvv() {
		return cvv;
	}
	public void setCvv(String cvv) {
		this.cvv = cvv;
	}
	public String getAvs() {
		return avs;
	}
	public void setAvs(String avs) {
		this.avs = avs;
	}
	public String getB_ret_transid() {
		return b_ret_transid;
	}
	public void setB_ret_transid(String bRetTransid) {
		b_ret_transid = bRetTransid;
	}
	public boolean isReturnById() {
		return returnById;
	}
	public void setReturnById(boolean returnById) {
		this.returnById = returnById;
	}
	public String getB_cap_trans() {
		return b_cap_trans;
	}
	public void setB_cap_trans(String bCapTrans) {
		b_cap_trans = bCapTrans;
	}
	public String getB_cap_date() {
		return b_cap_date;
	}
	public void setB_cap_date(String bCapDate) {
		b_cap_date = bCapDate;
	}
	public String getB_flg() {
		return b_flg;
	}
	public void setB_flg(String bFlg) {
		b_flg = bFlg;
	}
	public String getB_operator() {
		return b_operator;
	}
	public void setB_operator(String bOperator) {
		b_operator = bOperator;
	}
	public String getB_desc() {
		return b_desc;
	}
	public void setB_desc(String bDesc) {
		b_desc = bDesc;
	}
	private List captureList;
	
	public List getCaptureList() {
		return captureList;
	}
	public void setCaptureList(List captureList) {
		this.captureList = captureList;
	}
	public String getB_card_full_no() {
		return b_card_full_no;
	}
	public void setB_card_full_no(String bCardFullNo) {
		b_card_full_no = bCardFullNo;
	}
	private String b_card_full_no="";
	private byte[] b_sign;
	
	private String track1="";
	private String track2="";
	private boolean auth_status;
	
	public boolean isAuth_status() {
		return auth_status;
	}
	public void setAuth_status(boolean authStatus) {
		auth_status = authStatus;
	}
	public String getTrack1() {
		return track1;
	}
	public void setTrack1(String track1) {
		this.track1 = track1;
	}
	public String getTrack2() {
		return track2;
	}
	public void setTrack2(String track2) {
		this.track2 = track2;
	}
	public byte[] getB_sign() {
		return b_sign;
	}
	public void setB_sign(byte[] bSign) {
		b_sign = bSign;
	}
	private String b_service_key = "";
	private String b_asscode="";
	
	
	public String getB_asscode() {
		return b_asscode;
	}
	public void setB_asscode(String bAsscode) {
		b_asscode = bAsscode;
	}
	public String getB_service_key() {
		return b_service_key;
	}
	public void setB_service_key(String bServiceKey) {
		b_service_key = bServiceKey;
	}
	public String getB_driverid() {
		return b_driverid;
	}
	public void setB_driverid(String bDriverid) {
		b_driverid = bDriverid;
	}
	public String getB_trip_id() {
		return b_trip_id;
	}
	public void setB_trip_id(String bTripId) {
		b_trip_id = bTripId;
	}
	public String getB_card_issuer() {
		return b_card_issuer;
	}
	public void setB_card_issuer(String bCardIssuer) {
		b_card_issuer = bCardIssuer;
	}
	public String getB_card_type() {
		return b_card_type;
	}
	public void setB_card_type(String bCardType) {
		b_card_type = bCardType;
	}
	public String getB_paymentType() {
		return b_paymentType;
	}
	public void setB_paymentType(String bPaymentType) {
		b_paymentType = bPaymentType;
	}
	public String getB_card_no() {
		return b_card_no;
	}
	public void setB_card_no(String bCardNo) {
		b_card_no = bCardNo;
	}
	public String getB_amount() {
		return b_amount;
	}
	public void setB_amount(String bAmount) {
		b_amount = bAmount;
	}
	public String getB_cardExpiryDate() {
		return b_cardExpiryDate;
	}
	public void setB_cardExpiryDate(String bCardExpiryDate) {
		b_cardExpiryDate = bCardExpiryDate;
	}
	public String getB_trans_id() {
		return b_trans_id;
	}
	public void setB_trans_id(String bTransId) {
		b_trans_id = bTransId;
	}
	public String getB_approval() {
		return b_approval;
	}
	public void setB_approval(String bApproval) {
		b_approval = bApproval;
	}
	public String getB_tip_amt() {
		return b_tip_amt;
	}
	public void setB_tip_amt(String bTipAmt) {
		b_tip_amt = bTipAmt;
	}
	public String getB_voucherNo() {
		return b_voucherNo;
	}
	public void setB_voucherNo(String bVoucherNo) {
		b_voucherNo = bVoucherNo;
	}
	public String getB_voucherName() {
		return b_voucherName;
	}
	public void setB_voucherName(String bVoucherName) {
		b_voucherName = bVoucherName;
	}
	public String getB_approval_status() {
		return b_approval_status;
	}
	public void setB_approval_status(String bApprovalStatus) {
		b_approval_status = bApprovalStatus;
	}
	public String getB_approval_code() {
		return b_approval_code;
	}
	public void setB_approval_code(String bApprovalCode) {
		b_approval_code = bApprovalCode;
	}
	public String getProcess_date() {
		return process_date;
	}
	public void setProcess_date(String process_date) {
		this.process_date = process_date;
	}
	public String getTxn_prec() {
		return txn_prec;
	}
	public void setTxn_prec(String txn_prec) {
		this.txn_prec = txn_prec;
	}
	public String getTxn_amt() {
		return txn_amt;
	}
	public void setTxn_amt(String txn_amt) {
		this.txn_amt = txn_amt;
	}
	public String getTxn_total() {
		return txn_total;
	}
	public void setTxn_total(String txn_total) {
		this.txn_total = txn_total;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public String getStatus_code() {
		return status_code;
	}
	public void setStatus_code(String status_code) {
		this.status_code = status_code;
	}
	public String getMerchid() {
		return merchid;
	}
	public void setMerchid(String merchid) {
		this.merchid = merchid;
	}
	public String getServiceid() {
		return serviceid;
	}
	public void setServiceid(String serviceid) {
		this.serviceid = serviceid;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getError_log() {
		return error_log;
	}
	public void setError_log(String error_log) {
		this.error_log = error_log;
	}
	public String getAuthCapFlag() {
		return authCapFlag;
	}
	public void setAuthCapFlag(String authCapFlag) {
		this.authCapFlag = authCapFlag;
	}
 
	 
	
	
	
	
}
