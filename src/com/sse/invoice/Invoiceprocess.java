package com.sse.invoice;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import com.charges.constant.ICCConstant;
import com.charges.dao.ChargesDAO;
import com.common.action.PaymentProcessingAction;
import com.lowagie.text.pdf.codec.Base64;
import com.tds.dao.EmailSMSDAO;
import com.tds.dao.FinanceDAO;
import com.tds.pp.bean.PaymentProcessBean;
import com.tds.process.Email;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.ApplicationPoolBO;
import com.tds.tdsBO.PaymentSettledDetail;
import com.tds.tdsBO.VoucherBO;

/**
 * Servlet implementation class Invoiceprocess
 */
public class Invoiceprocess extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Invoiceprocess() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doprocess(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doprocess(request,response);
	}

	
	public void doprocess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("In Generating InVoice Process");

		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession(false).getAttribute("user");
		if(adminBO==null){
			return;
		}
		
        //content type must be set to text/event-stream
        response.setContentType("text/event-stream");
 
        //encoding must be set to UTF-8
        response.setCharacterEncoding("UTF-8");
        
        /*writer.write("data: "+ System.currentTimeMillis() +"\n\n");
        writer.close();*/
        //TODO
        //change verified to integer???
        System.out.println("URL : "+request.getRequestURL());
        String verified=request.getParameter("verified")!=null?request.getParameter("verified"):"0";
        String fdate = request.getParameter("fdate")!=null?request.getParameter("fdate"):"";
        String tdate = request.getParameter("tdate")!=null?request.getParameter("tdate"):"";
        String assocode = (request.getParameter("assoccode")!=null && !request.getParameter("assoccode").equals(""))?request.getParameter("assoccode"):"";
        System.out.println("verified:"+verified+" - Fdate:"+fdate+" - Tdate:"+tdate+" - Assoccode:"+assocode);
        
		ArrayList<VoucherBO> voucherList = FinanceDAO.getVerifiedUninvoicedVouchers(adminBO, verified, fdate, tdate,assocode);
		//voucherList = null;
		ArrayList<VoucherBO> voucherListForMail = new ArrayList<VoucherBO>(); 
		
		//voucherList = FinanceDAO.getVerifiedUninvoicedVouchers(adminBO.getAssociateCode(), 1);
		//String[] voucher = new String[voucherList.size()];
		
		final String[] paidOrUnpaid = {"1","2"};
		
		try {
			if(voucherList!=null && voucherList.size()>0){
				for(int j=0;j<voucherList.size();j++){
					VoucherBO voucherBo=voucherList.get(j);
					VoucherBO forMail = new VoucherBO();
					JSONArray array = new JSONArray();
					int[] invoiceNoMail = {0,0};
					PrintWriter writer = response.getWriter();
					
					PrintWriter writerSize = response.getWriter();
					writerSize.write("event:size\n");
					writerSize.write("data: " + "Number of Accounts Pending : "+(voucherList.size()-j) +". Processing "+voucherBo.getVno()+ "\n\n");
					writerSize.flush();
					PaymentSettledDetail voucherBean = new PaymentSettledDetail();
					for(int paid=0; paid<paidOrUnpaid.length;paid++){
						int invoiceNo=0;
						JSONObject invoiceNumbers = new JSONObject();
						ArrayList<PaymentSettledDetail> voucherDetail = FinanceDAO.getNewPaidUnpaidVoucherByVoucher(adminBO, voucherBo.getVno(),paidOrUnpaid[paid],verified);
						if(voucherDetail!=null && voucherDetail.size()>0){
							String[] setId = new String[voucherDetail.size()];
							
							BigDecimal amount = new BigDecimal(0.00);
							for (int i = 0; i < voucherDetail.size(); i++) {
								amount = amount.add(new BigDecimal(voucherDetail.get(i).getTotal()));
								if(paidOrUnpaid[paid].equals("1")){
									if(voucherList.get(j).getCc_Percent_processed()==0 && voucherList.get(j).getCcProcessPercentage()!=0){
										//float k = (float)(amount*(voucherList.get(j).getCcProcessPercentage()/100.0f));
										System.out.println("voucherId:"+voucherDetail.get(i).getTransId());
										System.out.println("amount:"+voucherDetail.get(i).getTotal());
										BigDecimal amt = new BigDecimal(voucherDetail.get(i).getTotal());
										BigDecimal percentage = new BigDecimal(voucherList.get(j).getCcProcessPercentage());
										BigDecimal n = (BigDecimal) amt.multiply(percentage).divide(new BigDecimal(100));
										System.out.println("percentage calculated:"+n);
										amt=amt.add(n);
										amount = amount.add(n);
										System.out.println("total processed amt:"+amt.toString());
										System.out.println("total amt:"+amount);
										FinanceDAO.update_VD_CC_PRCNT_AMT_InvoiceGenerate(adminBO, voucherDetail.get(i).getTransId(), amt+"", n+"");
									}
								}
								setId[i] = voucherDetail.get(i).getTransId();
							}
							amount = amount.setScale(2, RoundingMode.CEILING);
							voucherBean.setVoucherNo(voucherDetail.get(0).getVoucherNo());
							voucherBean.setTotal(amount + "");
							voucherBean.setTotalcount(voucherDetail.size() + "");
							//System.out.println("voucherbean/n V.No:"+voucherBean.getVoucherNo()+" , amount:"+voucherBean.getTotal()+" toatlacc:"+voucherBean.getTotalcount());
							//System.out.println("accounts:"+setId);
							
							invoiceNo = FinanceDAO.insertInvoice(adminBO, voucherBean, setId);
							//invoiceNo = 123;
							invoiceNoMail[paid]=invoiceNo;
							if(paidOrUnpaid[paid].equals("1")){
								//TODO Check if theres credit card. IF so process creditcard/
								//AFter creditc card store the approval numbner ad status in invoiceNumbers;; && !voucherBean.getTotal().equals("0.00")
								if(!voucherBean.getTotal().equalsIgnoreCase("0.00")){
									if(voucherList.get(j).getProcessMethod()==1 ){
										writerSize.write("event:size\n");
										writerSize.write("data: " + "Number of Accounts Pending : "+(voucherList.size()-j) +". Processing Account : "+voucherBo.getVno()+". Processing CreditCard.."+ "\n\n");
										writerSize.flush();
										PaymentProcessBean processBean = new PaymentProcessBean();
										ApplicationPoolBO poolBO = (ApplicationPoolBO) getServletContext().getAttribute("poolBO");
										processBean.setB_trip_id(invoiceNo+"");
										processBean.setB_amount(voucherBean.getTotal());
										processBean.setB_trans_id(voucherBo.getInfokey());
										processBean.setB_tip_amt("0.00");
										processBean.setMerchid(adminBO.getMerchid());
										processBean.setServiceid(adminBO.getServiceid());
										processBean.setCc_Invoice(1);
										processBean = PaymentProcessingAction.initiatePayProcess(poolBO, processBean, "Invoice",adminBO.getMasterAssociateCode());
										if(processBean.isAuth_status()){
											ChargesDAO.insertPaymentIntoCC(processBean, request.getRealPath("/images/nosign.gif"), poolBO, adminBO, ICCConstant.SETTLED,ICCConstant.GAC);
											FinanceDAO.update_VD_CCSettled_OnInvoiceGenerate(adminBO, setId);
											invoiceNumbers.put("CC","Successfully Paid by CreditCard. Approval Code : "+processBean.getB_trans_id());
										}else{
											invoiceNumbers.put("CC","UnPaid - Faliure to process CreditCard");
										}
									}else{
										invoiceNumbers.put("CC","UnPaid - Not a Creditcard Account");
									}
								}else{
									invoiceNumbers.put("CC","UnPaid - Can't Process the Amount");
								}
							}else{
								invoiceNumbers.put("CC","Paided Invoice");
							}
							
							String forEmail="";
							String ems="No";
							if(voucherBo.getEmailStatus()==1){
								forEmail += "Email - set to Automatically. ";
								if(!voucherBo.getEmailAddress().equals("")){
									ArrayList<String> emails=EmailSMSDAO.emailUsers(adminBO.getMasterAssociateCode());
									if(emails.size()>0){
										ems="Yes";
										forEmail += "Email Sent & you will be CC";
									} else {
										forEmail += "No Access To Send Mail";
									}
								}else{
									forEmail += "Email ID Not Found";
								}
							}else{
								ems="Manual";
								forEmail += "Email - Set to Manually";
							}
									
							invoiceNumbers.put("EM",forEmail);
							invoiceNumbers.put("EMS", ems);
							invoiceNumbers.put("Ac",voucherBean.getVoucherNo());
							invoiceNumbers.put("AN",voucherBo.getVname());
							invoiceNumbers.put("Am", voucherBean.getTotal());
							invoiceNumbers.put("NV", voucherBean.getTotalcount());
							invoiceNumbers.put("IN",invoiceNo);
							invoiceNumbers.put("PT", paidOrUnpaid[paid].equals("1")?"UnPaid Invoice":"Paid Invoice");
							invoiceNumbers.put("MSG", "Yes");
							
							array.put(invoiceNumbers);
						}
						System.out.println("On Mail: acc:"+voucherBo.getVno()+" invoiceNo:"+invoiceNoMail[paid]);
					}
					forMail=voucherBo;
					forMail.setInVoiceNo(invoiceNoMail);
					voucherListForMail.add(forMail);
					
					writer.write("data: " + array.toString() + "\n\n");
					writer.flush();
				}
			//System.out.println("Response:"+array.toString());
			//response.getWriter().write(array.toString());
	    	}else{
	    		PrintWriter writerSize = response.getWriter();
				writerSize.write("event:size\n");
				writerSize.write("data: " + (voucherList.size()) + "\n\n");
				writerSize.flush();
				Thread.sleep(1000);
	            
	    	}
		} catch (Exception e) {
			e.printStackTrace();
		}
		PrintWriter writer2 = response.getWriter();
		System.out.println("On Close");
		writer2.write("event: close\n");
		writer2.write("data: " + (voucherList.size()) + "\n\n");
		writer2.flush();
		
		if(voucherListForMail!=null && voucherListForMail.size()>0){
			//String companyEmail = RegistrationDAO.getCompanyEmailAddress(adminBO.getAssociateCode());
			String companyEmail = "";
			for(int j=0;j<voucherListForMail.size();j++){
				
				VoucherBO voucherBo=voucherList.get(j);
				int[] invoiceNoMail = voucherBo.getInVoiceNo();
				
				for(int paid=0; paid<paidOrUnpaid.length;paid++){
					if(invoiceNoMail[paid]!=0){
						System.out.println("On Mail: acc:"+voucherBo.getVno()+" invoiceNo:"+invoiceNoMail[paid]+" email status:"+(voucherBo.getEmailStatus()==1?"Automatic":"Manual"));
						if(voucherBo.getEmailStatus()==1){
							if(!voucherBo.getEmailAddress().equals("")){
								String username="";
								String password="";
								String host="";
								String port="";
								String add="";
								
								//ReportController?event=reportEvent&ReportName=invoice&output=pdf&invoiceNo=
								try{
									ArrayList<String> emails=EmailSMSDAO.emailUsers(adminBO.getMasterAssociateCode());
									if(emails.size()>0){
										username=emails.get(0);
										password=emails.get(1);
										host=emails.get(3);
										port = emails.get(4);
										add = emails.get(5);
										
										String urlString = request.getRequestURL().toString();
										System.out.println("serverName:"+request.getServerName());
										if(request.getServerName().contains("gacdispatch.com")){
											urlString = "https://www.gacdispatch.com/TDS/ReportController?event=reportEvent&ReportName=invoice&type=invoicePdf&output=pdf&invoiceNo="+invoiceNoMail[paid]+"&assoccode="+adminBO.getAssociateCode()+"&master="+adminBO.getMasterAssociateCode()+"&timeOffSet="+adminBO.getTimeZone();
										} else {
											urlString = "http://www.getacabdemo.com/TDS/ReportController?event=reportEvent&ReportName=invoice&type=invoicePdf&output=pdf&invoiceNo="+invoiceNoMail[paid]+"&assoccode="+adminBO.getAssociateCode()+"&master="+adminBO.getMasterAssociateCode()+"&timeOffSet="+adminBO.getTimeZone();
										}
										
										//String urlString = request.getRequestURL().toString().replace("/Invoiceprocess", "")+"/ReportController?event=reportEvent&ReportName=invoice&type=invoicePdf&output=pdf&invoiceNo="+invoiceNoMail[paid]+"&assoccode="+adminBO.getAssociateCode()+"&master="+adminBO.getMasterAssociateCode()+"&timeOffSet="+adminBO.getTimeZone();
										System.out.println("Url--->"+urlString);
										System.setProperty("http.keepAlive", "true");
									
										URL urlV = new URL(urlString.toString());
										HttpURLConnection conn = (HttpURLConnection) urlV.openConnection();
										conn.setDoOutput(true);
										conn.setRequestMethod("POST");
										conn.setConnectTimeout(43000);
									
										byte bytes[] = null;
										conn.connect();
										if (HttpURLConnection.HTTP_OK == conn.getResponseCode()) {
											java.io.InputStream is = conn.getInputStream();
											//Convert InputStream to ByteArray
											bytes = IOUtils.toByteArray(is);
											is.close();
											conn.disconnect();
										}
										String stringToSent = Base64.encodeBytes(bytes);
										Email otherProviderSMS = new Email(host,username,password,username,port,add);
										
										if(companyEmail!=null && !companyEmail.equals("")){
											otherProviderSMS.sendMailPdf(voucherBo.getEmailAddress()+";"+companyEmail,"Invoice_Deatails_For_"+invoiceNoMail[paid],stringToSent,"Invoice number for your account "+voucherBo.getVno()+" is : "+invoiceNoMail[paid]);
										}else{
											otherProviderSMS.sendMailPdf(voucherBo.getEmailAddress(),"Invoice_Deatails_For_"+invoiceNoMail[paid],stringToSent,"Invoice number for your account "+voucherBo.getVno()+" is : "+invoiceNoMail[paid]);
										}									
									} 
								}catch (Exception e ){
									System.out.println(e.toString());
									e.printStackTrace();
									System.out.println("Invoice generation: Exception on Sending mails");
								}
							} 
						}
					}
				}
			}
		}
		System.out.println("Exit after Sent the mail on InvoiceGeneratioon");
		//writer.write("data:close");
		return;
	}
	
}
