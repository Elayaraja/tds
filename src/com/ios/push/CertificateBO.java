package com.ios.push;

public class CertificateBO {

	public String path = "";
	public String password ="";
	public String assoccode = "";
	
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getAssoccode() {
		return assoccode;
	}
	public void setAssoccode(String assoccode) {
		this.assoccode = assoccode;
	}
	
}
